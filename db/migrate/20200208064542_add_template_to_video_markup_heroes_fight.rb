class AddTemplateToVideoMarkupHeroesFight < ActiveRecord::Migration[6.0]
  def change
    add_reference :video_markup_heroes_fights, :template, foreign_key: { to_table: :video_markup_heroes_templates }
  end
end
