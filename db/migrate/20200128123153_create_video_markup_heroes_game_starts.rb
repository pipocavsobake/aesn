class CreateVideoMarkupHeroesGameStarts < ActiveRecord::Migration[6.0]
  def change
    create_table :video_markup_heroes_game_starts do |t|
      t.references :moment, null: false, foreign_key: { to_table: :video_markup_moments }
      t.references :template, null: false, foreign_key: { to_table: :video_markup_heroes_templates }
      t.references :opponent, null: false, foreign_key: { to_table: :video_markup_streamers }
      t.references :hero, null: false, foreign_key: { to_table: :video_markup_heroes_heroes }
      t.references :opponent_hero, null: false, foreign_key: { to_table: :video_markup_heroes_heroes }
      t.references :castle, null: false, foreign_key: { to_table: :video_markup_heroes_castles }
      t.references :opponent_castle, null: false, foreign_key: { to_table: :video_markup_heroes_castles }
      t.bigint :difficulty
      t.boolean :red
      t.bigint :bonus

      t.timestamps
    end
  end
end
