class CreatePageTemplates < ActiveRecord::Migration[6.0]
  def change
    create_table :page_templates do |t|
      t.text :title
      t.string :fullpath
      t.text :content
      t.text :h1
      t.text :keywords
      t.text :description
      t.text :og_title
      t.text :robots

      t.timestamps
    end
  end
end
