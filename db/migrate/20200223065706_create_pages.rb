class CreatePages < ActiveRecord::Migration[6.0]
  def change
    create_table :pages do |t|
      t.string :title
      t.string :fullpath, null: false
      t.text :content
      t.string :h1
      t.text :keywords
      t.text :description
      t.string :og_title
      t.string :robots
      t.jsonb :og_image_data

      t.index :fullpath, unique: true

      t.timestamps
    end
  end
end
