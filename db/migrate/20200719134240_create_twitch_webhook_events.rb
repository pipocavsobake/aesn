class CreateTwitchWebhookEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :twitch_webhook_events do |t|
      t.jsonb :data

      t.timestamps
    end
  end
end
