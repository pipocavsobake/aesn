class AddFieldsToLocalMarkupMore < ActiveRecord::Migration[6.0]
  def change
    add_column :local_markup_markups, :default_form, :string
    add_reference :local_markup_items, :check_point, foreign_key: {to_table: :local_markup_check_points}
  end
end
