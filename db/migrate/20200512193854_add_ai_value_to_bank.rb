class AddAiValueToBank < ActiveRecord::Migration[6.0]
  def change
    add_column :video_markup_heroes_banks, :ai_values, :bigint, array: true
    add_column :video_markup_heroes_banks, :en_name, :string
  end
end
