class CreateLocalMarkupItems < ActiveRecord::Migration[6.0]
  def change
    create_table :local_markup_items do |t|
      t.string :file
      t.string :related_files, array: true, default: []
      t.integer :second
      t.jsonb :data, default: {}

      t.timestamps
    end
  end
end
