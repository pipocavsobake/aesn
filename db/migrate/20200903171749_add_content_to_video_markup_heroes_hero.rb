class AddContentToVideoMarkupHeroesHero < ActiveRecord::Migration[6.0]
  def change
    add_column :video_markup_heroes_heroes, :content, :text
  end
end
