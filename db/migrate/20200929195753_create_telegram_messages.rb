class CreateTelegramMessages < ActiveRecord::Migration[6.0]
  def change
    create_table :telegram_messages do |t|
      t.string :controller
      t.jsonb :payload

      t.timestamps
    end
  end
end
