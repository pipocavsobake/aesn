class CreateMarusiaAliases < ActiveRecord::Migration[6.1]
  def change
    create_table :marusia_aliases do |t|
      t.string :name
      t.string :kind, null: false
      t.string :target
      t.bigint :boost, null: false, default: 0

      t.timestamps
      t.index %i[kind name boost]
      t.index %i[kind name], unique: true
    end
  end
end
