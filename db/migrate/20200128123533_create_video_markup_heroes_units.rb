class CreateVideoMarkupHeroesUnits < ActiveRecord::Migration[6.0]
  def change
    create_table :video_markup_heroes_units do |t|
      t.string :name
      t.references :castle, foreign_key: { to_table: :video_markup_heroes_castles }

      t.timestamps
    end
  end
end
