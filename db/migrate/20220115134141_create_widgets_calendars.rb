class CreateWidgetsCalendars < ActiveRecord::Migration[6.1]
  def change
    create_table :widgets_calendars do |t|
      t.string :name, null: false
      t.date :date_from, null: false
      t.date :date_to, null: false
      t.jsonb :file_data
      t.references :user, null: false, foreign_key: true
      t.text :content
      t.boolean :enabled, null: false, default: false

      t.timestamps
    end
  end
end
