class AddMergedFromToMarkup < ActiveRecord::Migration[6.0]
  def change
    create_table :local_markup_merges do |t|
      t.references :source_markup, foreign_key: {to_table: :local_markup_markups}, null: false
      t.references :target_markup, foreign_key: {to_table: :local_markup_markups}, null: false
      t.index %i[source_markup_id target_markup_id], unique: true, name: 'local_markup_merge_idx'
    end
  end
end
