class CreateTwitchWebhookChallenges < ActiveRecord::Migration[6.0]
  def change
    create_table :twitch_webhook_challenges do |t|
      t.jsonb :data

      t.timestamps
    end
  end
end
