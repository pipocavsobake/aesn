class AddAiValueToHeroesBank < ActiveRecord::Migration[6.0]
  def change
    add_column :video_markup_heroes_units, :ai_value, :bigint
    add_column :video_markup_heroes_units, :fight_value, :bigint
    add_column :video_markup_heroes_units, :en_name, :string
  end
end
