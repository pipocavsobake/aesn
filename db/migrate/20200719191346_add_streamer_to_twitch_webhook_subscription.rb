class AddStreamerToTwitchWebhookSubscription < ActiveRecord::Migration[6.0]
  def change
    add_reference :twitch_webhook_subscriptions, :streamer, foreign_key: {to_table: :video_markup_streamers}, null: true
  end
end
