class AddChatIdToMmmPrintRequest < ActiveRecord::Migration[6.0]
  def change
    add_column :mmm_print_requests, :chat_id, :string
  end
end
