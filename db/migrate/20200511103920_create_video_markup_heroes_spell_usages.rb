class CreateVideoMarkupHeroesSpellUsages < ActiveRecord::Migration[6.0]
  def change
    create_table :video_markup_heroes_spell_usages do |t|
      t.references :spell, null: false, foreign_key: {to_table: :video_markup_heroes_spells}
      t.references :fight, null: false, foreign_key: {to_table: :video_markup_heroes_fights}
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
