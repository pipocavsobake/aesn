class AddEnabledToSportActivity < ActiveRecord::Migration[6.1]
  def change
    add_column :sport_activities, :enabled, :boolean, null: false, default: false
  end
end
