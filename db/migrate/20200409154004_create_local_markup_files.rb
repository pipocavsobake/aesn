class CreateLocalMarkupFiles < ActiveRecord::Migration[6.0]
  def change
    create_table :local_markup_files do |t|
      t.references :check_point, null: false, foreign_key: { to_table: :local_markup_check_points }
      t.string :path

      t.timestamps
    end
  end
end
