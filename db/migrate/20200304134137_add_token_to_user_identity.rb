class AddTokenToUserIdentity < ActiveRecord::Migration[6.0]
  def change
    add_column :user_identities, :token, :string
  end
end
