class AddEnabledByToVideoMarkupHeroesFight < ActiveRecord::Migration[6.0]
  def change
    add_reference :video_markup_heroes_fights, :enabled_by, foreign_key: { to_table: :users }
  end
end
