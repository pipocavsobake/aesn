class CreateUserMerges < ActiveRecord::Migration[6.1]
  def change
    create_table :user_merges do |t|
      t.references :identity, null: false, foreign_key: {to_table: :user_identities}
      t.string :email
      t.datetime :token_expires_at, null: false
      t.string :token, null: false
      t.index :token, unique: true

      t.timestamps
    end
  end
end
