class CreateTelegramFiles < ActiveRecord::Migration[6.0]
  def change
    create_table :telegram_files do |t|
      t.string :source
      t.string :telegram_id
      t.jsonb :data

      t.timestamps
      t.index :source
      t.index :telegram_id
    end
  end
end
