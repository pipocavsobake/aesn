class AddFieldsToTwitchSubscription < ActiveRecord::Migration[6.0]
  def change
    add_column :twitch_webhook_subscriptions, :response_body, :text
    add_column :twitch_webhook_subscriptions, :response_code, :bigint
  end
end
