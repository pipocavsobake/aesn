class AddRefreshTokenToUserIdentity < ActiveRecord::Migration[6.0]
  def change
    add_column :user_identities, :token_expires_at, :datetime
    add_column :user_identities, :refresh_token, :string
  end
end
