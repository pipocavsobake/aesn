class CreateUserIdentities < ActiveRecord::Migration[6.0]
  def change
    create_table :user_identities do |t|
      t.string :uid
      t.string :provider
      t.string :name
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end

    add_index :user_identities, %i[provider uid], unique: true
  end
end
