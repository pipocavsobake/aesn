class AddSnippetTemplateToVideoMarkupVideo < ActiveRecord::Migration[6.0]
  def change
    add_column :video_markup_videos, :snippet_template, :string
  end
end
