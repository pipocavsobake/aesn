class AddCasesToVideoMarkupHeroesUnit < ActiveRecord::Migration[6.0]
  def change
    add_column :video_markup_heroes_units, :cases, :jsonb, null: false, default: {}
    add_column :video_markup_heroes_banks, :cases, :jsonb, null: false, default: {}
    add_column :video_markup_heroes_heroes, :cases, :jsonb, null: false, default: {}
    add_column :video_markup_heroes_templates, :cases, :jsonb, null: false, default: {}
  end
end
