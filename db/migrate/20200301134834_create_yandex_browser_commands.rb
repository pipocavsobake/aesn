class CreateYandexBrowserCommands < ActiveRecord::Migration[6.0]
  def change
    create_table :yandex_browser_commands do |t|
      t.references :browser, null: false, foreign_key: { to_table: :yandex_browsers }
      t.jsonb :data

      t.timestamps
    end
  end
end
