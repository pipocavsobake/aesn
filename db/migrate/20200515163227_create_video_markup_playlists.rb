class CreateVideoMarkupPlaylists < ActiveRecord::Migration[6.0]
  def change
    create_table :video_markup_playlists do |t|
      t.references :user, null: false, foreign_key: true
      t.string :name

      t.timestamps
    end
    add_reference :video_markup_moments, :playlist, foreign_key: {to_table: :video_markup_playlists}
  end
end
