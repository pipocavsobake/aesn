class AddOgImageToSportActivity < ActiveRecord::Migration[6.1]
  def change
    add_column :sport_activities, :og_image_data, :jsonb
  end
end
