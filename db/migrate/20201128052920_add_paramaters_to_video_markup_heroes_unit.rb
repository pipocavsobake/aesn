class AddParamatersToVideoMarkupHeroesUnit < ActiveRecord::Migration[6.0]
  def change
    add_column :video_markup_heroes_units, :attack, :bigint
    add_column :video_markup_heroes_units, :defence, :bigint
    add_column :video_markup_heroes_units, :damage_min, :bigint
    add_column :video_markup_heroes_units, :damage_max, :bigint
    add_column :video_markup_heroes_units, :speed, :bigint
    add_column :video_markup_heroes_units, :pop_growth, :bigint
    add_column :video_markup_heroes_units, :skills, :text
    add_column :video_markup_heroes_units, :aliases, :string, array: true
  end
end
