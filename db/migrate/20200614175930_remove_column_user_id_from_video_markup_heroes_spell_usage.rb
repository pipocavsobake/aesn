class RemoveColumnUserIdFromVideoMarkupHeroesSpellUsage < ActiveRecord::Migration[6.0]
  def change
    remove_reference :video_markup_heroes_spell_usages, :user, null: false, foreign_key: true
  end
end
