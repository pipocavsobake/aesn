class AddTitleToVideoMarkupVideo < ActiveRecord::Migration[6.0]
  def change
    add_column :video_markup_videos, :title, :string
    add_column :video_markup_videos, :published_at, :datetime
    remove_column :video_markup_videos, :snippet_url, :string
  end
end
