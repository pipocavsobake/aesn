class CreateShareLinks < ActiveRecord::Migration[6.0]
  def change
    create_table :share_links do |t|
      t.references :user, null: false, foreign_key: true
      t.string :url
      t.string :session
      t.string :slug

      t.timestamps
      t.index :session
      t.index :slug
    end
  end
end
