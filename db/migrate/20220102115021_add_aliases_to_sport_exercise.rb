class AddAliasesToSportExercise < ActiveRecord::Migration[6.1]
  def change
    add_column :sport_exercises, :aliases, :string, array: true, null: false, default: '{}'
  end
end
