class CreateSportExerciseSimpleGroups < ActiveRecord::Migration[6.1]
  def change
    create_table :sport_exercise_simple_groups do |t|
      t.string :name

      t.timestamps
    end
    add_reference :sport_exercises, :simple_group, foreign_key: {to_table: :sport_exercise_simple_groups}
  end
end
