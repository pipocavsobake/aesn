class CreateVideoMarkupComments < ActiveRecord::Migration[6.0]
  def change
    create_table :video_markup_comments do |t|
      t.references :moment, null: false, foreign_key: { to_table: :video_markup_moments }
      t.text :content
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
