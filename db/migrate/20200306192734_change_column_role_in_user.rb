class ChangeColumnRoleInUser < ActiveRecord::Migration[6.0]
  def change
    remove_column :users, :role, :string
    add_column :users, :roles, :string, array: true
  end
end
