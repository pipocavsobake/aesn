class CreateSaiuClaims < ActiveRecord::Migration[6.1]
  def change
    create_table :saiu_claims do |t|
      t.string :phone
      t.string :email
      t.text :best_time
      t.text :comment
      t.string :status, null: false, default: :new

      t.timestamps
    end
  end
end
