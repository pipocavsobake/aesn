class CreateVideoMarkupHeroesLearnSkills < ActiveRecord::Migration[6.0]
  def change
    create_table :video_markup_heroes_learn_skills do |t|
      t.references :skill, foreign_key: { to_table: :video_markup_heroes_skills }
      t.references :hero, null: false, foreign_key: { to_table: :video_markup_heroes_heroes }
      t.references :moment, null: false, foreign_key: { to_table: :video_markup_moments }
      t.bigint :level, default: 1
      t.bigint :attack, default: 0
      t.bigint :defence, default: 0
      t.bigint :spell_power, default: 0
      t.bigint :knowledge, default: 0

      t.timestamps
    end
  end
end
