class CreateVideoMarkupHeroesTemplates < ActiveRecord::Migration[6.0]
  def change
    create_table :video_markup_heroes_templates do |t|
      t.string :name

      t.timestamps
    end
  end
end
