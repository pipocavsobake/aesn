class CreateVideoMarkupStreamerTags < ActiveRecord::Migration[6.0]
  def change
    create_table :video_markup_streamer_tags do |t|
      t.string :name
      t.index :name
      t.timestamps
    end
    create_table :video_markup_streamers_tags, id: false do |t|
      t.references :streamer, foreign_key: { to_table: :video_markup_streamers }, null: false, index: true
      t.references :tag, foreign_key: { to_table: :video_markup_streamer_tags }, null: false, index: false
      t.index %i[tag_id streamer_id], unique: true
    end
  end
end
