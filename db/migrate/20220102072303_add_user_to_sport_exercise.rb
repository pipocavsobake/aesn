class AddUserToSportExercise < ActiveRecord::Migration[6.1]
  def change
    add_reference :sport_exercises, :user, foreign_key: true
    reversible do |dir|
      dir.up do
        connection.execute('UPDATE sport_exercises SET user_id = 1')
        change_column :sport_exercises, :user_id, :bigint, null: false
      end
    end
  end
end
