class AddAiValueToFight < ActiveRecord::Migration[6.0]
  def change
    add_column :video_markup_heroes_fights, :ai_value, :bigint
  end
end
