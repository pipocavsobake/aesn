class CreateVideoMarkupHeroesFights < ActiveRecord::Migration[6.0]
  def change
    create_table :video_markup_heroes_fights do |t|
      t.references :moment, null: false, foreign_key: { to_table: :video_markup_moments }
      t.references :hero, null: false, foreign_key: { to_table: :video_markup_heroes_heroes }
      t.bigint :attack
      t.bigint :defence
      t.bigint :spell_power
      t.bigint :knowledge
      t.bigint :day
      t.boolean :better_than_autofight
      t.bigint :units_count
      t.bigint :upgraded_units_count
      t.bigint :bank_value
      t.references :bank, foreign_key: { to_table: :video_markup_heroes_banks }
      t.references :unit, foreign_key: { to_table: :video_markup_heroes_units }

      t.timestamps
    end

    add_index :video_markup_heroes_castles, :name, unique: true
    add_index :video_markup_heroes_heroes, %i[castle_id name], unique: true
    add_index :video_markup_heroes_units, :name, unique: true
    add_index :video_markup_heroes_banks, :name, unique: true
  end
end
