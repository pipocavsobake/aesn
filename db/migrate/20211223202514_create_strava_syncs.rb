class CreateStravaSyncs < ActiveRecord::Migration[6.1]
  def change
    create_table :strava_syncs do |t|
      t.datetime :after
      t.references :user_identity, null: false, foreign_key: true
      t.string :summary_state, null: false, default: :new
      t.string :streams_state, null: false, default: :new
      t.string :details_state, null: false, default: :new

      t.timestamps
    end
  end
end
