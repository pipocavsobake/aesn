class AddAliasesToModels < ActiveRecord::Migration[6.1]
  def change
    add_column :video_markup_heroes_heroes, :aliases, :string, array: true
    add_column :video_markup_heroes_spells, :aliases, :string, array: true
  end
end
