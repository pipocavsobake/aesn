class AddImageToVideoMarkupHeroes < ActiveRecord::Migration[6.0]
  def change
    add_column :video_markup_heroes_castles, :image_data, :jsonb
    add_column :video_markup_heroes_heroes, :image_data, :jsonb
    add_column :video_markup_heroes_units, :image_data, :jsonb
    add_column :video_markup_heroes_banks, :image_data, :jsonb
  end
end
