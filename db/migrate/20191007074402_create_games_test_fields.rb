class CreateGamesTestFields < ActiveRecord::Migration[6.0]
  def change
    create_table :games_test_fields do |t|
      t.string :name

      t.timestamps
    end
  end
end
