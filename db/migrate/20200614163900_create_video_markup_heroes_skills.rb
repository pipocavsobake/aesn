class CreateVideoMarkupHeroesSkills < ActiveRecord::Migration[6.0]
  def change
    create_table :video_markup_heroes_skills do |t|
      t.string :name
      t.index :name, unique: true

      t.timestamps
    end
  end
end
