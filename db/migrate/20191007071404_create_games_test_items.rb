class CreateGamesTestItems < ActiveRecord::Migration[6.0]
  def change
    create_table :games_test_items do |t|

      t.references :user
      t.timestamps
    end
  end
end
