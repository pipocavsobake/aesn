class AddHasSamplesToWidgetsKeyboard < ActiveRecord::Migration[6.0]
  def change
    add_column :widgets_keyboards, :has_samples, :boolean
  end
end
