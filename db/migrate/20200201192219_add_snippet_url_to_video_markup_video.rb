class AddSnippetUrlToVideoMarkupVideo < ActiveRecord::Migration[6.0]
  def change
    add_column :video_markup_videos, :snippet_url, :string
  end
end
