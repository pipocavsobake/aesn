class AddCheckPointsCountToLocalMarkupMarkup < ActiveRecord::Migration[6.0]
  def change
    add_column :local_markup_markups, :check_points_count, :bigint, null: false, default: 0
  end
end
