class CreateVideoMarkupStreamerStatuses < ActiveRecord::Migration[6.0]
  def change
    create_table :video_markup_streamer_statuses do |t|
      t.references :streamer, null: false, foreign_key: { to_table: :video_markup_streamers }
      t.bigint :game_id
      t.datetime :started_at

      t.timestamps
      t.index %i[game_id created_at]
    end
  end
end
