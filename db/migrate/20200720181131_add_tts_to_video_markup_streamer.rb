class AddTtsToVideoMarkupStreamer < ActiveRecord::Migration[6.0]
  def change
    add_column :video_markup_streamers, :tts, :string
  end
end
