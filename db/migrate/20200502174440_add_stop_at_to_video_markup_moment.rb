class AddStopAtToVideoMarkupMoment < ActiveRecord::Migration[6.0]
  def change
    add_column :video_markup_moments, :stop_at, :bigint
  end
end
