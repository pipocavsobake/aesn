class CreateMarusiaRequests < ActiveRecord::Migration[6.0]
  def change
    create_table :marusia_requests do |t|
      t.jsonb :request_params
      t.jsonb :response

      t.timestamps
    end
  end
end
