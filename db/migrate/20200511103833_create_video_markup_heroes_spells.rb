class CreateVideoMarkupHeroesSpells < ActiveRecord::Migration[6.0]
  def change
    create_table :video_markup_heroes_spells do |t|
      t.string :name
      t.string :elements, array: true
      t.jsonb :file_data
      t.bigint :level
      t.string :kind
      t.bigint :spell_points
      t.string :duration
      t.string :base
      t.string :advanced
      t.string :expert

      t.timestamps
    end
  end
end
