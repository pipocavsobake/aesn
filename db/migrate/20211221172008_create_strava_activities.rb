class CreateStravaActivities < ActiveRecord::Migration[6.1]
  def change
    create_table :strava_activities do |t|
      t.references :user_identity, foreign_key: true
      t.bigint :strava_id
      t.string :external_id
      t.bigint :upload_id
      t.string :name
      t.bigint :distance
      t.bigint :moving_time
      t.string :strava_type
      t.datetime :start_date
      t.point :start_point
      t.point :end_point
      t.decimal :average_speed, precision: 8, scale: 3
      t.decimal :max_speed, precision: 8, scale: 3
      t.text :description
      t.text :map_polyline
      t.jsonb :source_data

      t.timestamps
    end
  end
end
