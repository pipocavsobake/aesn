class AddHealthPointsToVideoMarkupHeroesUnit < ActiveRecord::Migration[6.0]
  def change
    add_column :video_markup_heroes_units, :health_points, :bigint
    add_column :video_markup_heroes_units, :gold, :bigint
  end
end
