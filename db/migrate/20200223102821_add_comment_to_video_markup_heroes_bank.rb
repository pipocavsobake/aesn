class AddCommentToVideoMarkupHeroesBank < ActiveRecord::Migration[6.0]
  def change
    add_column :video_markup_heroes_banks, :comment, :string
  end
end
