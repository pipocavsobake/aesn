class CreateMmmPrintRequests < ActiveRecord::Migration[6.0]
  def change
    create_table :mmm_print_requests do |t|
      t.string :uid
      t.string :state, null: false, default: :pending
      t.jsonb :file_data
      t.jsonb :data

      t.timestamps
    end
  end
end
