class CreateYandexMessages < ActiveRecord::Migration[6.0]
  def change
    create_table :yandex_messages do |t|
      t.jsonb :data

      t.timestamps
    end
  end
end
