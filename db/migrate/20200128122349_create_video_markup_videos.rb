class CreateVideoMarkupVideos < ActiveRecord::Migration[6.0]
  def change
    create_table :video_markup_videos do |t|
      t.string :type
      t.string :slug
      t.references :streamer, null: false, foreign_key: { to_table: :video_markup_streamers }

      t.timestamps
    end
  end
end
