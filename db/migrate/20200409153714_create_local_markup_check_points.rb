class CreateLocalMarkupCheckPoints < ActiveRecord::Migration[6.0]
  def change
    create_table :local_markup_check_points do |t|
      t.references :markup, null: false, foreign_key: { to_table: :local_markup_markups }
      t.string :path
      t.datetime :reached_at
      t.datetime :processed_at

      t.timestamps
    end
  end
end
