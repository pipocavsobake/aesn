class CreateUserTelegramChats < ActiveRecord::Migration[6.0]
  def change
    create_table :user_telegram_chats do |t|
      t.references :user, null: false, foreign_key: true
      t.string :telegram_id
      t.index :telegram_id, unique: true

      t.timestamps
    end
  end
end
