class AddLevelToVideoMarkupHeroesUnit < ActiveRecord::Migration[6.0]
  def change
    add_column :video_markup_heroes_units, :level, :bigint
    add_column :video_markup_heroes_units, :grade_level, :bigint, null: false, default: 0
  end
end
