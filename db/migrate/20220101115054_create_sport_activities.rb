class CreateSportActivities < ActiveRecord::Migration[6.1]
  def change
    create_table :sport_activities do |t|
      t.string :name, null: false
      t.references :user, foreign_key: true, index: true
      t.datetime :started_at
      t.bigint :duration_seconds
      t.string :kind

      t.timestamps
    end

    add_reference :strava_activities, :sport_activity, foreign_key: true, index: true, null: true
  end
end
