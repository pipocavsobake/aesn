class CreateVideoMarkupHeroesArtifacts < ActiveRecord::Migration[6.0]
  def change
    create_table :video_markup_heroes_artifacts do |t|
      t.string :name
      t.string :en_name
      t.string :aliases, array: true
      t.bigint :level
      t.bigint :attack
      t.bigint :defence
      t.bigint :spell_power
      t.bigint :knowledge
      t.jsonb :image_data
      t.bigint :value
      t.bigint :slot
      t.references :combination_artifact, foreign_key: {to_table: :video_markup_heroes_artifacts}, null: true

      t.timestamps
    end
  end
end
