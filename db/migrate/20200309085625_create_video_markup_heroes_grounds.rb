class CreateVideoMarkupHeroesGrounds < ActiveRecord::Migration[6.0]
  def change
    create_table :video_markup_heroes_grounds do |t|
      t.string :name
      t.bigint :penalty
      t.jsonb :cases, null: false, default: {}
      t.jsonb :image_data

      t.timestamps
    end
  end
end
