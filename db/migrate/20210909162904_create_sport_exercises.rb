class CreateSportExercises < ActiveRecord::Migration[6.1]
  def change
    create_table :sport_exercises do |t|
      t.string :name, null: false
      t.string :slug, null: false
      t.text :content
      t.jsonb :video_data
      t.boolean :enabled, null: false, default: false

      t.index :slug, unique: true

      t.timestamps
    end
  end
end
