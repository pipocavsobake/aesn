class CreateStravaStreamSets < ActiveRecord::Migration[6.1]
  def change
    create_table :strava_stream_sets do |t|
      t.references :activity, null: false, foreign_key: {to_table: :strava_activities}
      t.jsonb :source_data

      t.timestamps
    end
  end
end
