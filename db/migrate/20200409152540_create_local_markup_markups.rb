class CreateLocalMarkupMarkups < ActiveRecord::Migration[6.0]
  def change
    create_table :local_markup_markups do |t|
      t.references :user, null: false, foreign_key: true
      t.string :name
      t.jsonb :file_data
      t.jsonb :cues_file_data

      t.timestamps
    end
    add_reference :local_markup_items, :markup, foreign_key: { to_table: :local_markup_markups }
  end
end
