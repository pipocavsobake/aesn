class AddFieldsToGameTest < ActiveRecord::Migration[6.0]
  def change
    add_column :games_test_items, :strings, :string, array: true, default: []
    add_column :games_test_items, :floats, :float, array: true, default: []
    add_column :games_test_items, :ints, :bigint, array: true, default: []
  end
end
