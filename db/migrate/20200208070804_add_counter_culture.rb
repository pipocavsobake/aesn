class AddCounterCulture < ActiveRecord::Migration[6.0]
  def change
    add_column :video_markup_heroes_heroes, :enabled_fights_count, :bigint, null: false, default: 0
    add_column :video_markup_heroes_units, :enabled_fights_count, :bigint, null: false, default: 0
    add_column :video_markup_heroes_banks, :enabled_fights_count, :bigint, null: false, default: 0
    add_index :video_markup_heroes_heroes, :enabled_fights_count
    add_index :video_markup_heroes_units, :enabled_fights_count
    add_index :video_markup_heroes_banks, :enabled_fights_count
  end
end
