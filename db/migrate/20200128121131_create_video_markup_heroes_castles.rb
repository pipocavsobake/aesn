class CreateVideoMarkupHeroesCastles < ActiveRecord::Migration[6.0]
  def change
    create_table :video_markup_heroes_castles do |t|
      t.string :name
      t.integer :ground_penalty

      t.timestamps
    end
  end
end
