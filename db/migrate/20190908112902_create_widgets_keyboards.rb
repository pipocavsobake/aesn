class CreateWidgetsKeyboards < ActiveRecord::Migration[6.0]
  def change
    create_table :widgets_keyboards do |t|
      t.string :name
      t.string :slug
      t.bigint :keys, comment: 'how many keys in keyboard', default: 88
      t.float :first_freq, comment: 'freq of the first key', default: 27.5
      t.float :block_multiplicator, comment: 'relation between frequencies of correspondance notes in neabour blocks (octaves)', default: 2.0
      t.bigint :block_schema, array: true, comment: 'subblock sizes: 3,4 is the standard', default: [3, 4]
      t.bigint :start_block_at, comment: 'number of key (from 0) on which new block is started. Standard is 3', default: 3

      t.timestamps
    end
    add_index :widgets_keyboards, :slug, unique: true
  end
end
