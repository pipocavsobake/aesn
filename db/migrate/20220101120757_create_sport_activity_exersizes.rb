class CreateSportActivityExersizes < ActiveRecord::Migration[6.1]
  def change
    create_table :sport_activity_exercises do |t|
      t.references :activity, null: false, foreign_key: {to_table: :sport_activities}
      t.references :exercise, null: false, foreign_key: {to_table: :sport_exercises}
      t.text :comment
      t.bigint :position, default: 0, null: false

      t.timestamps
    end
  end
end
