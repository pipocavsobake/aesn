class CreateTwitchWebhookSubscriptions < ActiveRecord::Migration[6.0]
  def change
    create_table :twitch_webhook_subscriptions do |t|
      t.string :callback
      t.string :mode
      t.string :topic
      t.string :lease_seconds
      t.string :secret
      t.index :topic, where: "mode = 'subscribe'"

      t.timestamps
    end
  end
end
