class CreateVideoMarkupMoments < ActiveRecord::Migration[6.0]
  def change
    create_table :video_markup_moments do |t|
      t.bigint :value
      t.references :video, null: false, foreign_key: { to_table: :video_markup_videos }
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
