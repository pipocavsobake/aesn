class CreateUserNotes < ActiveRecord::Migration[6.0]
  def change
    create_table :user_notes do |t|
      t.references :user, null: false, foreign_key: true
      t.text :content
      t.references :theme, null: false, foreign_key: {to_table: :user_themes}

      t.timestamps
    end
    add_column :users, :telegram_code, :string
  end
end
