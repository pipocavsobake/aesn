class CreateVideoMarkupStreamers < ActiveRecord::Migration[6.0]
  def change
    create_table :video_markup_streamers do |t|
      t.string :slug
      t.string :name
      t.string :kind

      t.timestamps
    end
  end
end
