class CreateMarusaPhrases < ActiveRecord::Migration[6.1]
  def change
    create_table :marusia_phrases do |t|
      t.string :kind, null: false
      t.text :content
      t.text :tts
      t.bigint :boost, null: false, default: 1

      t.timestamps

      t.index 'kind, boost DESC'
    end
  end
end
