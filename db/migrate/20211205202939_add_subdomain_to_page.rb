class AddSubdomainToPage < ActiveRecord::Migration[6.1]
  def change
    add_column :pages, :subdomain, :string
    add_column :page_templates, :subdomain, :string
    remove_index :pages, :fullpath
    add_index :pages, %i[subdomain fullpath]
    add_index :pages, :fullpath, unique: true, where: 'subdomain IS NULL'
  end
end
