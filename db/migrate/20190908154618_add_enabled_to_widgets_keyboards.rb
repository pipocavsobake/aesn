class AddEnabledToWidgetsKeyboards < ActiveRecord::Migration[6.0]
  def change
    add_column :widgets_keyboards, :enabled, :boolean
  end
end
