class AddColumnMappingToWidgetsCalendar < ActiveRecord::Migration[6.1]
  def change
    add_column :widgets_calendars, :column_mapping, :jsonb
  end
end
