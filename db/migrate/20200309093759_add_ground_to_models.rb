class AddGroundToModels < ActiveRecord::Migration[6.0]
  def change
    add_reference :video_markup_heroes_fights, :ground, foreign_key: { to_table: :video_markup_heroes_grounds }
    add_reference :video_markup_heroes_units, :ground, foreign_key: { to_table: :video_markup_heroes_grounds }
    add_column :video_markup_heroes_fights, :bank_graded, :boolean
  end
end
