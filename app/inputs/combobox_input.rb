class ComboboxInput < SimpleForm::Inputs::Base
  def input(wrapper_options = nil)
    merged_input_options = merge_wrapper_options(input_html_options, wrapper_options)

    merged_input_options[:list] ||= SecureRandom.hex(10)

    template.content_tag(:div) do
      template.concat @builder.text_field(attribute_name, merged_input_options)
      template.concat datalist(merged_input_options[:list])
    end
  end

  def datalist(dom_id)
    template.content_tag(:datalist, id: dom_id) do
      options[:values]&.each do |value|
        template.concat template.content_tag(:option, value)
      end
    end
  end
end
