ActiveAdmin.register Marusia::Phrase do
  menu parent: 'Маруся'
  permit_params :kind, :content, :tts, :boost
end
