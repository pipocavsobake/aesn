ActiveAdmin.register Marusia::Alias do
  menu parent: 'Маруся'
  permit_params :name, :target, :kind, :boost
end
