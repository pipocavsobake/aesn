ActiveAdmin.register VideoMarkup::Heroes::Castle do
  menu parent: I18n.t('admin.menu.heroes'), label: I18n.t('admin.menu.heroes/castle')
  permit_params :name, :ground_penalty, :image
  filter :id
  filter :name
  form do |f|
    f.inputs do
      f.input :name
      f.input :ground_penalty
      f.input :image, as: :file, hint: (f.object.image.try { |im| image_tag im.url, alt: 'картинка банка'} )
    end
    f.actions
  end
  index do
    selectable_column
    id_column
    column :name
    column :ground_penalty
    column :image do |resource|
      resource.image.try do |image|
        image_tag image.url, alt: 'пропала'
      end
    end
    actions
  end
end
