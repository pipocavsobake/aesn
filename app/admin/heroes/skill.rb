ActiveAdmin.register VideoMarkup::Heroes::Skill do
  menu parent: I18n.t('admin.menu.heroes'), label: I18n.t('admin.menu.heroes/skill')
  permit_params :name
end
