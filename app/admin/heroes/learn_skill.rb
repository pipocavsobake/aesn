ActiveAdmin.register VideoMarkup::Heroes::LearnSkill do
  menu parent: I18n.t('admin.menu.heroes'), label: I18n.t('admin.menu.heroes/learn_skill')
  filter :name
  permit_params :skill_id, :moment_id, :hero_id, *VideoMarkup::Heroes::Skill::PRIMARIES
end
