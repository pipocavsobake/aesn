ActiveAdmin.register VideoMarkup::Heroes::Bank do
  menu parent: I18n.t('admin.menu.heroes'), label: I18n.t('admin.menu.heroes/bank')
  permit_params :name, :image, :comment, cases: AdminHelpers::Cases::LIST
  filter :id
  filter :name

  form do |f|
    f.inputs do
      f.input :name
      f.input :image, as: :file, hint: (f.object.image.try { |im| image_tag im.url, alt: 'картинка банка'} )
      AdminHelpers::Cases.inputs(self, f)
      f.input :comment
    end
    f.actions

  end

  index do
    selectable_column
    id_column
    column :select_name
    column :image do |resource|
      resource.image.try do |image|
        image_tag image.url, alt: 'пропала'
      end
    end
    column :cases do |resource|
      AdminHelpers::Cases.table(self, resource)
    end
    actions
  end

  show do
    attributes_table do
      row :select_name
      row :image do
        resource.image.try do |image|
          image_tag image.url, alt: 'пропала'
        end
      end
      AdminHelpers::Cases.row(self, resource)
    end
    active_admin_comments
  end
end
