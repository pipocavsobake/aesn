ActiveAdmin.register VideoMarkup::Heroes::Hero do
  menu parent: I18n.t('admin.menu.heroes'), label: I18n.t('admin.menu.heroes/hero')
  permit_params :name, :castle_id, :image, :content, cases: AdminHelpers::Cases::LIST
  filter :id
  filter :castle_id
  filter :name

  form do |f|
    f.inputs do
      f.input :name
      f.input :castle
      f.input :image, as: :file, hint: (f.object.image.try { |im| image_tag im.url, alt: 'картинка банка'} )
      f.input :sex
      f.input :content
      AdminHelpers::Cases.inputs(self, f)
    end
    f.actions

  end

  member_action :set_male, method: :put do
    resource.update!(sex: :male)
    redirect_back fallback_location: collection_path, flash: {success: "#{resource.name} стал мужчиной"}
  end

  member_action :set_female, method: :put do
    resource.update!(sex: :female)
    redirect_back fallback_location: collection_path, flash: {success: "#{resource.name} стала женщиной"}
  end

  index do
    selectable_column
    id_column
    column :select_name
    column :castle
    column :image do |resource|
      resource.image.try do |image|
        image_tag image.url, alt: 'пропала'
      end
    end
    column :content_size do |resource|
      resource.content&.size
    end
    column :sex do |resource|
      table do
        tr do
          td { link_to 'возмужать', set_male_admin_video_markup_heroes_hero_path(resource), method: :put } if resource.sex != 'male'
          td { link_to 'обабить', set_female_admin_video_markup_heroes_hero_path(resource), method: :put } if resource.sex != 'female'
        end
      end
    end
    column :cases do |resource|
      AdminHelpers::Cases.table(self, resource)
    end
    actions
  end

  show do
    attributes_table do
      row :select_name
      row :castle
      row :image do
        resource.image.try do |image|
          image_tag image.url, alt: 'пропала'
        end
      end
      row :content
      row :cases do |resource|
        AdminHelpers::Cases.table(self, resource)
      end
    end
    active_admin_comments
  end

  controller do
    def scoped_collection
      end_of_association_chain.preload(:castle)
    end
  end
end
