ActiveAdmin.register VideoMarkup::Heroes::Fight do
  batch_action :enable do |ids|
    authorize! :enable, VideoMarkup::Heroes::Fight
    begin
      VideoMarkup::Heroes::Fight.transaction do
        VideoMarkup::Heroes::Fight.preload(:hero, :unit, :bank).where(id: ids, enabled_by: nil).each do |fight|
          fight.update!(enabled_by: current_user)
        end
      end
      redirect_back fallback_location: collection_path, flash: {success: 'Одобрено'}
    rescue => e
      redirect_back fallback_location: collection_path, flash: {success: 'Ошибка'}
    end
  end

  batch_action :disable do |ids|
    authorize! :disable, VideoMarkup::Heroes::Fight
    begin
      VideoMarkup::Heroes::Fight.transaction do
        VideoMarkup::Heroes::Fight.preload(:hero, :unit, :bank).where(id: ids).where.not(enabled_by: nil).each do |fight|
          fight.update!(enabled_by: nil)
        end
      end
      redirect_back fallback_location: collection_path, flash: {success: 'Одобрение снято'}
    rescue => e
      redirect_back fallback_location: collection_path, flash: {success: 'Ошибка'}
    end
  end

  controller do
    include AdminHelpers
    def scoped_collection
      end_of_association_chain.preload(:hero, :unit, :bank, moment: :video)
    end
  end

  filter :id
  filter :ground
  filter :user_id_eq
  filter :user_id_not_eq

  form do |f|
    f.inputs do
      f.input :hero
      f.input :attack
      f.input :defence
      f.input :spell_power
      f.input :knowledge
      f.input :unit
      f.input :units_count
      f.input :bank
      f.input :bank_value
      f.input :day
      f.input :template
      f.input :bank_graded
      f.input :ground
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :moment
    column :hero
    column :unit
    column :bank
    column :full_text
    actions defaults: true do |resource|
      if resource.enabled?
        link_to 'Снять одобрение', disable_admin_video_markup_heroes_fight_path(resource.id), method: :put if can?(:disable, resource)
      else
        link_to 'Одобрить', enable_admin_video_markup_heroes_fight_path(resource.id), method: :put if can?(:enable, resource)
      end

    end
  end

  member_action :enable, method: :put do
    authorize! :enable, resource
    begin
      resource.update!(enabled_by: current_user)
      redirect_back fallback_location: collection_path, flash: { success: 'Одобрено' }
    rescue => e
      redirect_back fallback_location: collection_path, flash: { error: 'Ошибка' }
    end
  end

  member_action :disable, method: :put do
    authorize! :disable, resource
    begin
      resource.update!(enabled_by: nil)
      redirect_back fallback_location: collection_path, flash: { success: 'Одобрения снято' }
    rescue => e
      puts e.class.to_s
      puts e.message
      puts e.backtrace
      redirect_back fallback_location: collection_path, flash: { error: 'Ошибка' }
    end
  end

  menu parent: I18n.t('admin.menu.heroes'), label: I18n.t('admin.menu.heroes/fight')

  permit_params :hero_id,
    :attack,
    :defence,
    :spell_power,
    :knowledge,
    :day,
    :bank_id,
    :bank_value,
    :unit_id,
    :units_count,
    :upgraded_units_count,
    :bank_graded,
    :ground_id,
    :stop_at,
    spell_ids: []

  scope :enabled
  scope :disabled
  scope :bank
  scope :unit
  scope :tempest

  show do
    attributes_table do
      row :enabled_by
      row :full_text
    end
    active_admin_comments
  end
end
