ActiveAdmin.register VideoMarkup::Heroes::Unit do
  menu parent: I18n.t('admin.menu.heroes'), label: I18n.t('admin.menu.heroes/unit')
  permit_params :name, :castle_id, :image, :level, :grade_level, cases: AdminHelpers::Cases::LIST
  filter :id
  filter :castle_id
  filter :name
  form do |f|
    f.inputs do
      f.input :name
      f.input :castle_id
      f.input :level
      f.input :grade_level
      f.input :image, as: :file, hint: (f.object.image.try { |im| image_tag im.url, alt: 'картинка банка'} )
      AdminHelpers::Cases.inputs(self, f)
    end
    f.actions

  end

  index do
    selectable_column
    id_column
    column :select_name
    column :image do |resource|
      resource.image.try do |image|
        image_tag image.url, alt: 'пропала'
      end
    end
    column :cases do |resource|
      AdminHelpers::Cases.table(self, resource)
    end
    actions
  end

  show do
    attributes_table do
      row :select_name
      row :castle
      row :image do
        resource.image.try do |image|
          image_tag image.url, alt: 'пропала'
        end
      end
      AdminHelpers::Cases.row(self, resource)
      row :level
      row :grade_level
    end
    active_admin_comments
  end

  controller do
    def scoped_collection
      end_of_association_chain.preload(:castle)
    end
  end
end
