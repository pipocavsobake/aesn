ActiveAdmin.register VideoMarkup::Heroes::Spell do
  menu parent: I18n.t('admin.menu.heroes'), label: I18n.t('admin.menu.heroes/spell')
  filter :name
  permit_params :name, :file, :level, :spell_points
  index do
    selectable_column
    id_column
    column :name
    column :file do |resource|
      resource.file.try do |file|
        image_tag file.url, alt: resource.name
      end
    end
    column :level
    column :kind
    column :spell_points
    actions
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :file, as: :file, hint: f.object.file.try { |file| image_tag(file.url, alt: 'картинка') }
      f.input :level
      f.input :spell_points
    end
    f.actions
  end
end
