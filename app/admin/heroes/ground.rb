ActiveAdmin.register VideoMarkup::Heroes::Ground do
  menu parent: I18n.t('admin.menu.heroes'), label: I18n.t('admin.menu.heroes/ground')
  permit_params :name, :penalty, :image, cases: AdminHelpers::Cases::LIST
  filter :id
  filter :name
  filter :penalty

  form do |f|
    f.inputs do
      f.input :name
      f.input :penalty
      f.input :image, as: :file, hint: (f.object.image.try { |im| image_tag im.url, alt: 'картинка банка'} )
      AdminHelpers::Cases.inputs(self, f)
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :name
    column :penalty
    column :image do |resource|
      resource.image.try do |image|
        image_tag image.url, alt: 'пропала'
      end
    end
    column :cases do |resource|
      AdminHelpers::Cases.table(self, resource)
    end
    actions
  end

  show do
    attributes_table do
      row :name
      row :penalty
      row :image do
        resource.image.try do |image|
          image_tag image.url, alt: 'пропала'
        end
      end
      AdminHelpers::Cases.row(self, resource)
    end
  end
end
