ActiveAdmin.register Sport::Exercise do
  menu parent: I18n.t('admin.menu.sport'), label: I18n.t('admin.menu.sport/exercise')
  permit_params :name, :content, :enabled, :video, :slug
  filter :id
  filter :name
  filter :slug
  filter :enabled

  index do
    selectable_column
    id_column
    column :name
    column :slug
    column :video do |resource|
      AdminHelpers::Videos.preview(self, resource.video(:transcoded) || resource.video, video_attrs: {height: 100})
    end
    toggle_bool_column :enabled
    actions
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :name
      f.input :video, as: :file, hint: AdminHelpers::Videos.preview(self, f.object.video(:transcoded) || f.object.video, video_attrs: {height: 200})
      f.input :content
      f.input :enabled
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      row :video do
        AdminHelpers::Videos.preview(self, resource.video(:transcoded) || resource.video, video_attrs: {height: 200})
      end
      row :screenshot do
        AdminHelpers::Images.preview(self, resource.video(:screenshot), image_attrs: {height: 200})
      end
      row :enabled
      row :content
    end
  end

end
