ActiveAdmin.register VideoMarkup::Comment do
  menu parent: I18n.t('admin.menu.video_markup'), label: I18n.t('admin.menu.video_markup/comment')
  permit_params :content, :moment_id, :stop_at
  filter :user_id
  filter :moment_id

  before_create do |resource|
    resource.user = current_user
  end
end
