ActiveAdmin.register VideoMarkup::Video do
  menu parent: I18n.t('admin.menu.video_markup'), label: I18n.t('admin.menu.video_markup/video')
  permit_params :type, :slug, :streamer_id, :title, :published_at
  filter :id
  filter :slug
  filter :streamer_id
  filter :type, as: :select, collection: %w[VideoMarkup::Video::Twitch VideoMarkup::Video::Youtube]

  index do
    selectable_column
    id_column
    column :slug
    column :type
    column :streamer
    column :title
    column :published_at
    column :remote_url do |resource|
      next unless resource.remote_url
      link_to resource.remote_url
    end
    column :snippet do |resource|
      next unless  resource.snippet_url
      image_tag resource.snippet_url, height: 150
    end
    actions
  end

end
