ActiveAdmin.register VideoMarkup::Streamer do
  menu parent: I18n.t('admin.menu.video_markup'), label: I18n.t('admin.menu.video_markup/streamer')
  permit_params :name, :slug, :kind, :tts
  filter :id
  filter :name
  filter :slug
  filter :kind

  member_action :create_webhook_subscription, method: :post do
    begin
      authorize!(:create_webhook_subscription, resource)
      resource.create_webhook_subscription!
      redirect_back fallback_location: collection_path, flash: {success: 'Подписка на webhook создана'}
    rescue => e
      puts e.class.to_s
      puts e.message
      puts e.backtrace
      redirect_back fallback_location: collection_path, flash: {error: "Ошибка #{e.class.to_s} #{e.message}\n #{e.backtrace.first}"}
    end
  end

  action_item :create_webhook_subscription, only: %i[show edit] do
    if can?(:create_webhook_subscription, resource) && resource.kind == 'twitch'
      link_to 'Создать подписку на вебхук', create_webhook_subscription_admin_video_markup_streamer_path(resource.id), method: :post
    end
  end
end
