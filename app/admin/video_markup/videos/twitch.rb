ActiveAdmin.register VideoMarkup::Video::Twitch do
  menu parent: I18n.t('admin.menu.video_markup'), label: I18n.t('admin.menu.video_markup/videos/twitch')
  permit_params :slug, :streamer_id
  filter :id
  filter :slug
  filter :streamer_id

end
