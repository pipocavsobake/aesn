ActiveAdmin.register VideoMarkup::Video::Youtube do
  menu parent: I18n.t('admin.menu.video_markup'), label: I18n.t('admin.menu.video_markup/videos/youtube')
  permit_params :type, :slug, :streamer_id
  filter :id
  filter :slug
  filter :streamer_id
  filter :type, as: :select, collection: %w[VideoMarkup::Video::Twich VideoMarkup::Video::Youtube]

  show do
    attributes_table do
      row :slug
      row :streamer
      row :created_at
      row :updated_at
    end
    panel I18n.t('admin.panel.video_markup/videos/youtube/embedded') do
      iframe(
        src: "https://youtube.com/embed/#{resource.slug}",
        width: 800,
        height: 450,
        class: 'js-admin-video-markup-youtube-embed'
      )
    end
    active_admin_comments
  end

end
