ActiveAdmin.register VideoMarkup::Moment do
  menu parent: I18n.t('admin.menu.video_markup'), label: I18n.t('admin.menu.video_markup/moment')
  permit_params :value, :video_id, :stop_at,
    heroes_fights_attributes: [
      :hero_id,
      :day,
      :units_count, :bank_value, :bank_id, :unit_id,
      :upgraded_units_count, :bank_graded, :ground_id,
      *VideoMarkup::Heroes::Skill::PRIMARIES
    ] + [spell_ids: []],
    heroes_learn_skills_attributes: [
      :id, :skill_id, :hero_id, :level, :_destroy,
      *VideoMarkup::Heroes::Skill::PRIMARIES
    ],
    comments_attributes: [
      :content
    ]
  filter :id
  filter :video_id
  filter :value

  before_create do |moment|
    moment.user = current_user
    moment.comments.each { |com| com.user = current_user }
  end
end
