ActiveAdmin.register Saiu::Claim do
  menu parent: "Saiu"
  permit_params :status

  index do
    selectable_column
    id_column
    column :phone
    column :email
    tag_column :status
    actions
  end

  form do |f|
    f.inputs do
      f.input :email
      f.input :phone
      f.input :status
    end
    f.actions
  end
end
