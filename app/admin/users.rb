ActiveAdmin.register User do
  permit_params roles: []

  index do
    selectable_column
    id_column
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :roles do |resource|
      resource.roles&.to_a&.to_sentence
    end
    column :created_at
    actions defaults: true do |resource|
      next if resource.id == current_user.id || !can?(:inpersonate, resource)
      item("Стать", [:impersonate, :admin, resource], method: :put)
    end
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs do
      f.input :roles
    end
    f.actions
  end

  member_action :impersonate, method: :put do
    authorize!(:impersonate, resource)
    resource = User.find(params[:id])
    sign_in(:user, resource)
    redirect_to '/', flash: {success: "Вы стали #{resource.email}"}
  end

  show do
    attributes_table do
      row :email
      row :roles do
        resource.roles.map(&:to_s).to_sentence
      end
      row :created_at
    end
  end

end
