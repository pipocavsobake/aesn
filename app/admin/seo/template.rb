ActiveAdmin.register Page::Template do
  menu parent: I18n.t('admin.menu.seo'), label: I18n.t('admin.menu.seo/template')
  filter :id
  filter :fullpath
  permit_params :title, :fullpath, :content, :h1, :keywords, :description, :og_title, :robots
  form do |f|
    f.inputs do
      f.input :title, input_html: {style: "font-family: monospace"}
      f.input :fullpath
      f.input :content, input_html: {style: "font-family: monospace"}
      f.input :h1, input_html: {style: "font-family: monospace"}
      f.input :keywords, input_html: {style: "font-family: monospace"}
      f.input :description, input_html: {style: "font-family: monospace"}
      f.input :og_title, input_html: {style: "font-family: monospace"}
      f.input :robots, input_html: {style: "font-family: monospace"}
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :fullpath do |resource|
      link_to resource.fullpath, resource.fullpath
    end
    column :meta_tags do |resource|
      table do
        %w[title h1 keywords description og_title robots].map do |col|
          next if %w[keywords robots].include?(col) && resource.send(col).blank?

          tr do
            if %w[title description].include?(col) && resource.send(col).blank?
              td(style: 'color: red; font-weight: bold') { col }
            else
              td { col }
            end
            td { resource.send(col) }
          end
        end
      end
    end
    column :content
    actions
  end

  show do
    attributes_table do
      row :title
      row :fullpath do
        link_to resource.fullpath, resource.fullpath
      end
      row :content
      row :h1
      row :keywords
      row :description
      row :og_title
      row :robots
    end
    active_admin_comments
  end
end
