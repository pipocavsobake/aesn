ActiveAdmin.register Page do
  menu parent: I18n.t('admin.menu.seo'), label: I18n.t('admin.menu.seo/page')
  filter :id
  filter :fullpath
  filter :subdomain, as: :select, collection: -> { Page.distinct.pluck(:subdomain) }
  permit_params :title, :fullpath, :subdomain, :content, :h1, :keywords, :description, :og_title, :robots, :og_image
  form do |f|
    f.inputs do
      f.input :title
      f.input :subdomain
      f.input :fullpath
      f.input :content
      f.input :h1
      f.input :keywords
      f.input :description
      f.input :og_title
      f.input :robots
      f.input :og_image, as: :file, hint: (f.object.og_image.try { |im| image_tag im.url, alt: 'og image', style: 'max-height: 200px; max-width: 200px'} )
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :subdomain
    column :fullpath do |resource|
      if resource.subdomain
        link_to resource.fullpath, "https://#{resource.subdomain}.aesn.ru#{resource.fullpath}"
      else
        link_to resource.fullpath, resource.fullpath
      end
    end
    column :meta_tags do |resource|
      table do
        %w[title h1 keywords description og_title robots].map do |col|
          next if %w[keywords robots].include?(col) && resource.send(col).blank?

          tr do
            if %w[title description].include?(col) && resource.send(col).blank?
              td(style: 'color: red; font-weight: bold') { col }
            else
              td { col }
            end
            td { resource.send(col) }
          end
        end
      end
    end
    column :content
    column :og_image do |resource|
      resource.og_image.try do |image|
        image_tag image.url, alt: 'пропала', style: 'max-height: 200px; max-width: 200px'
      end
    end
    actions
  end

  show do
    attributes_table do
      row :title
      row :subdomain
      row :fullpath do
        if resource.subdomain
          link_to resource.fullpath, "https://#{resource.subdomain}.aesn.ru#{resource.fullpath}"
        else
          link_to resource.fullpath, resource.fullpath
        end
      end
      row :content
      row :h1
      row :keywords
      row :description
      row :og_title
      row :robots
      row :og_image do
        resource.og_image.try do |image|
          image_tag image.url, alt: 'пропала', width: 320
        end
      end
    end
    active_admin_comments
  end
end
