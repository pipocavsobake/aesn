ActiveAdmin.register LocalMarkup::Markup do
  menu parent: I18n.t('admin.menu.local_markup'), label: I18n.t('admin.menu.local_markup/markup')
  batch_action :merge, form: {name: :text} do |ids, inputs|
    resources = LocalMarkup::Markup.accessible_by(current_ability).find(ids.sort)
    resource = resources.to_a.reduce(&:&)
    resource.name = inputs[:name]
    resource.user = current_user
    resource.source_markups = resources
    if resource.save
      redirect_to admin_local_markup_markup_path(resource), flash: {success: 'Смёржено'}
    else
      redirect_back fallback_location: collection_path, flash: {error: resource.errors.full_message.join("\n") }
    end
  end
  scope :all
  scope :derivative
  controller do
    def scoped_collection
      super.preload(:source_markups)
    end
  end
  index do
    selectable_column
    id_column
    column :user
    column :name_with_counts
    column :file do |resource|
      resource.file.try do |file|
        link_to file.metadata['filename'], file.url, target: '_blank'
      end
    end
    column :source_markups
    column :created_at
    actions defaults: true do |resource|
      item 'Результат', local_markup_result_path(resource, format: :json)
    end
  end

  action_item :result, only: :show do
    link_to 'Результат', local_markup_result_path(resource, format: :json)
  end

  show do
    attributes_table do
      row :user
      row :name
      row :total
      row :check_points_count
      row :file do
        resource.file.try do |file|
          link_to file.metadata['filename'], file.url, target: '_blank'
        end
      end
      row :source_markups
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end
end
