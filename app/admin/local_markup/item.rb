ActiveAdmin.register LocalMarkup::Item do
  menu parent: I18n.t('admin.menu.local_markup'), label: I18n.t('admin.menu.local_markup/item')
  permit_params :file, :second, :markup_id, :check_point_file,
    related_files: [],
    data: (%i[composer_id composer_name piece_id piece_name performer_id performer_name part_number part_name] + [{composer_ids: [], performer_ids: []}])
end
