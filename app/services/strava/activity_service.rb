class Strava::ActivityService
  attr_reader :identity, :stream_job, :activity_job
  def initialize(identity, **args)
    @identity = identity
    @stream_job, @activity_job = args.values_at(:stream_job, :activity_job)
  end

  def fetch_activities(after: nil, before: nil, sync_id: nil)
    with_state(sync_id, :summary_state) do
      ActivitiesEnumerable.new(identity, after: after).each do |fetched_activity_data|
        activity = identity.strava_activities.find_or_initialize_by(
          **attributes_to_identify(fetched_activity_data)
        )
        activity.update!(**attributes_to_update(fetched_activity_data, file: activity.source.nil?))
        if stream_job.present?
          stream_job.perform_later(activity.id, keys: Strava::StreamSet::KEYS)
        else
          fetch_streams(activity, keys: Strava::StreamSet::KEYS)
        end
        if activity_job.present?
          activity_job.perform_later(activity.id)
        else
          fetch_activity(activity)
        end
      end
    end
  end

  def fetch_activity(activity, sync_id: nil)
    with_state(sync_id, :details_state) do
      fetched_activity_data = activity.user_identity.api_client.activity(activity.strava_id)
      activity.update!(**attributes_to_update(fetched_activity_data, file: true))
    end
  end

  def refresh_attributes(activity)
    fetched_activity_data = activity.fetched_data
    attributes = attributes_to_update(fetched_activity_data)
    activity.update!(**attributes)
  end

  def attributes_to_identify(fetched_activity_data)
    {strava_id: fetched_activity_data.id}
  end

  def attributes_to_update(fetched_activity_data, file: false)
    fetched_activity_data.to_h.symbolize_keys.except(:id, :map).slice(
      *Strava::Activity.column_names.map(&:to_sym),
    ).merge(
      {
        source: file ? StringIO.new(fetched_activity_data.to_h.to_json) : nil,
        map_polyline: fetched_activity_data.map.try { |map| map.polyline || map.summary_polyline },
        strava_type: fetched_activity_data.type,
        start_point: fetched_activity_data.start_latlng.presence.try { |pt| ActiveRecord::Point.new(*pt) },
        end_point: fetched_activity_data.end_latlng.presence.try { |pt| ActiveRecord::Point.new(*pt) },
      }.compact,
    )
  end

  def fetch_streams(activity, keys: Strava::StreamSet::KEYS, sync_id: nil)
    with_state(sync_id, :streams_state) do
      fetched_data = activity.user_identity.api_client.activity_streams(activity.strava_id, keys: keys)
      stream_set = activity.stream_set || activity.build_stream_set
      stream_set.update!(source: StringIO.new(fetched_data.to_h.to_json))
    end
  end

  def with_state(sync_id, state_col)
    set_state(sync_id, state_col, :in_progress)
    yield
    set_state(sync_id, state_col, :done)
  rescue => e
    set_state(sync_id, state_col, :failed)
    raise e
  end

  def set_state(sync_id, state_col, value)
    Strava::Sync.where(id: sync_id).update_all({state_col => value})
  end

  class ActivitiesEnumerable
    include Enumerable

    def initialize(identity, **options)
      @identity = identity
      @after, @before, @per_page = options.values_at(:after, :before, :per_page)
    end

    def each(&block)
      page = 0
      options = {after: @after&.to_i, before: @before&.to_i, per_page: @per_page}.compact
      loop do
        page += 1
        resp = @identity.api_client.athlete_activities(**options.merge(page: page))
        break if resp.blank?

        resp.each(&block)
      end
    end
  end
end
