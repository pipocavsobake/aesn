class LocalMarkupUploader < Shrine
  plugin :add_metadata
  add_metadata :schema_error do |io|
    Shrine.with_file(io) do |file|
      begin
        data = JSON.parse(File.open(file.path).read) rescue nil
        raise "is not a json-object" unless data.is_a?(Hash)
        data = data['files'] if data.key?('files')
        raise ".files is not an object" unless data.is_a?(Hash)
        key = data.find {|_, v| !v.is_a?(Array) }&.first
        raise ".#{key} is not an array" if key
        key = data.find { |_, v| v.find { |s| !s.is_a?(String) } }
        raise ".#{key} is not an array of strings" if key
        nil
      rescue => e
        e.inspect
      end
    end
  end

  add_metadata :check_points_count do |io|
    Shrine.with_file(io) do |file|
      data = JSON.parse(File.open(file.path).read)
      data = data['files'] if data.key?('files')
      data.keys.size
    end
  end
  Attacher.validate do
    validate_mime_type %w[text/plain application/json]
    errors << file.schema_error if file.schema_error
  end
end
