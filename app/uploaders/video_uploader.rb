class VideoUploader < Shrine
  def self.to_even_i(int)
    (int / 2).to_i * 2
  end

  def self.resize_video_to_fit(movie, limit_s, keep_too_small: false, **args)
    resized = Tempfile.new ["resized", ".mp4"]

    min_s, max_s = [movie.height, movie.width].sort
    scale = [1, limit_s.to_f / max_s.to_f].min
    return if scale == 1 && !keep_too_small

    to_even_i = VideoUploader.method(:to_even_i).to_proc
    resolution = [movie.width * scale, movie.height * scale].map(&to_even_i).join('x')
    movie.transcode(resized.path, resolution: resolution, **args)

    resized
  end

  def self.screenshot(movie)
    screenshot = Tempfile.new ["screenshot", ".jpg"]
    movie.screenshot(screenshot.path)
    screenshot
  end
  plugin :custom_pretty_location, namespace: '_'
  plugin :add_metadata
  plugin :refresh_metadata
  plugin :determine_mime_type
  Attacher.derivatives do |original|

    movie = FFMPEG::Movie.new(original.path)

    {
      max160: VideoUploader.resize_video_to_fit(movie, 160, keep_too_small: true),
      max300: VideoUploader.resize_video_to_fit(movie, 300),
      max500: VideoUploader.resize_video_to_fit(movie, 500),
      max800: VideoUploader.resize_video_to_fit(movie, 800),
      screenshot: VideoUploader.screenshot(movie),
    }.compact
  end


  add_metadata(:updated_at) { |io| Time.now }
  add_metadata do |io, context|
    movie = Shrine.with_file(io) { |file| FFMPEG::Movie.new(file.path) }

    {
      duration: movie.duration,
      bitrate: movie.bitrate,
      resolution: movie.resolution,
      frame_rate: movie.frame_rate,
      width: movie.width,
      height: movie.height,
    }
  end
end
