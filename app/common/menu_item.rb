class MenuItem
  attr_reader :label, :url, :children
  def initialize(label, url, current_url, crumbs, exact: false, **options)
    @label = label
    @url = url
    @current_url = current_url
    @crumbs = crumbs
    @options = options
    yield self if block_given?
    @active = url == current_url
    @has_active_child = calc_has_active_child?
    if children.blank? && !exact && crumbs.include?(url)
      @active = true
    end
  end

  def add_menu_item(label, url = nil, **options, &block)
    @children ||= []
    @children << self.class.new(label, url, @current_url, @crumbs, **options, &block)
  end

  def calc_has_active_child?
    children&.any?(&:expanded?) || @crumbs.include?(url)
  end

  def has_active_child?
    !!@has_active_child
  end

  def active?
    !!@active
  end

  def expanded?
    active? || has_active_child?
  end

  def without_children
    self.class.new(label, url, @current_url, @crumbs, exact: true, **options)
  end

  def divider?
    false
  end

  def options
    @options.slice(:method)
  end
end
