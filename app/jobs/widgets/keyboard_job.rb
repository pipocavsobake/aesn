class Widgets::KeyboardJob < ApplicationJob
  queue_as :widgets_keyboard
  def perform(id)
    @keyboard = Widgets::Keyboard.find(id)
    return if @keyboard.has_samples

    FileUtils.mkdir_p(Rails.root.join("public", "samples", @keyboard.id.to_s))
    @keyboard.key_data.each do |key|
      line.run(args_for(key))
    end
    @keyboard.update!(has_samples: true)
  end
  private

  def frequency_files
    @frequency_files ||= Dir.glob(Rails.root.join('tmp', 'samples', 'freqs', '*.wav'))
  end

  def frequencies
    @frequencies ||= frequency_files.map do |path|
      [File.basename(path).delete_suffix('.wav').to_f, path]
    end.sort_by(&:first)
  end

  def closest_for(frequency)
    frequencies.min_by { |freq, _path| (frequency - freq).abs }
  end

  def args_for(key)
    sample_freq, path = closest_for(key.frequency)
    mult = key.frequency / sample_freq
    {
      path: path,
      temp: mult,
      output: Rails.root.join("public", "samples", "#{@keyboard.id}", "#{key.id}.wav")
    }
  end

  def line
    Terrapin::CommandLine.new(
      'ffmpeg', '-y -i :path -af atempo=:temp,asetrate=11025*:temp :output'
    )
  end
end
