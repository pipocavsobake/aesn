class Shrine::DerivativesJob < ApplicationJob
  queue_as :shrine
  def perform(model, id, attachment_name, run_callbacks)
    record = model.find(id)
    return unless record.should_refresh_shrine_derivatives?(attachment_name)

    record.send("#{attachment_name}_derivatives!")
    if run_callbacks
      record.save!
    else
      column_name = "#{attachment_name}_data"
      record.update_column(column_name, record.send(column_name))
    end
  end
end
