class Strava::BaseJob < ApplicationJob
  queue_as :strava

  rescue_from Strava::Errors::Fault do
    retry_job wait: 15.minutes.to_i
  end

  def service
    Strava::ActivityService
  end
end
