class Strava::StreamJob < Strava::BaseJob
  def perform(activity_id, keys: Strava::StreamSet::KEYS)
    activity = Strava::Activity.find(activity_id)

    service.new(activity.user_identity).fetch_streams(activity, keys: keys)
  end
end
