class Strava::ActivityJob < Strava::BaseJob
  def perform(activity_id, keys: Strava::StreamSet::KEYS)
    activity = Strava::Activity.find(activity_id)

    service.new(activity.user_identity).fetch_activity(activity)
  end
end
