module StravaHelper
  def hhmmss(seconds)
    return nil if seconds.nil?

    Time.at(seconds).gmtime.strftime('%T').delete_prefix('00:').delete_prefix('0')
  end
end
