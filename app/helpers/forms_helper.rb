module FormsHelper
  def link_to_add_row(name, f, association, wrapper_class, view_path: , **args)
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.simple_fields_for(association, new_object, child_index: id) do |builder|
      render(view_path, f: builder)
    end
    classes = join_classes(args[:class].presence, 'js-add-has-many')
    link_to(name, '#', class: classes, data: {id: id, wrapperclass: wrapper_class, fields: fields.gsub("\n", "")})
  end

  def link_to_delete_row(tag_name, parent_class, **args, &block)
    classes = join_classes(args[:class].presence, 'js-remove-has-many')
    content_tag(tag_name, **args.merge('data-parentclass': parent_class, class: classes), &block)
  end

  def join_classes(ar, str)
    [ar, str].compact.flatten.join(' ')
  end
end
