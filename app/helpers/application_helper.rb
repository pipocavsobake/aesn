module ApplicationHelper
  def bootstrap_type(type)
    Hash[%w[notice error].zip(%w[success danger])][type] || type
  end

  def page_suffix
    params[:page].try do |page|
      page.to_i > 1 ? " — страница #{page}" : ''
    end
  end
end
