module MenuHelper
  def add_menu_item(label, url = nil, **options, &block)
    @menu_items ||= []
    @menu_items << MenuItem.new(label, url, request.path, breadcrumb_trail.map(&:path), **options, &block)
  end

  def add_menu_divider
    @menu_items ||= []
    @menu_items << MenuDivider.new
  end

  def menu_tree
    build_menu_tree if @menu_items.nil?

    @menu_items
  end

  def build_menu_tree
    add_menu_item('Главная', root_path, exact: true)
    add_menu_item('Разметка видео', video_markup_path) do |item|
      item.add_menu_item('Список видео', video_markup_videos_path)
      item.add_menu_item('Герои', video_markup_heroes_heroes_path)
      item.add_menu_item('Банки', video_markup_heroes_banks_path)
      item.add_menu_item('Юниты', video_markup_heroes_units_path)
      item.add_menu_item('Все бои', video_markup_heroes_fights_path)
    end
    add_menu_item('Разметка локальных файлов', local_markups_path)
    add_menu_item('Одноразовые ссылки', share_links_path)
    add_menu_item('Виджеты', widgets_path) do |item|
      item.add_menu_item('Клавиатуры', widgets_keyboards_path)
      item.add_menu_item('Календари', widgets_calendars_path)
    end
    add_menu_item('Тренажёры', 'https://understanding.shirykalov.ru')
    add_menu_item('Strava', strava_index_path) do |item|
      if strava_connected?
        item.add_menu_item('Активности', strava_activities_path)
      else
        item.add_menu_item('Привязать Strava', omniauth_authorize_path(:user, :strava))
      end
    end
    add_menu_item('Спорт', sport_index_path) do |item|
      item.add_menu_item('Мои тренировки', sport_activities_path)
      item.add_menu_item('Упражнения', sport_exercises_path)
      item.add_menu_item('Группы упражнений', sport_exercises_simple_groups_path)
    end
    add_menu_item('Пользователи', users_path)
    add_menu_divider
    if current_user
      add_menu_item(current_user.name, user_path(current_user)) do |item|
        item.add_menu_item('Выйти', destroy_user_session_path, method: :delete)
        if can?(:read, ActiveAdmin::Page, name: 'Dashboard')
          item.add_menu_item('Админка', admin_dashboard_path)
        end
        item.add_menu_item('Пробежки', user_runs_path(current_user))
      end
    else
      add_menu_item('Войти', new_user_session_path)
    end
  end
end
