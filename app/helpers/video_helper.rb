module VideoHelper
  def aesn_video_tag(*sources, **args)
    args[:wrapper_class] = [args[:wrapper_class], "js-video-tag d-flex"].compact.flatten.join(' ')
    klass, style = args.values_at(:wrapper_class, :wrapper_style)
    content_tag(:div, 'data-attributes': args.except(:wrapper_class, :wrapper_style).to_json, 'data-sources': sources.to_json, class: klass, style: style) do
      content_tag(:div, '', class: 'spinner-border text-dark js-video-spinner')
    end
  end
end
