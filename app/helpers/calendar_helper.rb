module CalendarHelper
  def calendar_value_style(idx)
    styles = [
      'width: 8px; height: 8px; border-radius: 4px',
      'width: 8px; height: 12px; border-radius: 4px',
      'width: 12px; height: 8px; border-radius: 4px',
    ]
    styles[idx % styles.size]
  end
end
