module SocialLinkHelper
  BOOTSTRAP_LINKS = {
    facebook: ['facebook-f', '3b5998'],
    google_oauth2: [:google, 'dd4b39', 'Google'],
    vkontakte: [:vk, '4c75a3'],
    spotify: [:spotify, 'fd7e14'],
    twitch: [:twitch, '15aabf'],
    strava: [:strava, 'fd7e14'],
  }
  def social_link(provider, url)
    return nil unless BOOTSTRAP_LINKS.key?(provider.to_sym)

    icon, bg_color, title = BOOTSTRAP_LINKS[provider.to_sym]
    title ||= OmniAuth::Utils.camelize(provider)
    render 'devise/social/bootstrap', url: url, icon: icon, bg_color: bg_color, title: title
  end
end
