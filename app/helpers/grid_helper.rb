module GridHelper
  def sort_by_orientation(list, group_sizes: [2, 3], vertical_method: )
    v_idxes = Hash.new(-1)
    h_idxes = Hash.new(-2)
    vs = 0
    hs = 0
    list.map do |item|
      vertical = if vertical_method.respond_to?(:call)
        vertical_method.call(item)
      elsif item.is_a?(Hash)
        item[vertical_method]
      else
        item.send(vertical_method)
      end
      sizes = group_sizes.each_with_object({}) do |group_size, object|
        if vertical
          if v_idxes[group_size] > h_idxes[group_size] || (vs % group_size != 0)
            v_idxes[group_size] += 1
          else
            v_idxes[group_size] = h_idxes[group_size] + 1
          end
          object[group_size] = v_idxes[group_size]
        else
          if h_idxes[group_size] > v_idxes[group_size]
            h_idxes[group_size] += 1
          else
            h_idxes[group_size] = 1 + ((group_size - (vs % group_size)) % group_size) + v_idxes[group_size]
          end
          object[group_size] = h_idxes[group_size]
        end
      end
      if vertical
        vs += 1
      else
        hs += 1
      end
      {item: item, sizes: sizes, vertical: !!vertical}
    end
  end
end
