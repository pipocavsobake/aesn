import axios from 'axios'

export const withCsrf = axios.create({
  headers: {
    'X-CSRF-Token': document.querySelector( 'meta[name=csrf-token]' ).content,
    'X-Requested-With': 'XMLHttpRequest',
  }
})

