import React from 'react';
import Plot from 'react-plotly.js';

import PropTypes from 'prop-types';

window.kaka = 12311111;

const StravaPlots = ({data, layout}) => {
  return (
    <Plot
      data={data}
      layout={layout}
      onSelecting={(event) => {console.log("selecting", event)}}
      onSelected={(event) => {console.log("selected", event)}}
      useResizeHandler={false}
      debug
    />
  );
}

export default StravaPlots;
