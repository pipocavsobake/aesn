import React from 'react';
import PropTypes from 'prop-types';
import { Button, ButtonGroup, InputGroup, Form } from 'react-bootstrap';

export const DIR_FORWARD = 1;
export const DIR_BACKWARD = -1;
const DEFAULT_CLEAN_UP_LENGTH = 60;
const DEFAULT_CLEAN_UP_STEP = 20;
const DEFAULT_LOAD_WINDOW = 10;
const DEFAULT_LOAD_COUNT = 20;

class Gallery extends React.Component {
  state = {
    idx: 0,
    items: [],
    cleanUpLength: DEFAULT_CLEAN_UP_LENGTH,
    cleanUpStep: DEFAULT_CLEAN_UP_STEP,
    loadWindow: DEFAULT_LOAD_WINDOW,
    loadCount: DEFAULT_LOAD_COUNT,
  }

  cleanUp(direction) {
    const { items, cleanUpStep, idx } = this.state;
    const { length } = items;
    if (length < this.state.cleanUpLength) {
      return;
    }
    switch(direction) {
      case DIR_FORWARD:
        return this.setState({items: items.slice(cleanUpStep), idx: idx - cleanUpStep})
      case DIR_BACKWARD:
        return this.setState({items: items.slice(0, length - cleanUpStep)})
      default:
        throw new Error(`Unsupported direction ${direction}`)
    }
  }

  decideToLoad() {
    const { loadWindow, idx, items, loadCount } = this.state;
    const { length } = items;
    if (length - idx < loadWindow) {
      this.props.loadMore(DIR_FORWARD, items[length - 1] || this.props.firstItem, loadCount).then(newItems => {
        this.setState({items: [...items, ...newItems]}, () => this.cleanUp(DIR_FORWARD));
      })
      return;
    }
    if (idx < loadWindow) {
      this.props.loadMore(DIR_BACKWARD, items[0], loadCount).then(newItems => {
        this.setState({items: [...newItems, ...items], idx: this.state.idx + newItems.length}, () => this.cleanUp(DIR_BACKWARD));
      })
    }
  }

  step(direction) {
    const {idx} = this.state;
    const previousItem = this.state.items[idx]
    this.setState({idx: idx + direction}, () => {
      const item = this.state.items[this.state.idx];
      this.props.onChange?.({item, previousItem, direction});
      this.decideToLoad();
    });
  }

  next = () => this.step(DIR_FORWARD);
  previous = () => this.step(DIR_BACKWARD);

  renderItem = (item, idx) => {
    const Item = this.props.component;
    const current = idx === this.state.idx
    return (
      <div style={{display: current ? 'block' : 'none'}} key={`${item.key}`}>
        <Item item={item} fixUrl={this.props.fixUrl} next={this.next} previous={this.previous} current={current} />
      </div>
    )
  }

  componentDidMount() {
    this.decideToLoad();
  }

  renderSettingsButton() {
    return (
      <Button
        className="mr-2"
        onClick={() => this.setState({settings: !this.state.settings})}
      >
        {this.state.settings ? 'Скрыть настройки' : 'Открыть настройки'}
      </Button>
    )
  }

  renderSettings() {
    if (!this.state.settings) {
      return null;
    }
    const schema = [
      {key: 'cleanUpLength', label: 'Число элементов в памяти'},
      {key: 'cleanUpStep', label: 'Количество одновременно удаляемых элементов'},
      {key: 'loadWindow', label: 'Сколько элементов до края, чтобы грузить новые'},
      {key: 'loadCount', label: 'Сколько элементов грузить за раз'},
    ];

    return (
      <div className="mb-2">
        <ButtonGroup className="mb-2">
          <Button
            onClick={() => this.setState({useProxy: !this.state.useProxy})}
          >
            {this.state.useProxy ? 'Выключить proxy' : 'Включить proxy'}
          </Button>
        </ButtonGroup>
        {schema.map(({key, label})=> (
          <Form.Group className="mb-2" key={key}>
            <Form.Label>{label}</Form.Label>
            <Form.Control
              type="number"
              value={this.state[key]}
              onChange={({target: {value}}) => this.setState({[key]: +value})}
            />
          </Form.Group>
        ))}
      </div>
    )
  }

  renderAction(action, item) {
    return (
      <Button
        className="mr-2"
        key={`key_${action.value}`}
        disabled={action.show && !action.show(item)}
        onClick={() => this.props?.onAction({action: action.value, item})}
      >
        {typeof action.label === 'function' ? action.label(item) : action.label}
      </Button>
    )
  }

  renderActions() {
    const item = this.state.items[this.state.idx];

    return this.props.actions?.map(
      action => this.renderAction(action, item)
    );
  }

  render() {
    return (
      <div>
        <ButtonGroup className="mb-2">
          <Button
            className="mr-2"
            disabled={this.props.disabled || this.state.idx === 0}
            onClick={this.previous}
          >
              Назад
          </Button>
          <Button
            className="mr-2"
            disabled={this.props.disabled}
            onClick={this.next}
          >
              Вперёд
          </Button>
          {this.renderActions()}
          {this.renderSettingsButton()}
        </ButtonGroup>
        {this.renderSettings()}
        <div style={{width: '90%', overflow: 'auto', maxHeight: '90%'}}>
          <div style={{display: 'flex'}}>
            {this.state.items.map(this.renderItem)}
          </div>
        </div>
      </div>
    )
  }
}

Gallery.propTypes = {
  loadMore: PropTypes.func.isRequired,
  component: PropTypes.elementType.isRequired,
  fixUrl: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  firstItem: PropTypes.shape({key: PropTypes.number.isRequired}),
  actions: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.func,
      ]).isRequired,
      value: PropTypes.string.isRequired,
      show: PropTypes.func,
    }),
  )
}

export default Gallery;
