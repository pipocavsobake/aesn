import React from "react"
import { hot } from 'react-hot-loader/root';
import PropTypes from "prop-types"
import Video from './video/Player';
import Form from './markup/Form';
import MarkupList from './markup/List';
import axios from 'axios';
import sortBy from 'lodash-es/sortBy'

import { Container, Row, Col } from 'react-bootstrap';

import { HeroesContext } from '~/contexts';

class VideoMarkup extends React.Component {
  videoWrapper = React.createRef();
  state = {
    data: {
      heroes: [],
      units: [],
      banks: [],
      spells: [],
      grounds: [],
      skills: [],
      heroes_hash: {},
      units_hash: {},
      banks_hash: {},
      spells_hash: {},
      grounds_hash: {},
      skills_hash: {},
      moments: [],
    },
    timestamps: [],
  }
  onMarkup = (ts) => {
    this.setState({
      timestamps: [...this.state.timestamps, ts]
    })
  }

  componentDidMount() {
    this.setWidth();
    this.loadValues({url: '/admin/video_markup_heroes_heroes.json', dataKey: 'heroes'});
    this.loadValues({url: '/admin/video_markup_heroes_units.json', dataKey: 'units'});
    this.loadValues({url: '/admin/video_markup_heroes_banks.json', dataKey: 'banks'});
    this.loadValues({url: '/admin/video_markup_heroes_spells.json', dataKey: 'spells'});
    this.loadValues({url: '/admin/video_markup_heroes_grounds.json', dataKey: 'grounds'});
    this.loadValues({url: '/admin/video_markup_heroes_skills.json', dataKey: 'skills'});
    this.loadMoments();
  }

  setWidth() {
    this.setState({width: this.videoWrapper.current.offsetWidth});
  }

  loadValues({url, dataKey, perPage}) {
    const per_page = perPage || 200;
    axios.get(url, { params: {per_page}}).then(resp => {
      const hash = {};
      for (let item of resp.data) {
        hash[item.id] = item;
      }
      this.setState({
        data: {
          ...this.state.data,
          [dataKey]: sortBy(resp.data, e => -(e.enabled_fights_count || 0)),
          [`${dataKey}_hash`]: hash,
        }
      })
    }).catch(err => {
      console.error(err);
    })
  }

  loadMoments() {
    axios.get(
      '/admin/video_markup_moments.json',
      {
        params: {
          'q[video_id_equals]': this.props.videoId,
          per_page: 1000,
          order: 'value_desc',
        },
      }
    ).then(resp => {
      this.setState({data: {...this.state.data, moments: resp.data}});
    }).catch(err => {
      console.error(err);
    })
  }

  removeTs(idx, ops) {
    this.setState({
      timestamps: [...this.state.timestamps.slice(0, idx), ...this.state.timestamps.slice(idx + 1)]
    })
    if (ops && ops.play) {
      this.videoPlayer.play();
    }
    this.loadMoments();
  }

  selectMoment = (moment, moment_idx) => {
    //console.log(moment);
    this.setState({moment_idx});
    this.videoPlayer.seekTo(moment.value);
  }

  onPlayer = (player) => {
    this.videoPlayer = player;
  }

  onKeydown = (event) => {
    if (this.state.timestamps.length > 0) {
      return;
    }
    switch(event.key) {
      case " ":
        event.preventDefault();
        this.videoPlayer.toggle();
    }
  }

  onEditFormDone = () => {
    this.setState({moment_idx: null});
    this.loadMoments();
  }

  getTs = () => {
    return this.videoPlayer.getCurrentTime();
  }

  render () {
    const { slug, type, videoId } = this.props;
    const moment = this.state.data.moments[this.state.moment_idx];
    const fight = moment?.heroes_fights?.[0];
    return (
      <HeroesContext.Provider value={this.state.data}>
        <Container fluid onKeyDown={this.onKeydown} tabIndex="0">
          <Row>
            <Col lg="8" xl="9" ref={this.videoWrapper}>
              {this.state.width ? <Video width={this.state.width - 20} slug={slug} streamerSlug={this.props.streamerSlug} type={type} onMarkup={this.onMarkup} onReady={this.onPlayer} /> : null }
            </Col>
            <Col lg="4" xl="3">
              {this.state.timestamps.map((ts, idx) => (
                <Form videoId={videoId} ts={ts} key={idx} onDone={(ops) => this.removeTs(idx, ops)} getTs={this.getTs} />
              ))}
              {fight?.can?.update || moment?.can?.update ? (
                <Form videoId={videoId} ts={moment.value} key={moment.value} onDone={this.onEditFormDone} momentId={moment.id} moment={moment} getTs={this.getTs} />
              ) : null}
            </Col>
          </Row>
          <Row>
            <Col>
              <MarkupList moments={this.state.data.moments} onClick={this.selectMoment} />
            </Col>
          </Row>
        </Container>
      </HeroesContext.Provider>
    );
  }
}
VideoMarkup.propTypes = {
  type: PropTypes.string,
  slug: PropTypes.string,
  streamerSlug: PropTypes.string,
  videoId: PropTypes.number,
};

export default hot(VideoMarkup);
