import React from 'react';
import PropTypes from 'prop-types';
import RunButton from './RunButton';

class Audio extends React.Component {
  render() {
    return (
      <>
        <audio src={this.props.path} style={{width: '100%'}} controls/>
        {this.props.hideActions ?  null :
          <RunButton className="mr-2" path={this.props.path} />
        }
      </>
    )
  }
}

Audio.propTypes = {
  path: PropTypes.string.isRequired,
}

export default Audio;
