import React from 'react';
const Image = ({path}) => {
  return (
    <div>
      <img src={path} height="600" />
    </div>
  )
}

export default Image;
