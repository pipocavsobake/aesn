import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';
import axios from 'axios';

class RunButton extends React.Component {
  state = {};
  play = () => {
    const url = new URL(this.props.path);
    url.search = new URLSearchParams({file: url.pathname}).toString();
    url.pathname = `/open`;
    axios.get(url.toString()).then(()=>{}).catch(err => {
      console.error(err);
      this.setState({error: err.response.body});
    })
  }
  render() {
    const {path, ...props} = this.props;
    if (this.state.error) {
      return <div>{this.state.error}</div>;
    }
    return (
      <Button
        title="Работает только если данный файл на вашем локальном устройстве"
        onClick={this.play}
        {...props}
      >Открыть в приложении по умолчанию</Button>
    )
  }
}

RunButton.propTypes = {
  path: PropTypes.string.isRequired,
}

export default RunButton;
