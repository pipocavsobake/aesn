import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

class TextFile extends React.Component {
  state = {
    error: null,
    content: null,
  }
  componentDidMount() {
    this.loadContent();
  }
  loadContent() {
    axios.get(this.props.path).then(resp => {
      this.setState({content: resp.data});
    }).catch(err => {
      this.setState({error: err.message});
    })
  }
  render() {
    if (this.state.error) {
      return (
        <pre>
          {this.state.error}
        </pre>
      )
    }
    if (!this.state.content) {
      return (
        <pre>
          Loading...
        </pre>
      )
    }
    return (
      <pre>
        {this.state.content}
      </pre>
    )
  }
}

TextFile.propTypes = {
  path: PropTypes.string.isRequired,
}

export default TextFile;
