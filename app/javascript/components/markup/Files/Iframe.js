import React from 'react';

export default (props) => {
  return (
    <iframe src={props.path} width="800" height="600"/>
  )
}
