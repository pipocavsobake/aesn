import React from 'react';
import { PropTypes } from 'prop-types';
import { LocalContext } from '~/contexts';
import CueParsed from './CueParsed';
import RunButton from './RunButton';

class Cue extends React.Component {
  state = {};
  static contextType = LocalContext;
  componentDidMount() {
    this.parse();
  }
  parse() {
    const parsed = this.context.cue[this.props.path];
    if (parsed) {
      this.setState({parsed});
    } else {
      this.setState({error: `Не могу распознать ${this.props.path}`})
    }
  }
  render() {
    if (this.state.error) {
      return <div>Ошибка {this.state.error}</div>
    }
    if (!this.state.parsed) {
      return <div>Пытаюсь распознать</div>
    }
    return (
      <div>
        <RunButton path={this.props.path} />
        <CueParsed onSetSecond={this.props.onSetSecond} {...this.state.parsed} />
        <pre>
          {JSON.stringify(this.state.parsed)}
        </pre>
      </div>
    )
  }
}

Cue.propTypes = {
  path: PropTypes.string.isRequired,
  onSetSecond: PropTypes.func.isRequired,
}

export default Cue;
