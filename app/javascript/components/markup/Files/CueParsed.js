import React from 'react';
import PropTypes from 'prop-types';
import { Card, ListGroup, ListGroupItem, ButtonGroup, Button } from 'react-bootstrap';

class CueParsed extends React.Component {
  onClick = (file, track, min, sec) => {
    this.props.onSetSecond(((+min) * 60) + (+sec))
  }
  render() {
    return(
      <Card>
        <Card.Body>
          <Card.Title>
            {this.props.title}
          </Card.Title>
          <Card.Subtitle>
            {this.props.performer}
          </Card.Subtitle>
        </Card.Body>
        {this.props.files.map(file => (
          <React.Fragment key={file.name }>
            <Card.Text className="mt-3" >{file.name}</Card.Text>
            <ListGroup>
              {file.tracks.map(track => (
                <ListGroupItem key={track.number}>
                  <h4>{track.title}</h4>
                  <p>{track.performer}</p>
                  <ButtonGroup>
                    {track.indexes.filter(({time}) => time).map(({time: {min, sec}}) => (
                      <Button key={`${min}:${sec}`} onClick={() => this.onClick(file, track, min, sec)}>{`${min}:${sec}`}</Button>
                    ))}
                  </ButtonGroup>
                </ListGroupItem>
              ))}
            </ListGroup>
          </React.Fragment>
        ))}
      </Card>
    )
  }
}
const trackType = PropTypes.shape({
  title: PropTypes.string,
  performer: PropTypes.string,
  indexes: PropTypes.arrayOf(
    PropTypes.shape({
      time: PropTypes.shape({
        min: PropTypes.number,
        sec: PropTypes.number,
      })
    })
  ),
  number: PropTypes.number,
});

CueParsed.propTypes = {
  files: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      tracks: PropTypes.arrayOf(trackType),
    })
  ),
  title: PropTypes.string,
  performer: PropTypes.string,
  encoding: PropTypes.string,
  track: trackType,
  onSetSecond: PropTypes.func.isRequired,
}

export default CueParsed;
