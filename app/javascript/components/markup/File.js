import React from 'react';
import PropTypes from 'prop-types';
import Audio from './Files/Audio';
import Iframe from './Files/Iframe';
import Default from './Files/Default';
import TextFile from './Files/TextFile';
import Cue from './Files/Cue';
import Image from './Files/Image';
import { OverlayScrollbarsComponent } from 'overlayscrollbars-react';

import { Button, Card } from 'react-bootstrap';
const extname = path => {
  const a = path.split('.');
  return `.${a[a.length - 1]}`.toLowerCase();
}

class File extends React.Component {
  fileComponent() {
    try {
      const url = new URL(this.props.path);
      if (url.hostname !== 'localhost' && !this.props.strict) {
        return Iframe;
      }
      const ext = extname(this.props.path);
      switch(ext) {
        case '.mp3':
        case '.wav':
        case '.ape':
        case '.flac':
          return Audio;
        case '.cue':
          return Cue;
        case '.jpg':
        case '.png':
        case '.gif':
          return Image;
        case '.txt':
        case '.m3u8':
          return TextFile;
        default:
          return Default;
      }
    } catch(e) {
      console.error(e);
      return Default;
    }
  }
  render() {
    const FileComponent = this.fileComponent();
    const parts = this.props.path.split('/');
    const basename = parts[parts.length - 1];
    return (
      <Card className="mb-4">
        <Card.Header>
          <h4>{basename}</h4>
          <pre>
            {this.props.path}
          </pre>
        </Card.Header>
        <Card.Body>
          <OverlayScrollbarsComponent style={{height: '50vh'}} className="mb-4">
            <FileComponent {...this.props} />
          </OverlayScrollbarsComponent>
        </Card.Body>
        <Card.Footer>
          <Button className="mr-2" href={this.props.path} target="_blank">Скачать</Button>
          {this.props.hideActions ? null :
            <Button
              onClick={this.props.onTogglePin}
            >
              {this.props.isPinned ? 'Открепить' : 'Закрепить'}
            </Button>
          }
        </Card.Footer>
      </Card>
    )
  }
}

File.propTypes = {
  path: PropTypes.string.isRequired,
  isPinned: PropTypes.bool,
  onTogglePin: PropTypes.func,
  onSetSecond: PropTypes.func,
  strict: PropTypes.bool,
  hideActions: PropTypes.bool,
}

export default File;
