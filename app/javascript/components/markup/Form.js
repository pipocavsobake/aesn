import React from "react"
import PropTypes from "prop-types"
import Select from 'react-select';

import Card from 'react-bootstrap/Card';

import FightForm from './FightForm';
import GameStartForm from './GameStartForm';
import CommentForm from './CommentForm';
import LearnSkillForm from './LearnSkillForm';


const models = [
  { 'value': FightForm, label: 'Heroes Битва' },
  { 'value': GameStartForm, label: 'Heroes Начало катки' },
  { 'value': LearnSkillForm, label: 'Обучение навыку' },
  { 'value': CommentForm, label: 'Комментарий' },
];

class Form extends React.Component {
  state = {
    model: models[0],
  }
  renderForm() {
    const FormComponent = this.state.model.value;
    return <FormComponent {...this.props} />
  }
  componentDidMount() {
    this.setModel();
  }
  componentDidUpdate(prevProps) {
    if (this.props.moment !== prevProps.moment) {
      this.setModel();
    }
  }

  setModel() {
    if (this.props.moment?.heroes_learn_skills?.length) {
      this.setState({model: models[2]});
    }
  }
  render () {
    return (
      <Card>
        <Card.Body>
          <Select
            value={this.state.model}
            onChange={(model) => {this.setState({model: model})}}
            options={models}
          />
        </Card.Body>
        <Card.Body>
          {this.renderForm()}
        </Card.Body>
      </Card>
    );
  }
}

Form.propTypes = {
  ts: PropTypes.number,
  videoId: PropTypes.number,
  onDone: PropTypes.func,
  momentId: PropTypes.number,
}

export default Form
