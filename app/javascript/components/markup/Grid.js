import React from 'react';
import PropTypes from 'prop-types';
import {
  ListGroup,
  ButtonGroup,
  Button,
  Badge,
  Image,
  Row,
  Col,
} from 'react-bootstrap';

const extname = path => {
  const a = path.split('.');
  return `.${a[a.length - 1]}`.toLowerCase();
}

class Grid extends React.Component {
  imageSrc(file) {
    if (!this.props.proxy) {
      return file;
    }
    const url = new URL(this.props.proxy);
    url.search = (new URLSearchParams({url: file})).toString();
    return url.toString();
  }
  renderImage(file) {
    return <Image src={this.imageSrc(file)} width={300} />
  }
  render() {
    return (
      <Row>
        {this.props.files.filter(fn => ['.jpg', '.png', '.gif'].includes(extname(fn))).map((file, idx) => (
          <Col key={file} onClick={() => this.props.onAction({file})}>
            {this.renderImage(file)}
          </Col>
        ))}
      </Row>
    )
  }
}

Grid.propTypes = {
  proxy: PropTypes.string,
  files: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedHash: PropTypes.objectOf(PropTypes.bool).isRequired,
  onAction: PropTypes.func.isRequired,
  actions: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.func,
      ]).isRequired,
      value: PropTypes.string.isRequired,
      show: PropTypes.func,
    }),
  )
}

export default Grid;
