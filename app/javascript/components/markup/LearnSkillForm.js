import React from 'react';
import PropTypes from 'prop-types';
import { FormControl, InputGroup, Button, ListGroup } from 'react-bootstrap';
import classNames from 'classnames';
import { HeroesContext } from '~/contexts';
import { withCsrf as axios } from '~/axios-headers';
import Select from 'react-select';
import orderBy from 'lodash-es/orderBy';

const placeholders = {
  skill: 'Вторичный навык',
  level: 'lvl',
  attack: 'A',
  defence: 'D',
  spell_power: 'SP',
  knowledge: 'KN',
}
const primaries = [
  'attack',
  'defence',
  'spell_power',
  'knowledge',
]

export class LearnSkillsForm extends React.Component {
  state = {
    forms: [],
    form: {},
  }
  buildPayload({saveTs}) {
    const {
      momentId,
      ts,
      videoId,
    } = this.props;

    const heroes_learn_skills_attributes = this.state.forms.map(form => {
      const {
        id,
        hero,
        attack,
        defence,
        spell_power,
        knowledge,
        skill,
        level,
        _destroy,
      } = form;
      return {
        id,
        hero_id: hero?.value,
        attack,
        defence,
        spell_power,
        knowledge,
        skill_id: skill?.value,
        level,
        _destroy,
      }
    });
    const video_markup_moment = {
      value: ts,
      video_id: videoId,
      heroes_learn_skills_attributes,
    }
    if (saveTs) {
      video_markup_moment.stop_at = ~~this.props.getTs();
    }
    return { video_markup_moment };
  }
  setMomentData() {
    const {
      moments,
      heroes_hash,
      skills_hash,
    } = this.props.heroesData;
    const moment = moments.find(m => m.id === this.props.momentId);
    if (!moment) {
      return;
    }
    const forms = moment.heroes_learn_skills.map(
      ({
        id,
        hero_id,
        attack,
        defence,
        spell_power,
        knowledge,
        skill_id,
        level,
      }) => ({
        id,
        hero: heroes_hash[hero_id],
        attack,
        defence,
        spell_power,
        knowledge,
        skill: skill_id && skills_hash[skill_id],
        level,
      })
    );
    this.setState({forms});
  }

  setPreviousHero = () => {
    if (this.state.forms.length) {
      return;
    }
    const {
      moments,
      heroes_hash,
    } = this.props.heroesData;
    const ordered = orderBy(moments, ["value"], ["desc"]);
    const moment = ordered.find(m => m.heroes_fights?.length || m.heroes_learn_skills?.length);
    if (!moment) {
      return;
    }
    const { hero_id } = ([...moment.heroes_fights, ...moment.heroes_learn_skills])[0];
    const hero = heroes_hash[hero_id];
    this.setState({
      last_hero: hero,
      forms: [{
        hero,
      }]
    });
  }

  componentDidMount() {
    if (this.props.momentId) {
      this.setMomentData();
    } else {
      setTimeout(this.setPreviousHero, 0)
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.momentId !== prevProps.momentId) {
      this.setMomentData();
    }
    if (this.state.submitted && prevState.submitted) {
      this.setState({submitted: false});
    }
  }
  onDelete = () => {
    this.props.onDone();
  }
  errors() {
    const errors = {};
    if (!this.state.submitted) {
      return {};
    }
    this.state.forms.forEach(({hero, skill, level, _destroy}, idx) => {
      if(_destroy) {
        return;
      }
      const errs = {};
      if (skill && (level < 1 || level > 3)) {
        errs.level = true;
      }
      if (!hero?.value) {
        errs.hero = true;
      }
      if (Object.keys(errs).length) {
        errors[idx] = errs;
      }
    })
    return errors;
  }

  endPoint() {
    if (this.props.momentId) {
      return `/admin/video_markup_moments/${this.props.momentId}`;
    }
    return '/admin/video_markup_moments';
  }

  onSave = ({play, saveTs}) => {
    if (this.state.submitted) {
      return;
    }
    this.setState({submitted: true}, () => {
      if (Object.keys(this.errors()).length > 0) {
        return;
      }
      const method = this.props.momentId ? 'put' : 'post';
      axios[method](this.endPoint(), this.buildPayload({saveTs})).then(resp => {
        this.props.onDone({play});
      }).catch(err => {
        this.setState({submitted: false});
        alert('Не удалось сохранить')
        console.error(err)
      })
    })
  }

  renderNewLearnSkillsBtn() {
    return (
      <InputGroup className="mb-2">
        <Button
          onClick={() => this.setState({forms: [...this.state.forms, {hero: this.state.forms[0]?.hero}]})}
        >
          Добавить
        </Button>
      </InputGroup>
    )
  }

  renderSelect = (form, idx, field, options) => {
    const error = this.errors()?.[idx]?.[field];
    return (
      <Select
        styles={ {container: base => ({...base, flex: 1})}}
        className={classNames({border: error, 'border-danger': error})}
        options={options}
        onChange={
          value => (
            this.setState({
              forms: [
                ...this.state.forms.slice(0, idx),
                {
                  ...form,
                  [field]: value,
                },
                ...this.state.forms.slice(idx + 1)
              ]
            })
          )}
        value={form[field]}
        placeholder={placeholders[field]}
      />
    );
  }

  renderInput(form, idx, field, props = {}) {
    return (
      <FormControl
        onChange={
          ({target: {value}}) => (
            this.setState({
              forms: [
                ...this.state.forms.slice(0, idx),
                {
                  ...form,
                  [field]: value,
                },
                ...this.state.forms.slice(idx + 1),
              ]
            })
          )}
        value={form[field] || ''}
        placeholder={placeholders[field]}
        {...props}
      />
    )
  }

  renderLearnSkill = (form, idx) => {
    const { heroes, skills } = this.props.heroesData;
    return (
      <ListGroup.Item className="mb-3" key={idx}>
        <InputGroup className="mb-2"> {this.renderSelect(form, idx, 'hero', heroes)} </InputGroup>
        <InputGroup className="mb-2">
          {this.renderSelect(form, idx, 'skill', skills)}
          {form.skill ? this.renderInput(form, idx, 'level', {type: 'number'}) : null}
        </InputGroup>
        <InputGroup className="mb-2">
          {primaries.map(field => (
            this.renderInput(
              form,
              idx,
              field,
              {
                type: 'number',
                key: field,
                className: 'mr-2'
              }
            )
          ))}
        </InputGroup>
        <InputGroup className="mb-2">
          <Button
            onClick={
              () => this.setState({
                forms: [
                  ...this.state.forms.slice(0, idx),
                  ...(form.id ? [{id: form.id, _destroy: true}] : []),
                  ...this.state.forms.slice(idx + 1)
                ]
              })}
          >
            Удалить
          </Button>
        </InputGroup>
      </ListGroup.Item>
    )
  }

  renderLearnSkills() {
    return (
      <ListGroup className="mb-3">
        {this.state.forms.filter(({_destroy}) => !_destroy).map(this.renderLearnSkill)}
      </ListGroup>
    )
  }

  renderActions() {
    const errors = this.errors();
    return (
      <InputGroup>
          <Button disabled={Object.keys(errors).length > 0 || this.state.submitted} variant="success" className="mr-2 mb-2" onClick={this.onSave}>Сохранить</Button>
          <Button disabled={Object.keys(errors).length > 0 || this.state.submitted} variant="success" className="mr-2 mb-2" onClick={() => this.onSave({saveTs: true})}>Сохранить с временем окончания</Button>
          <Button variant="danger" disabled={this.state.submitted} onClick={this.onDelete}>{this.props.momentId ? 'Отменить' : 'Удалить'}</Button>
      </InputGroup>
    )
  }

  render() {
    return (
      <>
        {this.renderLearnSkills()}
        {this.renderNewLearnSkillsBtn()}
        {this.renderActions()}
      </>
    )
  }
}

class LearnSkillsFormC extends React.Component {
  static contextType = HeroesContext;
  render() {
    return <LearnSkillsForm {...this.props} heroesData={this.context} />
  }
}
export default LearnSkillsFormC;
