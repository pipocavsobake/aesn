import React from 'react';
import PropTypes from 'prop-types';
import Gallery, { DIR_FORWARD, DIR_BACKWARD } from '~/components/Gallery';
import { submit } from './LocalForm';
import { FormControl } from 'react-bootstrap';

const extensions = ['.jpeg', '.jpg', '.png', '.gif'];

class Item extends React.PureComponent {
  render(){
    const {item, fixUrl} = this.props;
    let entry;
    if (extensions.find(ext => item.file.endsWith(ext))) {
      entry =  <iframe src={fixUrl(item.file)} height="800" width="900" />
    } else {
      entry = <div style={{height: '800px'}}>Не поддерживаемый формат файла {item.file}</div>
    }
    return (
      <div>
        <div>{item.idx} / {item.total}; CheckPoint({item.checkPointIdx}): {item.checkPoint} :: {item.file}</div>
        {entry}
      </div>
    )
  }
}

class GalleryProxy extends React.Component {
  state = {}
  loadMore = (direction, item = {key: -1}, count) => {
    return new Promise((resolve, reject) => {
      switch(direction) {
        case DIR_FORWARD:
          return resolve(
            this.props.flatFiles.slice( item.key + 1, item.key + 1 + count).map(
                (newItem, idx) => ({...newItem, key: idx + item.key + 1, file: newItem.file})
              )
          );
        case DIR_BACKWARD:
          return resolve(
            this.props.flatFiles.slice(item.key - count, item.key).map(
              (newItem, idx) => ({...newItem, key: idx + item.key - count})
            ));
        default:
          throw new Error(`Unexpected direction ${direction}`);
      }
    })
  }
  onChange = ({item, previousItem, direction}) => {
    if ((!item || item.idx === 0) && direction === DIR_FORWARD) {
      this.setState({disabled: true});
      if (this.props.checkPoint?.processed_at) {
        this.props.setIdx(item.checkPointIdx);
      }
      return;
    }
    if (item.checkPointIdx !== this.props.idx) {
      this.props.setIdx(item.checkPointIdx);
    }
  }
  componentDidMount() {
    setTimeout(() => {
      this.setState({
        firstItem: {
          key: this.props.flatFiles.findIndex(({checkPointIdx}) => checkPointIdx === this.props.idx)
        }
      });
    }, 0)
  }
  componentDidUpdate(prevProps) {
    if (prevProps.idx !== this.props.idx) {
      this.setState({disabled: false})
    }
  }
  onAction = (data) => {
    const {item, action} = data;
    switch(action) {
      case 'ok':
        if (item && item.idx !== 0 && !confirm('Вы уверены?')) {
          return;
        }
        return this.props.releaseCheckPoint();
      case 'mark':
        this.setState({submitting: true});
        submit({
          file: item.file,
          relatedFiles: this.props.files[item.checkPoint],
          markup_id: this.props.markup.id,
          check_point_file: item.checkPoint,
        }, {}).then(() => {
          this.props.loadCheckPoint().then(() => {
            this.setState({submitting: false});
          })
        }).catch(err => {
          console.error(err);
        })
        return;
      default:
        throw new Error(`unknown action ${action}`);
    }
  }
  fixUrl = (url) => {
    const { baseUrl } = this.state;
    if (!baseUrl || !url.startsWith('/') || url.startsWith('//')) {
      return url;
    }
    try {
      const uri = new URL(baseUrl);
      uri.pathname = url;
      return uri.toString();
    } catch(e) {
      this.setState({baseUrlError: e.message});
      return url;
    }
  }
  renderSettings() {
    return (
      <FormControl
        className="mb-2"
        onChange={({target: {value}}) => this.setState({baseUrl: value})}
        value={this.state.baseUrl || ''}
        placeholder="Базовый урл"
      />
    );
  }
  renderMeta() {
    const key = this.props.keys[this.props.idx];
    if (!this.props.meta || !this.props.meta[key]) {
      return null;
    }
    return (
      <pre>
        {JSON.stringify(this.props.meta[key])}
      </pre>
    )
  }
  render() {
    if (!this.state.firstItem) {
      return <div>Гружу</div>
    }
    const actions = [
      {label: 'Отметить', value: 'mark', show: (file) => !this.state.submitting && !this.state.disabled && !(this.props.checkPoint?.items?.find(item => item.file === file?.file)) && !file?.file?.match(/\/$/)},
      {label: `Окнуть чекпоинт ${this.props.idx}/${this.props.keys.length}`, show: () => !(this.props.checkPoint?.processed_at || this.props.checkPointPending), value: 'ok'},
    ];
    return (
      <>
        {this.renderSettings()}
        {this.renderMeta()}
        <Gallery
          loadMore={this.loadMore}
          component={Item}
          fixUrl={this.fixUrl}
          disabled={this.state.disabled}
          firstItem={this.state.firstItem}
          onChange={this.onChange}
          onAction={this.onAction}
          actions={actions}
        />
      </>
    )
  }
}

GalleryProxy.propTypes = {
  flatFiles: PropTypes.arrayOf(
    PropTypes.shape({
      file: PropTypes.string,
      checkPoint: PropTypes.string,
    })
  )
}

export default GalleryProxy;
