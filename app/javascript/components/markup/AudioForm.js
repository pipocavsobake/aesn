import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

import AsyncSelect from 'react-select/async';
import { Button, InputGroup, FormControl } from 'react-bootstrap';

const searchUrl = 'https://vsobake.ru/api/search/'

const { CancelToken } = axios;
const nullToStr = v => (v || v === 0) ? `${v}` : '';

class AudioForm extends React.Component {
  state = {
    form: {
      composers: [],
      performers: [],
    },
  }

  load = (kind, inputValue, callback, params={}) => {
    const cancelKey = `cancel_${kind}`;
    if (this[cancelKey]) {
      this[cancelKey]();
    }

    delete axios.defaults.headers.common['X-CSRF-Token'];
    delete axios.defaults.headers.common['X-Requested-With'];
    axios.get(
      searchUrl + kind, {
        params: {...params, q: inputValue},
        cancelToken: new CancelToken(cancel => this[cancelKey] = cancel),
      }).then(({data})=> {
        callback(data.map(({id, name}) => ({label: name, value: id})));
    }).catch(err => {
      if (axios.isCancel(err)) {
        return;
      }
      console.error(err);
      this.setState({errors: {...this.state.errors, [kind]: 'Сервер вернул ошибку'}})
    }).then(() => this[cancelKey] = null);
  }
  loadComposers = (inputValue, callback) => {
    this.load('composers', inputValue, callback);
  }
  loadPerformers = (inputValue, callback) => {
    this.load('performers', inputValue, callback);
  }
  loadPieces = (inputValue, callback) => {
    this.load('pieces', inputValue, callback, {'where[composer_id]': this.state.form.composer?.value});
  }

  onSubmit = () => {
    this.props.onSubmit(this.buildPayload())
  }

  buildPayload() {
    const {
      composer,
      composer_name,
      performer,
      performer_name,
      piece,
      piece_name,
      composers,
      composer_names,
      performers,
      performer_names,
      part_number,
      part_name,
    } = this.state.form;
    return {
      composer_id: composer?.value,
      performer_id: performer?.value,
      piece_id: piece?.value,
      part_number,
      composer_ids: composers?.map(({value}) => value),
      performer_ids: performers?.map(({value}) => value),
    }
  }

  addComposer = () => {
    this.setState({
      form: {
        ...this.state.form,
        composers: [
          ...this.state.form.composers,
          null,
        ]
      }
    })
  }

  addPerformer = () => {
    this.setState({
      form: {
        ...this.state.form,
        performers: [
          ...this.state.form.performers,
          null,
        ]
      }
    })
  }

  render() {
    return (
      <div className="mb-5">
        <InputGroup
          className="mb-2"
        >
          <AsyncSelect
            styles={ {container: base => ({...base, flex: 1})}}
            isDisabled={this.props.disabled}
            value={this.state.form.composer}
            loadOptions={this.loadComposers}
            defaultOptions={[]}
            onChange={composer => this.setState({form: {...this.state.form, composer}})}
            placeholder="Композитор"
          />
          <FormControl
            disabled={this.props.disabled}
            value={nullToStr(this.state.form.composer_name)}
            onChange={(event) => this.setState({form: {...this.state.form, composer_name: event.target.value}})}
            placeholder='Как он записан в источнике'
          />
          {
            this.state.form.composers?.length ? null :
            <Button
              disabled={this.props.disabled}
              className="ml-2"
              onClick={this.addComposer}
            >
              Добавить композитора
            </Button>
          }
        </InputGroup>
        {
          this.state.form.composers?.map((composer, idx) => (
            <InputGroup
              key={idx}
              className="mb-2"
            >
              <AsyncSelect
                styles={ {container: base => ({...base, flex: 1})}}
                isDisabled={this.props.disabled}
                value={composer}
                loadOptions={this.loadComposers}
                defaultOptions={[]}
                onChange={composer => this.setState({form: {...this.state.form, composers: [...this.state.form.composers.slice(0, idx), composer, ...this.state.composers.slice(idx+1)]}})}
                placeholder="Со-композитор"
              />
              <FormControl
                disabled={this.props.disabled}
                value={nullToStr(this.state.form.composer_names?.[idx])}
                onChange={(event) => this.setState({form: {...this.state.form, composer_names: [...(this.state.form.composer_names || []).slice(idx), event.target.value, ...(this.state.form.composer_names || []).slice(idx + 1)]}})}
                placeholder='Как он записан в источнике'
              />

              <Button
                className="ml-2"
                disabled={this.props.disabled}
                onClick={() => this.setState({form: {...this.state.form, composers: [...this.state.form.composers.slice(0, idx), ...this.state.form.composers.slice(idx + 1)]}})}
              >
                  Удалить
              </Button>
              {
                (this.state.form.composers.length > idx + 1) ? null :
                <Button
                  disabled={this.props.disabled}
                  className="ml-2"
                  onClick={this.addComposer}
                >
                  Добавить композитора
                </Button>
              }
            </InputGroup>
          ))
        }
        <InputGroup
          className="mb-2"
        >
          <AsyncSelect
            styles={ {container: base => ({...base, flex: 1})}}
            isDisabled={this.props.disabled}
            value={this.state.form.performer}
            loadOptions={this.loadPerformers}
            defaultOptions={[]}
            onChange={performer => this.setState({form: {...this.state.form, performer}})}
            placeholder="Исполнитель"
          />
          <FormControl
            disabled={this.props.disabled}
            value={nullToStr(this.state.form.performer_name)}
            onChange={(event) => this.setState({form: {...this.state.form, performer_name: event.target.value}})}
            placeholder='Как он записан в источнике'
          />

          {
            this.state.form.performers?.length ? null :
            <Button
              disabled={this.props.disabled}
              className="ml-2"
              onClick={this.addPerformer}
            >
              Добавить Исполнителя
            </Button>
          }
        </InputGroup>
          {
            this.state.form.performers?.map((performer, idx) => (
              <InputGroup
                key={idx}
                className="mb-2"
              >
                <AsyncSelect
                  styles={ {container: base => ({...base, flex: 1})}}
                  isDisabled={this.props.disabled}
                  value={this.state.form.performers[idx]}
                  loadOptions={this.loadPerformers}
                  defaultOptions={[]}
                  onChange={performer => this.setState({form: {...this.state.form, performers: [...this.state.form.performers.slice(0, idx), performer, ...this.state.performers(idx + 1)]}})}
                  placeholder="Со-исполнитель"
                />
                <FormControl
                  disabled={this.props.disabled}
                  value={nullToStr(this.state.form.performer_names?.[idx])}
                  onChange={(event) => this.setState({form: {...this.state.form, performer_names: [...(this.state.form.performer_names || []).slice(idx), event.target.value, ...(this.state.form.performer_names || []).slice(idx + 1)]}})}
                  placeholder='Как он записан в источнике'
                />

                <Button
                  className="ml-2"
                  disabled={this.props.disabled}
                  onClick={() => this.setState({form: {...this.state.form, performers: [...this.state.form.performers.slice(0, idx), ...this.state.form.performers.slice(idx + 1)]}})}
                >
                    Удалить
                </Button>
                {
                  this.state.form.performers?.length > idx + 1 ? null :
                  <Button
                    disabled={this.props.disabled}
                    className="ml-2"
                    onClick={this.addPerformer}
                  >
                    Добавить Исполнителя
                  </Button>
                }
              </InputGroup>
            ))
        }
        {
          this.state.form.composer ?
            <>
              <InputGroup className="mb-2">
                <AsyncSelect
                  className="mb-2"
                  styles={ {container: base => ({...base, flex: 1})}}
                  isDisabled={this.props.disabled}
                  value={this.state.form.piece}
                  loadOptions={this.loadPieces}
                  defaultOptions={[]}
                  onChange={piece => this.setState({form: {...this.state.form, piece}})}
                  placeholder="Произведение"
                />
                <FormControl
                  disabled={this.props.disabled}
                  value={nullToStr(this.state.form.piece_name)}
                  onChange={(event) => this.setState({form: {...this.state.form, piece_name: event.target.value}})}
                  placeholder='Как оно записан в источнике'
                />
              </InputGroup>

              <InputGroup className="mb-2">
                <FormControl
                  disabled={this.props.disabled}
                  value={nullToStr(this.state.form.part_number)}
                  onChange={(event) => this.setState({form: {...this.state.form, part_number: +event.target.value}})}
                  placeholder="Номер части"
                />
                <FormControl
                  className="ml-2"
                  disabled={this.props.disabled}
                  value={nullToStr(this.state.form.part_name)}
                  onChange={(event) => this.setState({form: {...this.state.form, part_name: event.target.value}})}
                  placeholder='Как он записан в источнике'
                />

              </InputGroup>
            </>
          : null
        }
        <Button
          onClick={this.onSubmit}
        >
          Сохранить
        </Button>
      </div>
    )
  }
}

AudioForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
}

export default AudioForm;
