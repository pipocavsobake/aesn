import React from 'react';
import LocalForm from './LocalForm';
import Explorer from './Explorer';
import Select from 'react-select';
import File from './File';
import Grid from './Grid';
import {
  ListGroup,
  Spinner,
  ButtonToolbar,
  ButtonGroup,
  Button,
  InputGroup,
  FormControl,
  Row,
  Col
} from 'react-bootstrap';
const defaultExplorer = {value: Explorer, label: 'По умолчанию'};
const explorers = [
  {value: Grid, label: 'Фото-грид'},
  defaultExplorer
];
class Sublist extends React.Component {
  state = {
    mainFile: null,
    explorer: defaultExplorer,
    pinnedFiles: {},
  }
  renderToolbar() {
    return (
      <ButtonToolbar className="mb-3">
        <ButtonGroup className="mr-4">
          <Button
            className="mr-2"
            onClick={this.props.previous}
          >
              Назад
          </Button>
          <Button
            className="mr-2"
            onClick={this.props.next}
          >
            Вперёд
          </Button>
          <Button
            className="mr-2"
            disabled={!!(!this.props.checkPoint || this.props.checkPointPending)}
            onClick={this.props.seekForward}
          >
            Вперёд к неразмеченному
          </Button>
          <Button
            disabled={!!(this.props.checkPoint?.processed_at || this.props.checkPointPending)}
            onClick={this.props.releaseCheckPoint}
          >
            Окнуть чекпоинт {this.props.idx}/{this.props.keys.length} и вперёд
          </Button>
        </ButtonGroup>
        <ButtonGroup>
          <Button
            onClick={() => this.setState({showCheckPoint: !this.state.showCheckPoint})}
          >
            {this.state.showCheckPoint ? 'Скрыть чек-поинт' : 'показать чек-поинт' } {this.props.checkPoint?.items?.length ? `(${this.props.checkPoint.items.length})` : null}
          </Button>
        </ButtonGroup>
      </ButtonToolbar>
    );
  }
  renderCheckPoint() {
    const { checkPoint, checkPointPending } = this.props;
    if (checkPointPending) {
      return <Spinner animation="border" />;
    }
    if (!checkPoint) {
      return <div className="mb-2">Нет даных по чек-поинту</div>;
    }
    return (
      <>
        <h5>{checkPoint.processed_at ? 'Обработанный' : null} чек-поинт файла {checkPoint.path} достигнут {checkPoint.reached_at}</h5>
        {this.state.showCheckPoint ?
          <ListGroup>
            {checkPoint.items.map(item => (
              <ListGroup.Item
                key={item.id}
                className="mb-2"
              >{item.file}{item.second ? ` (${item.second}-я секунда)`: null}
              </ListGroup.Item>
            ))}
          </ListGroup>
        : null }
      </>
    )
  }

  onAction = ({file, action}) => {
    switch(action) {
      case 'main':
        return this.setState({mainFile: file});
      default:
        this.setState({shownFile: file});
    }
  }

  togglePin = (file) => {
    this.setState({pinnedFiles: {...this.state.pinnedFiles, [file]: !this.state.pinnedFiles[file]}});
  }

  renderSettings() {
    return (
      <Row className="mb-2">
        <Col>
          <Select
            styles={ {container: base => ({...base, flex: 1})}}
            value={this.state.explorer}
            onChange={explorer => this.setState({explorer})}
            options={explorers}
            placeholder='Обозреватель списка файлов'
          />
        </Col>
      </Row>
    )
  }


  render() {
    const key = this.props.keys[this.props.idx]
    const files = this.props.files[key];
    const ExplorerComponent = this.state.explorer?.value;
    if (!files || !ExplorerComponent) {
      return <div>Гружу</div>
    }
    const relatedFiles = files.filter(file => file !== this.state.mainFile);
    // Костыль для того, чтобы плеер не вырубался, когда файл из shownFile переходит
    // в pinnedFiles
    const shownFiles = [];
    if (this.state.shownFile) {
      shownFiles.push(this.state.shownFile);
    }
    const shownAndPinnedFiles = [...shownFiles, ...Object.keys(this.state.pinnedFiles).filter(file => this.state.pinnedFiles[file] && file !== this.state.shownFile)];
    // Конец костыля
    const mainFormStyle = {};
    if (!this.state.mainFile) {
      mainFormStyle.visibility = 'hidden';
    }
    return (
      <div>
        {this.renderSettings()}
        {this.renderToolbar()}
        {this.renderCheckPoint()}
        {
          <div style={mainFormStyle}>
            <LocalForm
              key={this.state.mainFile}
              check_point_file={key}
              default_form={this.props.markup.default_form}
              markup_id={this.props.markup.id}
              file={this.state.mainFile || ''}
              relatedFiles={relatedFiles}
              onDone={(next) => this.setState({mainFile: null}, next ? this.props.next : this.props.loadCheckPoint)}
              second={this.state.second}
            />
          </div>
        }
        <Row>
          <Col lg={this.state.explorer === defaultExplorer ? 5 : 12} xl={this.state.explorer === defaultExplorer ? 4 : 12}>
            <ExplorerComponent
              proxy={this.props.proxy}
              files={files}
              selectedHash={this.state.shownFile ? {[this.state.shownFile]: true} : {}}
              onAction={this.onAction}
              actions={[{label: (file) => (this.props.checkPoint?.items?.find(item => item.file === file) ? 'Снова разметить' : 'Разметить'), value: 'main', show: file => file !== this.state.mainFile}]}
            />
          </Col>
          <Col lg="7" xl="8">
            {shownAndPinnedFiles.map(file => (
              <File
                key={file}
                map={this.props.map}
                path={this.props.buildUrl(file)}
                onSetSecond={(second) => this.setState({second})}
                isPinned={this.state.pinnedFiles?.[file]}
                strict={this.props.isStrict(file)}
                onTogglePin={() => this.togglePin(file)}
              />
            ))}
          </Col>
        </Row>
      </div>
    )
  }
}

export default Sublist;
