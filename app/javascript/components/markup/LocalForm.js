import React from 'react';
import PropTypes from 'prop-types';
import AudioForm from './AudioForm';
import OneButtonForm from './OneButtonForm';
import { Spinner } from 'react-bootstrap';
import { withCsrf as axios } from '~/axios-headers';
import Select from 'react-select';

export const endpoint = '/admin/local_markup_items'
export const submit = (props, payload) => {
  const {file, relatedFiles, second, markup_id, check_point_file} = props;
  const local_markup_item = {
    markup_id,
    file,
    second,
    check_point_file,
    related_files: relatedFiles,
    data: payload,
  };
  return axios.post(endpoint, {local_markup_item});
}
const forms = [
  {label: 'Аудио', value: 'audio'},
  {label: 'Кнопка', value: 'one_button'},
];

class LocalForm extends React.Component {
  state = {
    processing: false,
    form: null,
  }
  submit = (payload) => {
    if (this.state.processing) {
      return;
    }
    this.setState({processing: true});
    submit(this.props, payload).then(() => {
      this.props.onDone();
    }).catch(err => {
      console.error(err);
      alert('Произошла ошибка')
      this.setState({processing: false});
    });

  }
  componentDidMount() {
    const form = forms.find(({value}) => value === (this.props.default_form || 'audio'));
    this.setState({form})
  }
  formComponent() {
    switch(this.state.form?.value || this.props.default_form) {
      case 'one_button':
        return OneButtonForm;
      case 'audio':
      default:
        return AudioForm;
    }
  }

  renderForm() {
    const FormComponent = this.formComponent();
    return <FormComponent disabled={this.state.processing} onSubmit={this.submit} />
  }
  render() {
    const {file, relatedFiles, second} = this.props;
    const parts = file.split('/');
    const basename = parts[parts.length - 1]
    return (
      <div className="mb-3">
        <h5 title={file}>Размечаем {basename}{second || second === 0 ? `(${second} секунда)` : null}</h5>
        <div>
          {
            this.state.processing ? (
              <>
                <Spinner as="span" animation="border"/>
                Пересылаю данные
              </>
            ) : null
          }
        </div>
          <Select
            className="mb-3"
            onChange={form => this.setState({form})}
            value={this.state.form}
            options={forms}
          />
        {this.renderForm()}
      </div>
    )
  }
}

LocalForm.propTypes = {
  check_point_file: PropTypes.string.isRequired,
  default_form: PropTypes.string,
  markup_id: PropTypes.number.isRequired,
  file: PropTypes.string.isRequired,
  second: PropTypes.number,
  relatedFiles: PropTypes.arrayOf(PropTypes.string),
  onDone: PropTypes.func.isRequired,
}

export default LocalForm;
