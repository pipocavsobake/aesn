import React from "react"
import PropTypes from "prop-types"
import classNames from 'classnames';

import { HeroesContext } from '~/contexts';
import { withCsrf as axios } from '~/axios-headers';

import Select from 'react-select';

import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';

import filter from 'lodash-es/filter';
import orderBy from 'lodash-es/orderBy';

const primaries = [
  {label: 'A', key: 'attack'},
  {label: 'D', key: 'defence'},
  {label: 'SP', key: 'spell_power'},
  {label: 'KN', key: 'knowledge'},
]

const bank_values = [1, 2, 3, 4].map(value => ({ label: value, value }));

const nullToStr = v => (v || v === 0) ? `${v}` : '';

class FightForm extends React.Component {
  state = {
    form: {
      day: '',
      spells: [],
    },
  }

  parsedDay() {
    const { day } = this.state.form;
    if (day === '') {
      return '? . ? . ?';
    }
    const num = +day;
    const month = Math.floor(num / 100);
    const week = Math.floor((num % 100) / 10);
    const days = num % 10;
    if (month < 1) {
      return '? . ? . ?';
    }
    if (days === 0 || days > 7 || week === 0 || week > 4) {
      return '? . ? . ?';
    }
    return `${month} . ${week} . ${days}`;
  }

  buildPayload({saveTs}) {
    const { ts, videoId } = this.props;
    const {
      day,
      hero,
      bank,
      unit,
      bank_value,
      bank_graded,
      units_count,
      upgraded_units_count,
      attack,
      defence,
      spell_power,
      knowledge,
      ground,
      spells,
    } = this.state.form;
    const video_markup_heroes_fight = {
      hero_id: hero.value,
      attack, defence, spell_power, knowledge,
      day,
      bank_id: bank && bank.value,
      unit_id: unit && unit.value,
      ground_id: ground && ground.value,
      bank_value: bank_value && bank_value.value,
      bank_graded,
      units_count,
      upgraded_units_count,
      spell_ids: spells.map(({value}) => value),
    }
    if (this.props.momentId) {
      if (saveTs) {
        video_markup_heroes_fight.stop_at = ~~this.props.getTs();
      }
      return { video_markup_heroes_fight }
    }
    const video_markup_moment = {
      value: ts, video_id: videoId,
      heroes_fights_attributes: [video_markup_heroes_fight],
    }
    if (saveTs) {
      video_markup_moment.stop_at = ~~this.props.getTs();
    }
    return { video_markup_moment };
  }

  endPoint() {
    if (this.props.momentId) {
      const moment = this.props.heroesData.moments.find(m => m.id === this.props.momentId);
      // TODO по-человечески написать пользователю, что что-то не так, если он вдруг сюда попал с moment без heroes_fight
      // хотя такого быть не должно
      const fight = moment.heroes_fights[0];
      return `/admin/video_markup_heroes_fights/${fight.id}`;
    }
    return '/admin/video_markup_moments'
  }

  onSave = ({play, saveTs}) => {
    if (this.state.submitted) {
      return;
    }
    this.setState({submitted: true}, () => {
      if (Object.keys(this.errors()).length > 0) {
        return;
      }
      //console.log(this.buildPayload());
      //return;
      const method = this.props.momentId ? 'put' : 'post';
      axios[method](this.endPoint(), this.buildPayload({saveTs})).then(resp => {
        this.props.onDone({play});
      }).catch(err => {
        this.setState({submitted: false});
        alert('Не удалось сохранить')
        console.error(err)
      })
    });
  }

  onDelete = () => {
    this.props.onDone();
  }

  errors() {
    const errors = {};
    if (!this.state.submitted) {
      return {};
    }
    const {hero, bank, unit} = this.state.form;
    if (!hero) {
      errors.hero = true;
    }
    if (!bank && !unit) {
      errors.bank_or_unit = true;
    }
    return errors;
  }

  componentDidMount() {
    if (this.props.momentId) {
      this.setMomentData();
    } else {
      setTimeout(this.setPreviousHero, 0);
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.momentId !== prevProps.momentId) {
      this.setMomentData();
    }
  }

  setMomentData() {
    const { heroesData } = this.props;
    const moment = heroesData.moments.find(m => m.id === this.props.momentId);
    if(!moment.heroes_fights || moment.heroes_fights.length === 0) {
      return;
    }
    const fight = moment.heroes_fights[0];
    const {
      day,
      hero_id,
      bank_id,
      unit_id,
      bank_value,
      bank_graded,
      units_count,
      upgraded_units_count,
      attack,
      defence,
      spell_power,
      knowledge,
      ground_id,
      spell_ids,
    } = fight;
    const hero = hero_id && heroesData.heroes_hash[hero_id];
    const bank = bank_id && heroesData.banks_hash[bank_id];
    const unit = unit_id && heroesData.units_hash[unit_id];
    const ground = ground_id && heroesData.grounds_hash[ground_id];
    const spells = spell_ids.map(spell_id => heroesData.spells_hash[spell_id]);
    this.setState({
      form: {
        day,
        hero,
        bank,
        unit,
        bank_value: {
          value: bank_value,
          label: bank_value
        },
        bank_graded,
        units_count,
        upgraded_units_count,
        attack,
        defence,
        spell_power,
        knowledge,
        ground,
        spells,
      }
    })

  }

  setPreviousHero = () => {
    const { heroesData } = this.props;
    const { moments } = heroesData;
    if (!moments || moments.length === 0) {
      return;
    }
    const ordered = orderBy(moments, ["value"], ["desc"]);
    const moment = ordered.find(m => m.heroes_fights && m.heroes_fights.length > 0);
    if (!moment) {
      return;
    }

    const { hero_id, day, ground_id } = moment.heroes_fights[0];

    this.setState({
      form: {
        ...this.state.form,
        hero: heroesData.heroes_hash[hero_id],
        ground: heroesData.grounds_hash[ground_id],
        day
      }
    }, this.setPreviousStats);

  }

  setPreviousStats = () => {
    const { hero } = this.state.form;
    if (!hero) {
      return;
    }
    const { moments } = this.props.heroesData;
    const ordered = orderBy(moments, ["value"], ["desc"]);
    const fights = filter(ordered.map(({heroes_fights}) => heroes_fights && heroes_fights.length > 0 && heroes_fights[0] ), f => f);
    const fight = fights.find(fight => fight.hero_id === hero.id);
    if (!fight) {
      return;
    }
    const { attack, defence, spell_power, knowledge } = fight;
    this.setState({form: {...this.state.form, attack, defence, spell_power, knowledge }});
  }

  changeHero = (hero) => {
    this.setState({form: {...this.state.form, hero}}, () => setTimeout(this.setPreviousStats, 0));
  }

  renderNewSpellBtn() {
    return (
      <InputGroup className="mb-2">
        <Button
          onClick={() => this.setState({form: {...this.state.form, spells: [...this.state.form.spells, null]}})}
        >
          Добавить заклинание
        </Button>
      </InputGroup>
    )
  }

  renderSpell = (spell, idx) => {
    return (
      <InputGroup key={idx} className="mb-2">
        <Select
          styles={ {container: base => ({...base, flex: 1})}}
          value={spell}
          onChange={value => this.setState({form: {...this.state.form, spells: [...this.state.form.spells.slice(0, idx), value, ...this.state.form.spells.slice(idx + 1)]}})}
          options={this.props.heroesData.spells}
        />
        <Button
          onClick={() => this.setState({form: {...this.state.form, spells: [...this.state.form.spells.slice(0, idx), ...this.state.form.spells.slice(idx + 1)]}})}
        >
          Удалить
        </Button>
      </InputGroup>
    )
  }

  renderSpellUsages() {
    const { spells } = this.state.form;
    return (
      <>
        {spells?.map(this.renderSpell)}
        {this.renderNewSpellBtn()}
      </>
    )
  }

  render () {
    const { heroes, units, banks, grounds } = this.props.heroesData;
    const {
      hero,
      unit,
      bank,
      attack,
      defence,
      spell_power,
      knowledge,
    } = this.state.form;
    const errors = this.errors();
    return (
      <React.Fragment>
        <InputGroup className="mb-2">Текущий таймкод {this.props.ts} сек</InputGroup>
        <Select
          className={classNames("mb-2", {border: errors.hero, 'border-danger': errors.hero})}
          value={hero}
          onChange={this.changeHero}
          options={heroes}
          placeholder="Герой стримера (*)"
        />
        <InputGroup className="mb-2">
          {primaries.map(({label, key}) => (
            <FormControl
              key={key}
              placeholder={label}
              type="number"
              value={nullToStr(this.state.form[key])}
              onChange={(event) => this.setState({form: {...this.state.form, [key]: event.target.value}})}
            />
          ))}
        </InputGroup>
        <Select
          className={classNames("mb-2", {border: errors.bank_or_unit, 'border-danger': errors.bank_or_unit})}
          value={unit}
          onChange={(unit) => this.setState({form: {...this.state.form, unit, bank: null, bank_value: null, bank_graded: null}})}
          options={units}
          placeholder="Вражеский тип юнитов"
        />
        {(this.state.form.unit ? (
          <>
          <InputGroup className="mb-2">
            <InputGroup.Text>Число вражеских юнитов (всех и улучшенных)</InputGroup.Text>
            <FormControl
              type="number"
              value={nullToStr(this.state.form.units_count)}
              onChange={(event) => this.setState({form: {...this.state.form, units_count: event.target.value}})}
            />
            <FormControl
              type="number"
              value={nullToStr(this.state.form.upgraded_units_count)}
              onChange={(event) => this.setState({form: {...this.state.form, upgraded_units_count: event.target.value}})}
            />
          </InputGroup>
          </>
        ) : null)}
        <Select
          className={classNames("mb-2", {border: errors.bank_or_unit, 'border-danger': errors.bank_or_unit})}
          value={bank}
          onChange={(bank) => this.setState({form: {...this.state.form, bank, unit: null, units_count: null, upgraded_units_count: null}})}
          options={banks}
          placeholder="Банк существ"
        />
        {(this.state.form.bank ? (
          <>
            <Select
              className="mb-2"
              placeholder="Уровень банка существ"
              value={this.state.form.bank_value}
              options={bank_values}
              onChange={(bank_value) => this.setState({form: {...this.state.form, bank_value}})}
            />
            {this.state.form.bank.id != '' ? (
              <InputGroup className="mb-2">
                <InputGroup.Text>Есть улучшенный стек</InputGroup.Text>
                <InputGroup.Checkbox
                  checked={this.state.form.bank_graded || false}
                  onChange={(event) => this.setState({form: {...this.state.form, bank_graded: event.target.checked}})}
                />
              </InputGroup>
            )  : null }
          </>
        ) : null)}
        <InputGroup className="mb-2">
          <FormControl
            placeholder='День (mwd)'
            type="number"
            value={this.state.form.day}
            onChange={(event) => this.setState({form: {...this.state.form, day: event.target.value}})}
          />
          <InputGroup.Text>{this.parsedDay()}</InputGroup.Text>
        </InputGroup>
        <Select
          className="mb-2"
          placeholder="Земля"
          value={this.state.form.ground}
          options={grounds}
          onChange={(ground) => this.setState({form: {...this.state.form, ground}})}
        />
        {this.renderSpellUsages()}
        <InputGroup>
          <Button disabled={Object.keys(errors).length > 0 || this.state.submitted} variant="success" className="mr-2 mb-2" onClick={this.onSave}>Сохранить</Button>
          <Button disabled={Object.keys(errors).length > 0 || this.state.submitted} variant="success" className="mr-2 mb-2" onClick={() => this.onSave({saveTs: true})}>Сохранить с временем окончания</Button>
          <Button variant="danger" disabled={this.state.submitted} onClick={this.onDelete}>{this.props.momentId ? 'Отменить' : 'Удалить'}</Button>
        </InputGroup>
      </React.Fragment>
    );
  }
}

FightForm.propTypes = {
  ts: PropTypes.number,
  videoId: PropTypes.number,
  onDone: PropTypes.func,
  momentId: PropTypes.number,
  getTs: PropTypes.func,
}

export { FightForm };


class FightFormC extends React.Component {
  static contextType = HeroesContext;
  render() {
    return <FightForm {...this.props} heroesData={this.context} />
  }
}
export default FightFormC;
