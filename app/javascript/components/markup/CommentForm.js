import React from 'react';
import PropTypes from 'prop-types';
import { FormControl, InputGroup, Button } from 'react-bootstrap';
import classNames from 'classnames';
import { withCsrf as axios } from '~/axios-headers';

class CommentForm extends React.Component {
  state = {
    form: {
      content: '',
    }
  }

  buildPayload({saveTs}) {
    const { content } = this.state.form;
    const { ts, videoId, momentId } = this.props;
    const video_markup_comment = { content };
    if (momentId) {
      video_markup_comment.moment_id = momentId;
      if (saveTs) {
        video_markup_comment.stop_at = ~~this.props.getTs();
      }
      return { video_markup_comment };
    }
    const video_markup_moment = {
      value: ts, video_id: videoId,
      comments_attributes: [video_markup_comment],
    }
    if (saveTs) {
      video_markup_moment.stop_at = ~~this.props.getTs();
    }
    return { video_markup_moment }
  }

  endPoint() {
    if (this.props.momentId) {
      return '/admin/video_markup_comment';
    }
    return '/admin/video_markup_moments';
  }

  onSave = ({play, saveTs}) => {
    if (this.state.submitted) {
      return;
    }
    this.setState({submitted: true}, () => {
      if (Object.keys(this.errors()).length > 0) {
        return;
      }
      const method = this.state.momentId ? 'put' : 'post';
      axios[method](this.endPoint(), this.buildPayload({saveTs})).then(resp => {
        this.props.onDone({play})
      }).catch( err => {
        this.setState({submitted: false});
        alert('Не удалось сохранить');
        console.error(err);
      });
    })
  }

  onDelete = () => {
    this.props.onDone();
  }

  errors() {
    const errors = {};
    if (!this.state.submitted) {
      return {};
    }
    const { content } = this.state.form;
    if (!content) {
      errors.content = true;
    }
    return errors;
  }

  render() {
    const errors= this.errors();
    return (
      <div>
        <InputGroup>
          <FormControl
            as="textarea"
            className={classNames("mb-2", {border: errors.content, 'border-danger': errors.content})}
            value={this.state.content}
            onChange={({target: {value}}) => this.setState({form: {...this.state.form, content: value}})}
            placeholder="Комментарий"
          />
        </InputGroup>
        <InputGroup>
          <Button disabled={Object.keys(errors).length > 0 || this.state.submitted} variant="success" className="mr-2 mb-2" onClick={this.onSave}>Сохранить</Button>
          <Button disabled={Object.keys(errors).length > 0 || this.state.submitted} variant="success" className="mr-2 mb-2" onClick={() => this.onSave({saveTs: true})}>Сохранить с временем окончания</Button>
          <Button variant="danger" disabled={this.state.submitted} onClick={this.onDelete}>{this.props.momentId ? 'Отменить' : 'Удалить'}</Button>
          </InputGroup>
      </div>
    )
  }
}

CommentForm.propTypes = {
  ts: PropTypes.number,
  videoId: PropTypes.number,
  onDone: PropTypes.func,
  momentId: PropTypes.number,
  getTs: PropTypes.func,
}

export default CommentForm;
