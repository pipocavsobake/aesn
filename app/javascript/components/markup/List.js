import React from "react"
import PropTypes from "prop-types"

import { ListGroup, Button, Badge } from 'react-bootstrap';
import sortBy from 'lodash-es/sortBy';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { OverlayScrollbarsComponent } from 'overlayscrollbars-react';
class List extends React.Component {
  state = {
    youtube_text: '',
  }

  componentDidMount() {
    this.setYoutubeText();
  }
  setYoutubeText() {
    this.setState({
      youtube_text: sortBy(this.props.moments, 'value').map(m => m.youtube_text).join("\n"),
      copied: false,
    })
  }
  componentDidUpdate(prevProps) {
    if(prevProps.moments !== this.props.moments) {
      this.setYoutubeText();
    }
  }

  onCopy = () => {
    this.setState({copied: true})
  }
  renderCopyBtn() {
    if (this.props.moments.length === 0) {
      return null;
    }
    return (
      <CopyToClipboard text={this.state.youtube_text} onCopy={this.onCopy}>
        <Button className="mb-2" variant="info">{this.state.copied ? 'Скопировать ещё раз' : "Скопировать комментарий с timestamp'ами"}</Button>
      </CopyToClipboard>
    )
  }

  onClick(moment, idx) {
    this.props.onClick(moment, idx);
    this.setState({moment_id: moment.id});
  }
  render () {
    return (
      <>
        { this.renderCopyBtn() }
        <OverlayScrollbarsComponent style={{maxHeight: '300px', maxWidth: '800px'}}>
          <ListGroup>
            {this.props.moments.map((moment, idx) =>(
              <ListGroup.Item
                key={moment.id}
                onClick={() => this.onClick(moment, idx)}
                style={{cursor: 'pointer'}}
                active={moment.id === this.state.moment_id}
                className="d-flex justify-content-between align-items-center"
              >
                {moment.full_text}
                {moment?.can?.update ? <Badge variant="danger" className="p-2">Редактировать</Badge> : null}
              </ListGroup.Item>
            ))}
          </ListGroup>
        </OverlayScrollbarsComponent>
      </>
    );
  }
}

List.propTypes = {
  moments: PropTypes.array,
  onClick: PropTypes.func,
};
export default List
