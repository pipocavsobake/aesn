import React from 'react';

import { Button } from 'react-bootstrap';

class OneButtonForm extends React.Component {
  render() {
    return (
      <Button
        onClick={() => this.props.onSubmit()}
        disabled={this.props.disabled}
      >
        Окнуть
      </Button>
    )
  }
}

export default OneButtonForm;
