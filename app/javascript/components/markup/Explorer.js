import React from 'react';
import PropTypes from 'prop-types';
import { ListGroup, ButtonGroup, Button, Badge } from 'react-bootstrap';
import { OverlayScrollbarsComponent } from 'overlayscrollbars-react';

const extname = file => {
  const parts = file.split('.');
  const ret = parts[parts.length - 1];
  if (ret.length > 4) {
    return "ERR";
  }
  return "."+ret;
}

const basename = file => {
  const parts = file.replace(/\/$/, '').split(/[\/\\]/);
  return parts[parts.length - 1];
}

class Explorer extends React.Component {
  renderAction(action, file) {
    return (
      <Button
        className="ml-2"
        variant="dark"
        key={`key_${action}`}
        onClick={() => this.props.onAction({action: action.value, file: file})}
      >
        {typeof action.label === 'function' ? action.label(file) : action.label}
      </Button>
    )
  }
  renderActions(file) {
    if (!this.props.actions?.length) {
      return null;
    }
    return (
      <ButtonGroup>
        {
          this.props.actions.filter(
            ({show}) => show(file)
          ).map(
            (action) => this.renderAction(action, file)
          )
        }
      </ButtonGroup>
    )
  }
  render() {
    return (
      <OverlayScrollbarsComponent style={{maxHeight: '50vh'}}>
        <ListGroup>
        {this.props.files.map((file, idx) => (
          <ListGroup.Item
            key={file}
            onClick={() => this.props.onAction({file})}
            style={{cursor: 'pointer'}}
            active={this.props.selectedHash[file]}
            className="d-flex justify-content-between align-items-center"
          >
            <Badge className="mr-2" variant="secondary">{extname(file)}</Badge>
            <span style={{wordBreak: 'break-all'}} title={file}>{basename(file)}</span>
            {this.renderActions(file)}
          </ListGroup.Item>
        ))}
        </ListGroup>
      </OverlayScrollbarsComponent>
    );
  }
}

Explorer.propTypes = {
  files: PropTypes.arrayOf(PropTypes.string).isRequired,

  selectedHash: PropTypes.objectOf(PropTypes.bool).isRequired,

  onAction: PropTypes.func.isRequired,

  actions: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.func,
      ]).isRequired,
      value: PropTypes.string.isRequired,
      show: PropTypes.func,
    }),
  )
}

export default Explorer;
