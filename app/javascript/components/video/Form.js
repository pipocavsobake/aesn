import React from "react"
import PropTypes from "prop-types"
import { withCsrf as axios } from '~/axios-headers';
import classNames from 'classnames';

import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';

class Form extends React.Component {
  state = {
    form: {
      url: '',
    }
  }

  onSubmit = () => {
    this.setState({pending: true})
    axios.post('/video_markup/videos', { url: this.state.form.url} ).then(resp => {
      window.location = resp.data.url;
    }).catch(error => {
      this.setState({pending: false});
      if (error.response.status !== 422) {
        console.error(error);
        alert('Неизвестная ошибка')
        return;
      }
      alert(error.response.data.error);
    })
  }

  render () {
    let err = null;
    try {
      const parsed = new URL(this.state.form.url);
      if (!parsed.protocol === 'https') {
        err = new Error('Можно вставлять только ссылки протокола https');
      }
    } catch(e) {
      err = e;
    }

    return (
      <React.Fragment>
        <InputGroup className="mb-2">
          <InputGroup.Text>Ссылка</InputGroup.Text>
            <FormControl
              className={classNames({'border-danger': this.state.form.url !== '' && !!err})}
              value={this.state.form.url}
              placeholder="Вставьте ссылку"
              onChange={(event) => this.setState({form: {...this.state.form, url: event.target.value} })}
          />
        </InputGroup>
        <InputGroup>
          <Button disabled={this.state.pending || !!err} className="mr-2" onClick={this.onSubmit}>
            {this.state.pending ? <Spinner as="span" animation="border" variant="warning"/> : 'Отправить'}
          </Button>
        </InputGroup>
      </React.Fragment>
    );
  }
}

export default Form
