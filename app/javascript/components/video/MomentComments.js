import React from 'react';
import PropTypes from 'prop-types';
import { withCsrf as axios } from '~/axios-headers';
import { FormControl, Button, Card, ListGroup, Spinner, Accordion } from 'react-bootstrap';
import CommentForm from './MomentCommentForm';
class MomentComments extends React.Component {
  state = {
    comments: [],
  };
  renderComment = (comment) => {
    const {id, content, can: {update}} = comment;
    return (
      <ListGroup.Item className='d-flex justify-content-between' key={id}>{content} {(update && id !== this.state.moment?.id) ?
          <Button
            variant="danger"
            size="sm"
            onClick={() => this.setState({comment, showForm: true})}
          >
            Ред.
          </Button>
          : null}</ListGroup.Item>
    )
  }
  onToggleForm = () => {
    if (this.state.showForm) {
      return this.setState({showForm: false});
    }
    const { id } = this.props.moment;
    const comment = {moment_id: id, content: ''};
    this.setState({comment, showForm: true});
  }
  renderForm(){
    if (!this.state.showForm) {
      return null;
    }
    return (
      <CommentForm comment={this.state.comment} onDone={() => {this.setState({comment: null, showForm: false}, this.fetchComments)}} />
    )
  }

  componentDidMount() {
    this.fetchComments();
  }
  componentDidUpdate(prevProps) {
    if (prevProps.moment !== this.props.moment) {
      this.fetchComments();
    }
  }
  fetchComments = () => {
    this.setState({pending: true});
    axios.get(`/admin/video_markup_comments`, {params: {per_page: 1000, 'q[moment_id_eq]': this.props.moment.id}}).then(resp => {
      this.setState({comments: resp.data});
    }).catch(err => {
      alert('Комментарии не удалось загрузить');
      console.error(err);
    }).then(() => this.setState({pending: false}));
  }
  renderHeader() {
    return (
      <>
        Комментарии {this.state.pending ? <Spinner as="span" animation="border" variant="warning"/> : null}
      </>
    );
  }
  renderBody() {
    const { comments } = this.state;
    return (
      <>
        <ListGroup>
          {comments.map(this.renderComment)}
        </ListGroup>
        {this.props.canCreate ?
          <Card.Body>
            <Button
              onClick={this.onToggleForm}
            >
              {this.state.showForm ? 'Отмена' : 'Новый' }
            </Button>
            {this.renderForm()}
          </Card.Body>
        : null }
      </>
    );
  }
  render() {
    return (
      <Card className="mt-2">
        <Card.Header>
          {this.props.accordionKey ?
            <Accordion.Toggle
              as="span"
              style={{cursor: 'pointer'}}
              eventKey={this.props.accordionKey}
            >
              {this.renderHeader()}
            </Accordion.Toggle>
            : this.renderHeader() }
        </Card.Header>
          {this.props.accordionKey ?
            <Accordion.Collapse eventKey={this.props.accordionKey}>
              {this.renderBody()}
            </Accordion.Collapse>
            : this.renderBody()
          }
      </Card>
    )
  }
}

MomentComments.propTypes = {
  moment: PropTypes.shape({
    id: PropTypes.number.isRequired,
  }),
  canCreate: PropTypes.bool.isRequired,
  accordionKey: PropTypes.string,
};

export default MomentComments;
