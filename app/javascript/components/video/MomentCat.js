import React from 'react';
import PropTypes from 'prop-types';
import Video from './Player';
import { withCsrf as axios } from '~/axios-headers';
import { Row, Col, Button, Spinner, Accordion } from 'react-bootstrap';
import MomentComments from './MomentComments';

const hms = value => {
  const h = ~~(value / 3600);
  const m = ~~((value % 3600) / 60)
  const s = ~~(value % 60);
  return [h, m, s];
}

const hhmmss = value => {
  return hms(value).map(v => `${v}`.padStart(2, '0')).join(':')
}

import { hot } from 'react-hot-loader/root';
class MomentCat extends React.Component {
  state = {
    moment_idx: 0,
    touchedVideoHash: {},
    players: {},
  }
  componentDidUpdate(prevProps) {
    if (prevProps.moments !== this.props.moments) {
      this.setState({moment_idx: 0});
    }
  }

  renderEmpty() {
    return (
      <div>Нечего показывать</div>
    );
  }

  renderVideo = (video) => {
    const moment = this.props.moments[this.state.moment_idx];
    const divStyle = moment.video_id === video.id ? {} : {display: 'none'};
    return (
      <div style={divStyle} key={video.id}>
        {this.state.touchedVideoHash[video.id] ?
          <Video
            type={video.js_type}
            slug={video.slug}
            onReady={(player) => this.setState({players: {...this.state.players, [video.id]: player}}, this.onPlayerReady)}
            width={this.state.width - 20}
            onMarkup={this.onMarkup}
            toolbar={false}
            controls={false}
          />
          : null}
      </div>
    )
  }

  renderStart() {
    return (
      <div>
        <div>Видео готовы к просмотру. Жми, чтобы начать</div>
        <Button
          className="mb-2"
          variant="success"
          size="lg"
          block
          onClick={this.start}
          disabled={Object.keys(this.state.touchedVideoHash).length > 0}
        >
          Окей
        </Button>
      </div>
    )
  }

  start = () => {
    const nextDisabled = false;
    if (this.state.touchedVideoHash[this.props.moments[this.state.moment_idx].video_id]) {
      this.onPlayerReady();
      this.setState({ nextDisabled });
    }
    this.setState({
      touchedVideoHash: {
        ...this.state.touchedVideoHash,
        [this.props.moments[this.state.moment_idx].video_id]: true,
      },
      nextDisabled,
    });
  }

  onPlayerReady = () => {
    const {video_id, value} = this.props.moments[this.state.moment_idx];
    const player = this.state.players[video_id];
    player.seekTo(value);
    player.play();
  }

  videoWrapper = React.createRef();

  setWidth() {
    this.setState({width: this.videoWrapper.current.offsetWidth});
  }

  componentDidMount() {
    this.setWidth();
    this.setAutoNextInterval()
  }

  renderCurrentStopAt() {
    const {stop_at} = this.props.moments[this.state.moment_idx];
    if (!stop_at) {
      return <div style={{height: '12ex', overflowY: 'auto'}}>Нет сохранённой отметки об окончании фрагмента, только вы можете остановить это видео</div>;
    }
    return (
      <div style={{height: '8ex', overflowY: 'auto'}}>
        Текущая сохранённая отметка об окончании фрагмента {hhmmss(stop_at)}
      </div>
    )
  }

  next = () => {
    if (this.state.nextDisabled) {
      return;
    }
    this.setState({nextDisabled: true});
    const {video_id, value, stop_at, id} = this.props.moments[this.state.moment_idx];
    const player = this.state.players[video_id];
    player.pause();
    const current = player.getCurrentTime();
    this.setState({needConfirm: {current, stop_at, id}});
  }

  confirm = () => {
    if (this.state.confirming) {
      return;
    }
    this.setState({confirming: true});
    const { current, stop_at, id } = this.state.needConfirm;
    axios.put(`/admin/video_markup_moments/${id}`, {stop_at: current}).then(resp => {
      if (this.state.moment_idx + 1 < this.props.moments.length) {
        this.setState({moment_idx: this.state.moment_idx + 1}, this.start);
      }
    }).catch(err => {
      alert('Ошибка сохранения отметки');
      console.error(err);
      this.setState({nextDisabled: false});
    }).then(() => {
      this.setState({confirming: false, needConfirm: null, nextDisabled: false});
    })
  }

  renderForm() {
    if (this.state.needConfirm) {
      return this.renderConfirm();
    }
    return this.renderNextBtn();
  }

  renderNextBtn() {
    const ts = this.state.currentTs ? hhmmss(this.state.currentTs) : null;
    return (
      <Button
        onClick={this.next}
        variant="info"
        disabled={this.state.nextDisabled || !ts}
      >
          {ts ? `Отметить конец фрагмента как ${ts}` : <Spinner animation="border" size="sm"/>}
      </Button>
    )
  }
  renderConfirm() {
    const { current } = this.state.needConfirm;
    return (
      <div>
        <div>{`Вы действительно хотите сделать отметку об окончании фрагмента на ${current}-й секунде?`}</div>
        <div>
          <Button
            className="mr-2"
            disabled={this.state.confirming}
            onClick={this.confirm}
          >
            Да
          </Button>
            <Button
              variant="danger"
              disabled={this.state.confirming}
              onClick={() => this.setState({nextDisabled: false, currentTs: null, confirming: false, needConfirm: false}, this.play) }
            >
                Нет
            </Button>
        </div>
      </div>
    )
  }

  autoNext = () => {
    const {video_id, value, stop_at, id} = this.props.moments[this.state.moment_idx];
    const player = this.state.players[video_id];
    if (!player) {
      return;
    }
    const current = player.getCurrentTime();
    this.setState({currentTs: current});
    if (!stop_at) {
      return;
    }
    if (current > stop_at) {
      this.navNext();
    }
  }

  setAutoNextInterval() {
    if (this.autoNextInterval) {
      return;
    }
    this.autoNextInterval = setInterval(this.autoNext, 1000);
  }
  removeAutoNextInterval() {
    if (this.autoNextInterval) {
      clearInterval(this.autoNextInterval);
      this.autoNextInterval = null;
    }
  }
  componentWillUnmount() {
    this.removeAutoNextInterval();
  }

  renderActions() {
    const finish = this.state.moment_idx + 1 >= this.props.moments.length;
    return (
      <div>
        <Button
          className="mr-2 mt-2"
          variant="success"
          onClick={() => this.navNext()}
          disabled={this.state.nextDisabled || finish}
        >
          Перейти к следующей битве
        </Button>
        <Button
          className="mr-2 mt-2"
          variant="info"
          onClick={() => this.navNext(-1) }
          disabled={this.state.nextDisabled || this.state.moment_idx === 0}
        >
          Предыдущая битва
        </Button>
        {finish ?
          <Button
            className="mt-2"
            variant="success"
            href={this.props.path_to_next_page}
          >
            Смотреть ещё битвы
          </Button>
          : null
        }
      </div>
    )
  }

  navNext = (direction = 1) => {
    const {video_id, value, stop_at, id} = this.props.moments[this.state.moment_idx];
    const player = this.state.players[video_id];
    if (!player) {
      return;
    }
    player.pause();
    this.setState({nextDisabled: true, currentTs: null});
    if ((direction === 1 && this.state.moment_idx + 1 >= this.props.moments.length) || (direction === -1 && this.state.moment_idx === 0)) {
      return;
    }
    this.setState({
      moment_idx: this.state.moment_idx + direction,
      confirming: false,
      needConfirm: false,
    }, this.start);
  }

  renderCurrentInfo() {
    const moment = this.props.moments[this.state.moment_idx];
    return (
      <div className="mb-4">
        <div>{this.state.moment_idx + 1} / {this.props.moments.length}</div>
        <div style={{height: '10ex', overflowY: 'auto'}}>{moment.full_text}</div>
      </div>
    )
  }

  play = () => {
    const {video_id} = this.props.moments[this.state.moment_idx];
    this.state.players[video_id]?.play();
  }

  renderComments() {
    const { current_user, moments } = this.props;
    const moment = moments[this.state.moment_idx];
    if (!current_user) {
      return null;
    }
    return <MomentComments moment={moment} canCreate={!!current_user} accordionKey={'comments'} />;
  }

  renderAccordion() {
    return (
      <Accordion>
        {this.renderComments()}
      </Accordion>
    )
  }

  render() {
    const moment = this.props.moments[this.state.moment_idx];
    return (
      <Row>
        <Col lg="8" xl="9" ref={this.videoWrapper}>
          {this.props.moments.length === 0 ? this.renderEmpty() : null }
          {Object.keys(this.state.touchedVideoHash).length === 0 ? this.renderStart() : null}
          {this.state.width ? this.props.videos.map(this.renderVideo) : null}
        </Col>
        <Col lg="4" xl="3">
          {Object.keys(this.state.touchedVideoHash).length === 0 ? null :
            <>
              {this.renderCurrentInfo()}
              {this.renderCurrentStopAt()}
              {moment?.can?.update ? this.renderForm() : null }
              {this.renderActions()}
              {this.renderAccordion()}
            </>
          }
        </Col>
      </Row>
    )
  }
}

MomentCat.propTypes = {
  moments: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
    stop_at: PropTypes.number,
    video_id: PropTypes.number.isRequired,
    can: PropTypes.shape({
      create: PropTypes.bool,
      destroy: PropTypes.bool,
      read: PropTypes.bool,
      update: PropTypes.bool,
    })
  })).isRequired,
  videos: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    slug: PropTypes.string.isRequired,
    js_type: PropTypes.oneOf(['youtube', 'twitch']).isRequired,
  })).isRequired,
  path_to_next_page: PropTypes.string.isRequired,
  current_user: PropTypes.shape({
    id: PropTypes.number.isRequired,
  })
}

export default hot(MomentCat);
