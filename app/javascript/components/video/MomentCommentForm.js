import React from 'react';
import PropTypes from 'prop-types';
import { withCsrf as axios } from '~/axios-headers';
import { InputGroup, FormControl, Button, Spinner } from 'react-bootstrap';
import classNames from 'classnames';

class MomentCommentForm extends React.Component {
  state = {
    form: {
      content: '',
    },
  }

  static defaultProps = {
    onDone: () => {},
    onError: e => { alert('Ошибка'); console.error(e); },
  }

  componentDidMount() {
    this.setState({
      form: this.props.comment,
    })
  }

  componentDidUpdate(prevProps) {
    if (prevProps.comment !== this.props.comment) {
      this.setState({form: this.props.comment});
    }
  }

  buildPayload() {
    const { moment_id, id } = this.props.comment;
    const { content } = this.state.form;
    if (id) {
      return { content }
    }
    return { moment_id, content };
  }

  submit = () => {
    const { id } = this.props.comment;
    const method = id ? 'put' : 'post';
    const endPoint = id ? `/admin/video_markup_comments/${id}` : `/admin/video_markup_comments`;
    this.setState({touched: true});
    if (Object.keys(this.errors()).length) {
      return;
    }
    this.setState({submitting: true});
    axios[method](endPoint, this.buildPayload()).then(resp => {
      this.props.onDone(resp.data);
    }).catch(err => {
      this.props.onError(err);
    }).then(() => this.setState({submitting: false}));
  }

  errors() {
    const errors = {};
    if (this.state.form.content.length === 0) {
      errors.content = true;
    }

    return errors;
  }
  render() {
    const errors = this.state.touched ? this.errors() : {};
    const hasError = Object.keys(errors).length > 0;
    return (
      <div className="mt-2">
        <InputGroup className="mb-2">
          <FormControl
            as="textarea"
            className={classNames({'border-danger': errors.content, border: errors.content})}
            value={this.state.form.content}
            onChange={({target: {value}}) => this.setState({form: {...this.state.form, content: value}})}
            placeholder="Текст комментария"
          />
        </InputGroup>
        <InputGroup>
          <Button
            variant="success"
            disabled={hasError}
            onClick={this.submit}
          >
            {this.state.submitting ? <Spinner as="span" animation="border" variant="warning"/> : 'Сохранить'}
          </Button>
        </InputGroup>
      </div>
    )
  }
}


MomentCommentForm.propTypes = {
  comment: PropTypes.shape({
    id: PropTypes.number,
    content: PropTypes.string,
    moment_id: PropTypes.number.isRequired,
  }),
  onDone: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
};

export default MomentCommentForm;
