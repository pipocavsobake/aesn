import React from "react";
import PropTypes from "prop-types";
import YouTube from 'react-youtube';
import Twitch from 'react-twitch-embed-video';
import Button from 'react-bootstrap/Button';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';

import FormControl from 'react-bootstrap/FormControl';

const false2undefined = v => v ? v : undefined;

class PlayerControl {
  constructor({youtube, twitch}) {
    this.youtube = youtube;
    this.twitch = twitch;
  }
  pause() {
    if (this.youtube) {
      this.youtube.pauseVideo();
    }
    if (this.twitch) {
      this.twitch.pause();
    }
  }
  play() {
    if (this.youtube) {
      this.youtube.playVideo();
    }
    if (this.twitch) {
      this.twitch.play();
    }
  }

  toggle() {
    if (this.youtube) {
      switch (this.youtube.getPlayerState()) {
        case 1:
          return this.youtube.pauseVideo();
        case 2:
          return this.youtube.playVideo();
      }
    }
    if (this.twitch) {
      if (this.twitch.isPaused()) {
        this.twitch.play();
      } else {
        this.twitch.pause();
      }
    }
  }

  seekTo(seconds) {
    if (this.youtube) {
      this.youtube.seekTo(seconds, true);
    }
    if(this.twitch) {
      this.twitch.seek(seconds);
    }
  }

  getCurrentTime() {
    if (this.youtube) {
      return this.youtube.getCurrentTime();
    }
    if (this.twitch) {
      return this.twitch.getCurrentTime();
    }
  }

}

class Player extends React.Component {
  state = {
    last_ts: '',
  }
  static defaultProps = {
    onMarkup: (ts) => { },
    toolbar: true,
    controls: true,
  }
  onReadyYoutube = (event) => {
    this.player = new PlayerControl({youtube: event.target});
    if (this.props.onReady) {
      this.props.onReady(this.player);
    }
  }

  onReadyTwitch = (player) => {
    this.player = new PlayerControl({twitch: player})
    if (this.props.onReady) {
      this.props.onReady(this.player);
    }
  }

  onMarkup = ({pause} = {}) => {
    if (pause) {
      this.player.pause()
    }
    if (this.props.onMarkup) {
      this.props.onMarkup(this.player.getCurrentTime());
    }
  }

  onStateChange = (event) => {
    this.setState({playing: event.data === YouTube.PlayerState.PLAYING});
  }

  renderYoutube() {
    if (this.props.type !== 'youtube') {
      return null;
    }
    return (
      <YouTube
        opts={ {width: this.props.width, height: Math.round(this.props.width * 9 / 16), playerVars: {controls: this.props.controls ? 1 : 0}} }
        videoId={this.props.slug}
        onReady={this.onReadyYoutube}
        onStateChange={this.onStateChange}
      />
    )
  }
  renderTwitch() {
    if (this.props.type !== 'twitch') {
      return null;
    }
    // channel={this.props.streamerSlug}
    return (
      <Twitch
        video={this.props.slug}
        width={this.props.width}
        layout='video'
        height={Math.round(this.props.width * 9 / 16)}
        onPlayerReady={this.onReadyTwitch}
      />
    )
  }

  componentDidCatch(error, errorInfo) {
    console.error(error);
    console.error(errorInfo);
  }

  onTsInputChange = ({target: {value}}) => {
    this.setState({last_ts: value}, () => {
      this.player.seekTo(+value);
    })
  }

  render () {
    return (
      <React.Fragment>
        {this.renderYoutube()}
        {this.renderTwitch()}
        {this.props.toolbar ?
          <>
            <ButtonToolbar className="mb-2 cb-player-indicator" data-playing={false2undefined(this.state.playing)}>
              <Button className="mr-2" onClick={() => this.onMarkup()}>+Заметка</Button>
              <Button onClick={() => this.onMarkup({pause: true})}>Пауза и заметка</Button>
            </ButtonToolbar>
            <FormControl
              className="mb-2"
              value={this.state.last_ts}
              placeholder="Timestamp в секундах"
              onChange={this.onTsInputChange}
            />
          </>
        : null}
      </React.Fragment>
    );
  }
}

Player.propTypes = {
  type: PropTypes.oneOf(['youtube', 'twitch']).isRequired,
  slug: PropTypes.string.isRequired,
  streamerSlug: PropTypes.string,
  onMarkup: PropTypes.func,
  // onReady вернёт экземпляр player
  onReady: PropTypes.func,
  width: PropTypes.number.isRequired,
  toolbar: PropTypes.bool.isRequired,
  onStateChanged: PropTypes.func,
  controls: PropTypes.bool.isRequired,
};
export default Player
