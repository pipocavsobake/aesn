import React from 'react';
import PropTypes from 'prop-types';
import { hot } from 'react-hot-loader/root';

import axios from 'axios';
import { withCsrf } from '~/axios-headers';
import Sublist from './markup/Sublist';
import GalleryProxy from './markup/GalleryProxy';
import Select from 'react-select';


import {
  ListGroup,
  Spinner,
  ButtonToolbar,
  ButtonGroup,
  Button,
  InputGroup,
  FormControl,
  Row,
  Col
} from 'react-bootstrap';
import { LocalContext } from '~/contexts';

const defaultExplorer = {value: Sublist, label: 'По умолчанию'};
const explorers = [
  {value: GalleryProxy, label: 'Галерея'},
  defaultExplorer
];
class LocalMarkup extends React.Component {
  state = {
    contextData: {cue: {}},
    files: null,
    meta: {},
    keys: null,
    idx: null,
    markup: null,
    pending: null,
    explorer: defaultExplorer,
  };

  componentDidMount() {
    this.setState({markup: {...this.props}}, () => this.loadFiles())
  }

  loadFiles() {
    axios.get(this.state.markup.file).then(({data}) => {
      let files = data;
      const flatFiles = [];
      const meta = data.meta || this.state.meta;

      if (data.files) {
        files = data.files;
      }
      const keys = Object.keys(files);
      if (keys.length === 0) {
        return;
      }
      keys.forEach((key, checkPointIdx) => {
        files[key] = [key, ...files[key]];
        files[key].forEach((file, idx) => {
          flatFiles.push({file, checkPoint: key, idx, total: files[key].length, checkPointIdx});
        })
      })
      this.setState({files, meta, flatFiles, keys, idx: 0, checkPointPending: true}, this.loadCheckPoint);
    }).catch(err => {
      console.error(err)
    })
  }
  loadCues = () => {
    if (!this.state.markup.cues_file) {
      return;
    }
    axios.get(this.state.markup.cues_file).then(({data}) => {
      const keys = Object.keys(data);
      const cue = {};
      for(let key of keys) {
        cue[this.buildUrl(key)] = data[key];
      }
      this.setState({contextData: {...this.state.contextData, cue}});
    })
  }
  next = () => {
    this.setState({idx: (this.state.idx + 1) % this.state.keys.length, checkPoint: null, checkPointPending: true}, this.loadCheckPoint);
  }
  previous = () => {
    const { length } = this.state.keys;
    this.setState({idx: (this.state.idx + length - 1) % length, checkPoint: null, checkPointPending: true}, this.loadCheckPoint);
  }
  releaseCheckPoint = () => {
    const path = this.state.keys[this.state.idx]
    withCsrf.put(`/local_markups/${this.props.id}/check_points/1`, {path}).then(this.next).catch(err => {
      alert('Ошибка сервера');
      console.error(err);
    })
  }
  loadCheckPoint = () => {
    const path = this.state.keys[this.state.idx]
    this.setState({checkPointPending: true, shownFile: null, pinnedFiles: {}});
    return withCsrf.get(`/local_markups/${this.props.id}/check_points/1`, { params: {path}}).then(({data}) => {
      this.setState({checkPoint: data});
    }).catch(err => {
      console.error(err);
    }).then(() => this.setState({
      checkPointPending: false,
    }));
  }

  seekForward = () => {
    this.setState({pending: true});
    withCsrf.get(`/local_markups/${this.props.id}/check_points`).then(({data}) => {
      this.seekForwardWith(data);
    }).catch(err => {
      alert('Ошибка');
      console.error(err);
    }).then(() => {
      this.setState({pending: false});
    })
  }
  seekForwardWith(paths_hash) {
    for(let idx = this.state.idx + 1; idx != this.state.idx; idx = (idx + 1) % this.state.keys.length) {
      if (!paths_hash[this.state.keys[idx]]) {
        return this.setState({idx: idx, checkPoint: null, checkPointPending: true}, this.loadCheckPoint);
      }
    }
  }

  changeRawMap = event => {
    this.setState({rawMap: event.target.value, rawMapState: null}, () => {
      try {
        JSON.parse(this.state.rawMap)
        this.setState({rawMapState: 'ok'})
      } catch(e) {
        this.setState({rawMapState: e.message})
      }
    })
  }

  buildUrl = (path) => {
    for(let directory in this.state.map) {
      if(path.startsWith(directory)) {
        return path.replace(directory, this.state.map[directory])
      }
    }
    if (path.startsWith('/')) {
      try {
        const checkPointUrl = new URL(this.state.keys[this.state.idx]);
        checkPointUrl.pathname = path;
        return checkPointUrl.toString();
      } catch(e) {
        console.error(e);
        return path;
      }
    }
    return path;
  }

  isStrict = (file) => {
    return !!Object.keys(this.state.map).find(prefix => file.startsWith(prefix));
  }

  renderSelectExplorer() {
    return (
      <Select
        className="mb-2"
        value={this.state.explorer}
        onChange={explorer => this.setState({explorer})}
        options={explorers}
        placeholder='Выберете обозреватель файлов'
      />
    )
  }

  proxyInvalid() {
    if (!this.state.proxy) {
      return false;
    }
    try {
      new URL(this.state.proxy);
    } catch(err) {
      return true;
    }
  }

  renderMeta() {
    if (!this.state.meta) {
      return null;
    }
    const key = this.state.keys[this.state.idx];
    return JSON.stringify(this.state.meta[key]);
  }

  render() {
    if (this.state.pending) {
      return (
        <Spinner animation="border" />
      )
    }
    if (!this.state.map) {
      return (
        <>
          <h2>
            Шаг 1. Нужно научить разметчик получать файлы из хранилища
          </h2>
          <p>
            Если вы открыли страницу по ссылки от друга, то он должен вам дать текст, который нужно сюда вставить.
          </p>
          <p>
            Если же вы сами владелец хранилища файлов и создали это разметку, то вам необходимо раздать нужные папки по http. Например, с помощью <a href="https://www.npmjs.com/package/http-server" title="http-server: a command-line http server">http-server</a>. Допустим вы раздаёте папку <code>/hd1</code> по адресу <code>http://127.0.0.1:8080</code>, тогда вам нужно ввести json <code>{'{"/hd1":"http://127.0.0.1:8080"}'}</code>. Вы можете раздавать несколько папок разом с одного или нескольких http-server'ов, например, <code>{'{"/hd1/hd2/":"http://localhost:36359/0/","/hd2/":"http://localhost:36359/1/"}'}</code>
          </p>
          <InputGroup className="mb-2">
            <InputGroup.Text>Json от файлового-сервера</InputGroup.Text>
            <FormControl
              value={this.state.rawMap || ''}
              as='textarea'
              placeholder="Вставьте"
              onChange={this.changeRawMap}
            />
            <InputGroup.Text>Адрес прокси</InputGroup.Text>
            <FormControl
              value={this.state.proxy || ''}
              as='textarea'
              placeholder="Вставьте"
              onChange={({target: {value}}) => this.setState({proxy: value})}
            />
            <Button
              disabled={this.state.rawMapState !== 'ok' || this.proxyInvalid()}
              onClick={() => this.setState({map: JSON.parse(this.state.rawMap)}, this.loadCues)}
            >
              Применить
            </Button>
          </InputGroup>
        </>
      )
    }
    if (!this.state.files) {
      return <>Гружу</>;
    }
    const ExplorerComponent = this.state.explorer.value;
    return (
      <LocalContext.Provider value={this.state.contextData}>
        {this.renderSelectExplorer()}
        <div>
          <pre>
            {this.state.keys[this.state.idx]}
          </pre>
        </div>
          <div>
            <pre>
              {this.renderMeta()}
            </pre>
          </div>
        <ExplorerComponent
          flatFiles={this.state.flatFiles}
          keys={this.state.keys}
          files={this.state.files}
          meta={this.state.meta}
          previous={this.previous}
          next={this.next}
          proxy={this.state.proxy}
          seekForward={this.seekForward}
          checkPoint={this.state.checkPoint}
          checkPointPending={this.state.checkPointPending}
          releaseCheckPoint={this.releaseCheckPoint}
          loadCheckPoint={this.loadCheckPoint}
          idx={this.state.idx}
          setIdx={(idx) => this.setState({idx}, this.loadCheckPoint)}
          markup={this.state.markup}
          buildUrl={this.buildUrl}
          isStrict={this.isStrict}
        />
      </LocalContext.Provider>
    )
  }
}

LocalMarkup.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  file: PropTypes.string.isRequired,
  cues_file: PropTypes.string,
  default_form: PropTypes.string,
}

export default hot(LocalMarkup);
