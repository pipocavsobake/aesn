import maxBy from 'lodash-es/maxBy';
import minBy from 'lodash-es/minBy';
import debounce from 'lodash-es/debounce';

const WRAPPER_SELECTOR = '.js-video-tag';
const SPINNER_SELECTOR = '.js-video-spinner';
function init() {

    function hide(elem) {
        if (!elem) {
            return;
        }
        elem.classList.add('d-none');
    }

    function show(elem) {
        if (!elem) {
            return;
        }
        elem.classList.remove('d-none');
    }

    function isSrcsEqual(src1, src2) {
        // расчитано на то, что в src2 всегда будут только пути,
        // а не полные урлы с доменом и протоколом
        try {
            return (src1 === src2) || (new URL(src1).pathname === src2);
        } catch (err) {
            return false;
        }
    }

    function replaceSrc(video, sources) {
        const source = maxBy(sources.filter(({width}) => width <= video.scrollWidth ), 'width') || minBy(sources, 'width');
        const currentSource = sources.find(({src}) => isSrcsEqual(video.src, src));
        if (currentSource && (currentSource.width >= source.width)) {
            return;
        }
        video.src = source.src;
        video.load();
    }

    function processWrapper(wrapper) {
        const {sources, attributes} = wrapper.dataset;
        let video = wrapper.querySelector('video');
        if (!video) {
            video = document.createElement('video');
            const attrs = JSON.parse(attributes);
            Object.keys(attrs).forEach(attribute => video[attribute] = attrs[attribute]);
            wrapper.appendChild(video);
        }
        replaceSrc(video, JSON.parse(sources));
        hide(wrapper.querySelector(SPINNER_SELECTOR));
    }

    function processWrappers() {
        document.querySelectorAll(WRAPPER_SELECTOR).forEach(wrapper => {
            processWrapper(wrapper);
        });
    }
    const resizeHandler = debounce(processWrappers, 300);
    window.addEventListener('resize', resizeHandler);
    document.addEventListener('turbolinks:before-cache', function() {
        window.removeEventListener('resize', resizeHandler);
        document.querySelectorAll(WRAPPER_SELECTOR).forEach(wrapper => {
            wrapper.querySelector('video')?.remove();
            show(wrapper.querySelector(SPINNER_SELECTOR));
        });
    })

    processWrappers();
}

document.addEventListener('turbolinks:load', init);
