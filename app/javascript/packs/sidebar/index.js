const OPEN_BTN_SELECTOR = '.js-sidebar-open';
const CLOSE_BTN_SELECTOR = '.js-sidebar-close';
const SIDEBAR_SELECTOR = '.js-sidebar';
function init() {
    const sidebar = document.querySelector(SIDEBAR_SELECTOR);
    if (!sidebar) {
        return;
    }
    document.addEventListener('click', function(event) {
        const sidebar = document.querySelector(SIDEBAR_SELECTOR);
        const {target} = event;
        if (target.closest(OPEN_BTN_SELECTOR)) {
            sidebar.classList.remove('d-none');
        } else if (target.closest(CLOSE_BTN_SELECTOR)) {
            sidebar.classList.add('d-none');
        }
    });
}

document.addEventListener('DOMContentLoaded', init);
