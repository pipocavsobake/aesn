import Plotly from 'plotly.js';
import debounce from 'lodash-es/debounce';

const CHARTS_DIV_ID = 'js-activity-charts';

var resizeListener;
function init() {
    initContainer();
}

function initContainer() {
    const container = document.getElementById(CHARTS_DIV_ID);
    if (!container || (container.offsetParent === null)) {
        return;
    }
    setTimeout(() => runInContainer(container), 200);
}

function show(element) {
    element.classList.remove('d-none');
}

function hide(element) {
    element.classList.add('d-none');
}
function runInContainer(container) {
    if (!container) {
        return;
    }
    const spinnerWrapper = document.getElementById(container.dataset.spinner);
    show(spinnerWrapper);
    const {data, layout} = JSON.parse(container.dataset.props);
    const {distance, heartbeats, time} = JSON.parse(container.dataset.extra);
    const plot = Plotly.newPlot(CHARTS_DIV_ID, data, layout);
    hide(spinnerWrapper);

    container.on('plotly_selected', async function(eventData) {
        if (!eventData) {
            Plotly.relayout(CHARTS_DIV_ID, {annotations: [], shapes: []});
            return;
        }
        const {range} = eventData;
        const startIndex = Math.ceil(range.x[0]);
        const endIndex = Math.ceil(range.x[1]);
        const distanceDiff = distance[endIndex] - distance[startIndex];
        const tDiff = time[endIndex] - time[startIndex];
        const averageVelocity = distanceDiff / tDiff;
        const averagePace = Math.floor(1000 * tDiff / distanceDiff);
        const [mm, ss] = [Math.floor(averagePace / 60), averagePace % 60];

        const beatsDiff = heartbeats[endIndex] - heartbeats[startIndex];
        const averageHeartrate = (60 * beatsDiff) / tDiff;

        const tMid = data[0].x[Math.floor((endIndex + startIndex) / 2)];
        const annotations = [
            {
                x: tMid,
                y: 150,
                xref: 'x',
                yref: 'y2',
                text: `${mm}:${ss < 10 ? `0${ss}` : ss} — ${Math.floor(averageHeartrate)}`,
                showarrow: false,
            }
        ];


        const shapes = [{
            type: 'rect',
            xref: 'x',
            yref: 'paper',
            x0: data[0].x[startIndex],
            y0: 0,
            x1: data[0].x[endIndex],
            y1: 1,
            fillcolor: '#d3d3d3',
            opacity: 0.2,
            line: {
                width: 1,
            },
        }]
        Plotly.relayout(CHARTS_DIV_ID, {annotations, shapes});
    });

    resizeListener = debounce(
        function() {
            show(spinnerWrapper);
            const width = container.parentElement.clientWidth;
            const height = container.parentElement.clientHeight;
            Plotly.relayout(CHARTS_DIV_ID, {width, height});
            hide(spinnerWrapper);
        },
        1000
    )
    window.addEventListener('resize', resizeListener);
}

document.addEventListener('turbolinks:before-cache', function() {
    window.removeEventListener('resize', resizeListener);
    Plotly.purge(CHARTS_DIV_ID);
})


document.addEventListener('turbolinks:load', init);
