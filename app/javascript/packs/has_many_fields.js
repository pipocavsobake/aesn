// Работает с bootstrap-классами

const REMOVE_BTN_CLASS = '.js-remove-has-many';
const ADD_BTN_CLASS = '.js-add-has-many';

function handleRemove(event) {
    const target = event.target.closest(REMOVE_BTN_CLASS);
    const {parentclass} = target.dataset;
    const wrapper = target.closest(`.${parentclass}`);
    const idValue = wrapper.querySelector('[name$="[id]"]').value;
    if (idValue) {
        wrapper.querySelector('[name*="[_destroy]"]').value = "1";
        wrapper.classList.add('d-none');
    } else {
        wrapper.remove();
    }
}

function handleAdd(event) {
    const target = event.target.closest(ADD_BTN_CLASS);
    const {wrapperclass, fields, id} = target.dataset;
    const wrapper = target.closest('form').querySelector(`.${wrapperclass}`);
    const regexp = new RegExp(id, 'g');
    //const newId = new Date().getTime();
    const newId = wrapper.children.length;
    wrapper.insertAdjacentHTML('beforeend', fields.replace(regexp, newId));
}

function init() {
    document.addEventListener('click', function(event) {
        if (event.target.closest(REMOVE_BTN_CLASS)) {
            event.preventDefault();
            handleRemove(event);
        } else if (event.target.closest(ADD_BTN_CLASS)) {
            event.preventDefault();
            handleAdd(event);
        }
    })
}

document.addEventListener('DOMContentLoaded', init);
