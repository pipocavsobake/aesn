import * as Tone from 'tone';

const tones = {};
function playTone(elem) {
  const freq = elem.dataset.frequency
  if (tones[freq]) {
    return;
  }
  const synth = new Tone.Synth().toMaster();
  //console.log('try start freq', freq);
  synth.triggerAttack(freq);
  tones[freq] = synth;
}
function stopTone(elem) {
  const freq = elem.dataset.frequency
  if (!tones[freq]) {
    return;
  }
  //console.log('try stop freq', freq);
  tones[freq].triggerRelease();
  tones[freq] = null;
}


const samples = {};

function playSample(elem) {
  const audio = samples[elem.dataset.id] || new Audio(elem.dataset.sample);
  audio.currentTime = 0;
  audio.play();
  samples[elem.dataset.id] = audio;
}

function stopSample(elem) {
  const audio = samples[elem.dataset.id]
  if (audio) {
    audio.pause();
  }
}
function startPlay(event) {
  const elem = event.target;
  elem.classList.add('widgets__keyboard-key-pressed');
  if (elem.dataset.sample) {
    playSample(elem);
  } else {
    playTone(elem);
  }
}

function endPlay(event) {
  const elem = event.target;
  elem.classList.remove('widgets__keyboard-key-pressed');
  if (elem.dataset.sample) {
    stopSample(elem);
  } else {
    stopTone(elem);
  }
}

function initKeyboard() {
  [].forEach.call(document.querySelectorAll('.js-widgets-keyboard-key'), function(element) {
    element.addEventListener('touchstart', startPlay);
    element.addEventListener('mousedown', startPlay);
    element.addEventListener('touchend', endPlay);
    element.addEventListener('touchcancel', endPlay);
    element.addEventListener('mouseup', endPlay);
  });
}

function init() {
  initKeyboard()
}

document.addEventListener('turbolinks:load', init)
