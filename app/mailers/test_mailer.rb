class TestMailer < ApplicationMailer
  def check(email)
    mail to: email, subject: 'Проверка связи'
  end
end
