class ApplicationMailer < ActionMailer::Base
  default from: 'admin@aesn.ru'
  layout 'mailer'
end
