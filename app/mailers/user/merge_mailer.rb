class User::MergeMailer < ApplicationMailer
  def confirm(merge)
    @merge = merge
    @user = User.find_by(email: merge.email)
    @identity = @merge.identity
    mail to: merge.email, subject: "Подтверждения слияния аккаунтов на aesn.ru"
  end
end
