# == Schema Information
#
# Table name: sport_activities
#
#  id               :bigint           not null, primary key
#  name             :string           not null
#  user_id          :bigint
#  started_at       :datetime
#  duration_seconds :bigint
#  kind             :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  enabled          :boolean          default(FALSE), not null
#  og_image_data    :jsonb
#
# Indexes
#
#  index_sport_activities_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class Sport::Activity < ApplicationRecord
  has_many :strava_activities, class_name: 'Strava::Activity', dependent: :nullify, foreign_key: :sport_activity_id
  belongs_to :user
  has_many :activity_exercises, class_name: 'Sport::ActivityExercise', dependent: :destroy, foreign_key: :activity_id
  has_many :sorted_activity_exercises, -> { sorted }, class_name: 'Sport::ActivityExercise'
  has_many :exercises, class_name: 'Sport::Exercise', through: :activity_exercises

  accepts_nested_attributes_for :activity_exercises, allow_destroy: true, reject_if: proc { |attributes| attributes.slice('exercise_id', 'exercise').values.all?(&:blank?) }

  def duration
    return nil if duration_seconds.nil?

    Time.at(duration_seconds).gmtime
  end

  def duration=(value)
    self.duration_seconds = nil if value.nil?

    hh, mm, ss = value.split(':').map(&:to_i)
    self.duration_seconds = hh * 3600 + mm * 60 + ss
  end

  attr_accessor :strava_activity_id
  after_create :set_sport_activity_id_to_strava_activity, if: -> { strava_activity_id.present? }

  def set_sport_activity_id_to_strava_activity
    user.strava_activities.find(strava_activity_id).update!(sport_activity_id: id)
  end

  def self.default_kinds
    ['Бег трусцой', 'Бег интервальный', 'Функциональная', 'Велопрогулка']
  end

  def provider_activities
    strava_activities.sort_by(&:strava_id)
  end

  def provider_names
    strava_activities.map(&:provider_name).uniq
  end

  def activity_exercises_with_video
    @__activity_exercises_with_video ||= activity_exercises.to_a.select { |obj| obj.exercise.video_derivatives.present? }
  end

  def og_image_sizes
    width = [(activity_exercises_with_video.size / 1.618).round, 2].max
    height = (activity_exercises_with_video.size.to_f / width.to_f).ceil
    [width, height]
  end

  def generate_montage_image
    tempfile = Tempfile.new ["montage", ".jpg"]
    MiniMagick::Tool::Montage.new do |montage|
      montage.mode 'concatenate'

      montage.background 'none'
      activity_exercises_with_video.each do |activity_exercise|
        montage.resize '200x'
        montage << activity_exercise.exercise.video(:screenshot).download.path
      end

      montage.tile og_image_sizes.join('x')

      montage << tempfile.path
    end
    tempfile
  end

  def refresh_og_image!
    self.og_image = generate_montage_image
    save!
  end

  include OgImageUploader::Attachment(:og_image)

  def title
    [user.name, name].join(' — ')
  end

  def content
    exercises.map(&:name).to_sentence
  end

  def h1
    title
  end

  def keywords
    "тренировка, упражнения, спорт"
  end

  def description
    content
  end

  def og_title
    ['Тренировка', title].join(' — ')
  end

  def robots
  end

  def fullpath
  end
end
