# == Schema Information
#
# Table name: sport_exercise_simple_groups
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Sport::Exercise::SimpleGroup < ApplicationRecord
  has_many :exercises, class_name: 'Sport::Exercise', foreign_key: :simple_group_id, dependent: :nullify
end
