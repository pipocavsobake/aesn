# == Schema Information
#
# Table name: sport_exercises
#
#  id              :bigint           not null, primary key
#  name            :string           not null
#  slug            :string           not null
#  content         :text
#  video_data      :jsonb
#  enabled         :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  user_id         :bigint           not null
#  aliases         :string           default([]), not null, is an Array
#  simple_group_id :bigint
#
# Indexes
#
#  index_sport_exercises_on_simple_group_id  (simple_group_id)
#  index_sport_exercises_on_slug             (slug) UNIQUE
#  index_sport_exercises_on_user_id          (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (simple_group_id => sport_exercise_simple_groups.id)
#  fk_rails_...  (user_id => users.id)
#
class Sport::Exercise < ApplicationRecord
  belongs_to :simple_group, class_name: 'Sport::Exercise::SimpleGroup', required: false
  belongs_to :user
  has_many :activity_exercises, class_name: 'Sport::ActivityExercise', dependent: :destroy, foreign_key: :exercise_id
  has_many :activities, class_name: 'Sport::Activity', through: :activity_exercises
  validates_presence_of :name
  include VideoUploader::Attachment(:video)
  extend FriendlyId
  friendly_id :slug_candidates, use: %i[slugged finders history]
  def slug_candidates
    [
      [:name],
      %i[id name],
    ]
  end
  after_commit :schedule_refresh_derivatives!
  def schedule_refresh_derivatives!
    Shrine::DerivativesJob.perform_later(self.class, id, :video, false) if should_refresh_shrine_derivatives?(:video)
  end

  searchkick word_start: %i[name aliases]

  def search_data
    attributes.symbolize_keys.slice(:name, :alias, :content, :user_id)
  end

  def video_source_urls
    return video_url if video_derivatives.blank?

    video_derivatives.keys.map { |key| video_url(key) if key.to_s != 'screenshot' }.compact
  end

  def only_video_derivatives
    video_derivatives.select { |key, value| key.to_s != 'screenshot' }
  end

  def video_sources_with_meta(*keys)
    return [{src: video_url}] if video_derivatives.blank?

    only_video_derivatives.values.sort_by(&:size).reverse.map do |derivative|
      meta = derivative.metadata.symbolize_keys
      meta = meta.slice(*keys.map(&:to_sym)) if keys.present?

      meta.merge(src: derivative.url)
    end
  end

  def video_sources_for_part_of_width(part_percent = 0.8)
    video_sources_with_media do |width, height|
      min_width = (width / part_percent).to_i
      next nil if min_width < 360

      "(min-width:#{min_width}px)"
    end
  end

  def video_sources_with_media
    return {src: video_url} if video_derivatives.blank?

    all = only_video_derivatives.values.sort_by(&:size).reverse.map do |derivative|
      width, height = derivative.metadata.symbolize_keys.values_at(:width, :height)
      {
        src: derivative.url,
        media: yield(width, height),
      }.compact
    end

    all.last.delete(:media)

    all
  end

  def simple_group_name=(value)
    clean_value = value.squish
    if clean_value.present?
      self.simple_group_id = nil
      self.simple_group = Sport::Exercise::SimpleGroup.find_or_create_by!(name: clean_value)
    else
      self.simple_group = nil
      self.simple_group_id = nil
    end
  end

  def simple_group_name
    simple_group&.name
  end

  def video_vertical?
    video_ratio < 1.0
  end

  def video_ratio
    wh = video.metadata&.slice("width", "height")&.compact.presence || video(:max160)&.metadata&.slice("width", "height")&.compact.presence
    return if wh.nil?

    width, height = wh.values_at('width', 'height')
    width.to_f / height.to_f
  end
end
