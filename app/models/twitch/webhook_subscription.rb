# == Schema Information
#
# Table name: twitch_webhook_subscriptions
#
#  id            :bigint           not null, primary key
#  callback      :string
#  mode          :string
#  topic         :string
#  lease_seconds :string
#  secret        :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  response_body :text
#  response_code :bigint
#  streamer_id   :bigint
#
# Indexes
#
#  index_twitch_webhook_subscriptions_on_streamer_id  (streamer_id)
#  index_twitch_webhook_subscriptions_on_topic        (topic) WHERE ((mode)::text = 'subscribe'::text)
#
# Foreign Keys
#
#  fk_rails_...  (streamer_id => video_markup_streamers.id)
#
class Twitch::WebhookSubscription < ApplicationRecord
  belongs_to :streamer, class_name: 'VideoMarkup::Streamer', required: false
  before_create :send_to_twitch!
  def send_to_twitch!
    return if User::Identity.count == 0
    self.secret ||= SecureRandom.base64(12)
    resp = HTTParty.post(endpoint, body: build_payload.to_json, headers: {
      'Client-Id' => Rails.application.secrets.twitch_id,
      'Accept' => 'application/vnd.twitchtv.v5+json',
      'Content-type' => 'application/json',
      'Authorization' => "Bearer #{User::Identity.twitch_authorization}"
    })
    self.response_body = resp.body
    self.response_code = resp.code
  end

  def endpoint
    'https://api.twitch.tv/helix/webhooks/hub'.freeze
  end

  def build_payload
    attributes.slice('callback', 'mode', 'topic', 'lease_seconds', 'secret').transform_keys { |k| "hub.#{k}" }
  end
end
