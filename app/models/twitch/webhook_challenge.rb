# == Schema Information
#
# Table name: twitch_webhook_challenges
#
#  id         :bigint           not null, primary key
#  data       :jsonb
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Twitch::WebhookChallenge < ApplicationRecord
end
