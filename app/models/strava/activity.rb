# == Schema Information
#
# Table name: strava_activities
#
#  id                :bigint           not null, primary key
#  user_identity_id  :bigint
#  strava_id         :bigint
#  external_id       :string
#  upload_id         :bigint
#  name              :string
#  distance          :bigint
#  moving_time       :bigint
#  strava_type       :string
#  start_date        :datetime
#  start_point       :point
#  end_point         :point
#  average_speed     :decimal(8, 3)
#  max_speed         :decimal(8, 3)
#  description       :text
#  map_polyline      :text
#  source_data       :jsonb
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  sport_activity_id :bigint
#
# Indexes
#
#  index_strava_activities_on_sport_activity_id  (sport_activity_id)
#  index_strava_activities_on_user_identity_id   (user_identity_id)
#
# Foreign Keys
#
#  fk_rails_...  (sport_activity_id => sport_activities.id)
#  fk_rails_...  (user_identity_id => user_identities.id)
#
class Strava::Activity < ApplicationRecord
  belongs_to :sport_activity, class_name: 'Sport::Activity', required: false
  belongs_to :user_identity, class_name: 'User::Identity'
  has_one :user, through: :user_identity
  has_one :stream_set, dependent: :destroy
  include Shrine::Attachment(:source)

  def refetch_data!
    Strava::ActivityService.new(user_identity).fetch_activity(self)
  end

  def refetch_streams!
    Strava::ActivityService.new(user_identity).fetch_streams(self)
  end

  def refresh_attributes!
    Strava::ActivityService.new(user_identity).refresh_attributes(self)
  end

  def fetched_raw_data
    JSON.parse(source.download.read)
  end

  def fetched_data
    Strava::Models::Activity.new(fetched_raw_data)
  end

  def full_name
    [user&.name, name].join(' — ')
  end

  def strava_url
    "https://www.strava.com/activities/#{strava_id}"
  end

  def to_pace(speed)
    return speed if speed.nil? || speed.zero?

    (1000 / speed).to_i
  end

  def average_pace
    to_pace(average_speed)
  end

  def max_pace
    to_pace(max_speed)
  end

  def strava_type_human
    I18n.t(strava_type, scope: 'strava.activity.type')
  end

  def sport_activity_attributes
    {
      started_at: start_date,
      duration_seconds: moving_time,
      kind: strava_type_human,
      strava_activity_id: id,
      name: name,
    }
  end

  # provider_activity
  def started_at
    start_date
  end

  def kind_human
    strava_type_human
  end

  def provider_name
    "Strava"
  end
  # \provider_activity

  def to_sport_activity
    build_sport_activity(sport_activity_attributes)
  end

  def average_heartrate
    return nil if stream_set.nil?
    return nil if moving_time.nil? || moving_time.zero?

    (stream_set.restored_heartbeats.last * 60) / moving_time
  end

end
