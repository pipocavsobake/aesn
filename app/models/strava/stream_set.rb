# == Schema Information
#
# Table name: strava_stream_sets
#
#  id          :bigint           not null, primary key
#  activity_id :bigint           not null
#  source_data :jsonb
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_strava_stream_sets_on_activity_id  (activity_id)
#
# Foreign Keys
#
#  fk_rails_...  (activity_id => strava_activities.id)
#
class Strava::StreamSet < ApplicationRecord
  KEYS = %i[time distance latlng altitude heartrate velocity_smooth].freeze
  belongs_to :activity

  include Shrine::Attachment(:source)

  def data
    @data ||= JSON.parse(source.download.read)
  end

  KEYS.each do |key|
    define_method key do
      data.dig(key.to_s, "data")
    end
  end

  def pace
    velocity_smooth.map do |velocity|
      next 0 if velocity.nil? || velocity.zero?

      (1000 / velocity).to_i
    end
  end

  def time_human
    time.map do |secs|
      next nil if secs.nil?

      Time.at(secs).gmtime.strftime('%T').delete_prefix('0').delete_prefix('0:')
    end
  end

  def normalized_velocity_smooth
    velocity_smooth.map.with_index do |value, idx|
      [value, 1.85].max
    end
  end

  def normalized_heartrate
    heartrate.map.with_index do |value, idx|
      [value, 80].max
    end
  end

  def restored_heartbeats
    time.zip(heartrate).each_cons(2).reduce([0]) do |result, ((t1, h1), (t2, h2))|
      result + [result.last + (h1 + h2).to_f * (t2 - t1).to_f / (2.0 * 60)]
    end.map(&:to_i)
  end

  def data_to_calc_average_values_in_segments
    {
      distance: distance,
      heartbeats: restored_heartbeats,
      time: time,
    }
  end

  def to_plotly(step: 20)
    {
      data: [{
        x: time_human,
        #y: Arrays.filled_moving_average(velocity_smooth, step, 2),
        y: normalized_velocity_smooth,
        type: :scatter,
        fill: :tozeroy,
        name: 'Темп',
        opacity: 0.5,
      }, {
        x: time_human,
        #y: Arrays.filled_moving_average(heartrate, step, 0),
        y: normalized_heartrate,
        type: :scatter,
        fill: :tonexty,
        name: 'Пульс',
        yaxis: 'y2',
        opacity: 0.5,
      }],
      layout: {
        title: 'Активность',
        selectdirection: :h,
        dragmode: :select,
        xaxis: {nticks: 10},
        yaxis: {
          title: 'Темп',
          type: 'log',
          tickmode: :array,
          fixedrange: true,
          tickvals: [1.85, 2.08, 2.38, 2.78, 3.33, 4.17, 5.56, 8.33],
          ticktext: ['9:00', '8:00', '7:00', '6:00', '5:00', '4:00', '3:00', '2:00'],
        },
        yaxis2: {
          title: 'Пульс',
          titlefont: {color: 'rgb(148, 103, 189)'},
          tickfont: {color: 'rgb(148, 103, 189)'},
          overlaying: 'y',
          side: 'right'
        }
      }
    }
  end

  def dump_csv(path)
    CSV.open(path, 'w') do |csv|
      csv << ['time_in_secs', 'speed_in_m_per_sec', 'heartrate'];
      time.each_with_index do |time, idx|
        csv << [time, set.velocity_smooth[idx], set.heartrate[idx]]
      end
    end
  end
end
