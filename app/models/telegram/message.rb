# == Schema Information
#
# Table name: telegram_messages
#
#  id         :bigint           not null, primary key
#  controller :string
#  payload    :jsonb
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Telegram::Message < ApplicationRecord
  self.table_name = 'telegram_messages'
end
