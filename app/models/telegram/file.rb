# == Schema Information
#
# Table name: telegram_files
#
#  id          :bigint           not null, primary key
#  source      :string
#  telegram_id :string
#  data        :jsonb
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_telegram_files_on_source       (source)
#  index_telegram_files_on_telegram_id  (telegram_id)
#

class Telegram::File < ApplicationRecord
  self.table_name = 'telegram_files'

  def self.save_reply!(type, source, reply)
    create!(
      data: reply.dig('result', type.to_s),
      source: source,
      telegram_id: reply.dig('result', type.to_s, 'file_id'),
    )
  end

  def send_media
    return send_new_audio if telegram_id.blank?
  end

  def send_new_audio(bot, chat_id, params = {})
    r = bot.send_audio(params.merge(
      chat_id: chat_id,
      audio: File.open(source)
    ))
    self.data = r['result']['audio']
    self.telegram_id = data['file_id']
    self
  end

  def resend_audio(bot, chat_id, params)
    bot.send_audio(params.merge(
      chat_id: chat_id,
      audio: telegram_id,
    ))
  end
end
