# == Schema Information
#
# Table name: pages
#
#  id            :bigint           not null, primary key
#  title         :string
#  fullpath      :string           not null
#  content       :text
#  h1            :string
#  keywords      :text
#  description   :text
#  og_title      :string
#  robots        :string
#  og_image_data :jsonb
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  subdomain     :string
#
# Indexes
#
#  index_pages_on_fullpath                (fullpath) UNIQUE WHERE (subdomain IS NULL)
#  index_pages_on_subdomain_and_fullpath  (subdomain,fullpath)
#

class Page < ApplicationRecord
  include OgImageUploader::Attachment(:og_image)
  validates :fullpath, presence: true, uniqueness: {scope: :subdomain}
  validates :h1, :title, :description, presence: true
end
