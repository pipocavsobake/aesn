# == Schema Information
#
# Table name: page_templates
#
#  id          :bigint           not null, primary key
#  title       :text
#  fullpath    :string
#  content     :text
#  h1          :text
#  keywords    :text
#  description :text
#  og_title    :text
#  robots      :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  subdomain   :string
#

class Page::Template < ApplicationRecord
  FIELDS = %w[title content h1 keywords description og_title robots]

  def render(resource)
    Page.new(
      FIELDS.each_with_object({}) do |field, page_attributes|
        template = Liquid::Template.parse(send(field))
        page_attributes[field] = template.render(
          'resource' => resource.to_liquid,
          'global' => {
            'video_support' => ['youtube', 'twitch'],
          }
        )
      end
    )
  end

end
