# == Schema Information
#
# Table name: user_telegram_chats
#
#  id          :bigint           not null, primary key
#  user_id     :bigint           not null
#  telegram_id :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_user_telegram_chats_on_telegram_id  (telegram_id) UNIQUE
#  index_user_telegram_chats_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

class User::TelegramChat < ApplicationRecord
  belongs_to :user
end
