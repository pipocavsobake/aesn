# == Schema Information
#
# Table name: user_merges
#
#  id               :bigint           not null, primary key
#  identity_id      :bigint           not null
#  email            :string
#  token_expires_at :datetime         not null
#  token            :string           not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_user_merges_on_identity_id  (identity_id)
#  index_user_merges_on_token        (token) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (identity_id => user_identities.id)
#
class User::Merge < ApplicationRecord
  belongs_to :identity
  has_one :user, through: :identity
  before_save do
    self.token_expires_at = 1.day.after
    self.token = SecureRandom.hex(64)
  end

  validate do
    next if target_user

    errors.add(:email, "Такого пользователя не существует")
  end

  after_save do
    User::MergeMailer.confirm(self).deliver_now
  end

  def target_user
    User.find_by(email: email)
  end

  def target_user!
    User.find_by!(email: email)
  end

  def confirm!
    target_id = target_user.id

    User.reflect_on_all_associations(:has_many).each do |association|
      user.send(association.name).update_all({association.foreign_key => target_id})
    end
    User.reflect_on_all_associations(:has_one).each do |association|
      user.send(association.name)&.update_columns({association.foreign_key => target_id})
    end
    User.reflect_on_all_associations(:has_and_belongs_to_many).each do |association|
      target_user.send("#{association.name}+=", user.send(association.name))
      association.klass.where({association.foreign_key => user.id}).update_all({association.foreign_key => target_id})
    end
  end

  def confirm_in_transaction!
    transaction { confirm! }
  end
end
