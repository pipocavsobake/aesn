# == Schema Information
#
# Table name: user_identities
#
#  id               :bigint           not null, primary key
#  uid              :string
#  provider         :string
#  name             :string
#  user_id          :bigint           not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  token            :string
#  token_expires_at :datetime
#  refresh_token    :string
#
# Indexes
#
#  index_user_identities_on_provider_and_uid  (provider,uid) UNIQUE
#  index_user_identities_on_user_id           (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

class User::Identity < ApplicationRecord
  belongs_to :user
  has_many :merges, dependent: :destroy
  has_many :strava_activities, class_name: 'Strava::Activity', dependent: :destroy, foreign_key: :user_identity_id
  has_many :strava_syncs, class_name: 'Strava::Sync', dependent: :destroy, foreign_key: :user_identity_id

  def self.twitch_authorization_identity
    find_by(uid: '489799013', provider: :twitch)
  end

  def self.twitch_authorization
    twitch_authorization_identity.get_active_token
  end

  def get_active_token
    return token if token_expires_at.try { |time| time > 5.minutes.after }

    return nil if refresh_token.nil?

    strategy = get_strategy
    at = strategy.access_token
    at = at.refresh!
    update!(token: at.token, refresh_token: at.refresh_token, token_expires_at: Time.at(at.expires_at))
    at.token
  end

  def refresh_options
    {refresh_token: refresh_token, expires_at: token_expires_at}
  end

  def get_strategy
    User::Identity.get_strategy(provider, token, refresh_options)
  end

  def self.get_strategy(provider, token, token_options = {})
    rake_fake_app = {}
    provider = provider.to_sym
    options = Devise.omniauth_configs[provider].strategy
    strategy = Devise.omniauth_configs[provider].strategy_class.new(rake_fake_app, options)
    client = OAuth2::Client.new(
      options.client_id,
      options.client_secret,
      options.client_options.to_h.deep_symbolize_keys
    )
    strategy.access_token = OAuth2::AccessToken.new(client, token, token_options)
    strategy
  end

  def build_email_for_omniauth
    User.build_omniauth_email(provider, uid)
  end

  def profile_label
    "#{provider} — #{name}"
  end

  def profile_url
    case provider&.to_sym
    when :facebook
      "https://www.facebook.com/profile.php?id=#{uid}"
    when :vkontakte
      "https://vk.com/id#{uid}"
    when :google_oauth2
      nil
    when :spotify
      "https://open.spotify.com/user/#{uid}"
    when :twitch
      "https://www.twitch.tv/#{name}"
    when :strava
      "https://www.strava.com/athletes/#{uid}"
    end
  end

  def api_client
    case provider&.to_sym
    when :strava
      Strava::Api::Client.new(access_token: get_active_token)
    end
  end
end
