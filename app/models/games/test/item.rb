# == Schema Information
#
# Table name: games_test_items
#
#  id         :bigint           not null, primary key
#  user_id    :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  strings    :string           default([]), is an Array
#  floats     :float            default([]), is an Array
#  ints       :bigint           default([]), is an Array
#
# Indexes
#
#  index_games_test_items_on_user_id  (user_id)
#

class Games::Test::Item < ApplicationRecord
end
