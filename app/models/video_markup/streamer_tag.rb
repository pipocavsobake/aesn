# == Schema Information
#
# Table name: video_markup_streamer_tags
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_video_markup_streamer_tags_on_name  (name)
#
class VideoMarkup::StreamerTag < ApplicationRecord
  has_and_belongs_to_many :tags,
    class_name: 'VideoMarkup::Streamer',
    foreign_key: :tag_id,
    association_foreign_key: :streamer_id,
    join_table: :video_markup_streamers_tags
end
