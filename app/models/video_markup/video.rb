# == Schema Information
#
# Table name: video_markup_videos
#
#  id               :bigint           not null, primary key
#  type             :string
#  slug             :string
#  streamer_id      :bigint           not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  title            :string
#  published_at     :datetime
#  snippet_template :string
#
# Indexes
#
#  index_video_markup_videos_on_streamer_id  (streamer_id)
#
# Foreign Keys
#
#  fk_rails_...  (streamer_id => video_markup_streamers.id)
#

class VideoMarkup::Video < ApplicationRecord
  class KnownError < StandardError; end
  class NotEmbeddable < KnownError; def message; "Видео запрещено показывать на других сайтах"; end; end
  class NotPublic < KnownError; def message; "Нет публичного доступа к видео"; end; end
  class NotUploaded < KnownError; def message; "Видео ещё не загружено полностью"; end; end
  belongs_to :streamer
  has_many :moments, dependent: :destroy
  YOUTUBE_REGEXP = /^(?:https?:\/\/)?(?:www\.|m\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=)?([\w-]{10,})/i
  TWITCH_REGEXP =  /^(?:https?:\/\/)?(?:www\.|go\.)?twitch\.tv\/videos\/([a-z0-9_]+)($|\?)/i

  def self.youtube?(url)
    return false if url.nil?
    !url.match(YOUTUBE_REGEXP).nil?
  end

  def self.youtube_id(url)
    return nil if url.blank?
    m = url.match(YOUTUBE_REGEXP)
    return nil if m.nil?
    m[1]
  end

  def self.twitch?(url)
    return false if url.nil?
    !url.match(TWITCH_REGEXP).nil?
  end

  def self.twitch_id(url)
    return nil if url.blank?
    m = url.match(TWITCH_REGEXP)
    return nil if m.nil?
    m[1]
  end

  def kind
    self.class.name.demodulize.underscore
  end

  def remote_url

  end


  def self.create_from_url(url)
    youtube_id(url).try do |slug|
      v = VideoMarkup::Video::Youtube.find_by(slug: slug)
      return v if v && v.title && v.published_at

      resp = Youtube.new(slug: slug).remote_data
      item = resp['items'][0]
      status = item['status']
      raise NotEmbeddable if !status['embeddable']
      raise NotPublic if status['privacyStatus'] != 'public'
      raise NotUploaded if status['uploadStatus'] != 'processed'
      data = item['snippet']
      streamer = VideoMarkup::Streamer.find_or_initialize_by(slug: data['channelId'], kind: 'youtube')
      streamer.update!(name: data['channelTitle'])
      video = streamer.youtube_videos.find_or_initialize_by(slug: slug)
      video.update!(title: data['title'], published_at: data['publishedAt'])
      video
    end || twitch_id(url).try do |slug|
      v = VideoMarkup::Video::Twitch.find_by(slug: slug)
      return v if v && v.title && v.published_at

      resp = HTTParty.get(
        "https://api.twitch.tv/kraken/videos/#{slug}",
        headers: {
          'Client-ID' => Rails.application.secrets.twitch_client_id,
          'Accept' => 'application/vnd.twitchtv.v5+json'
        }
      )
      raise resp.body if resp.code > 299
      data = resp.parsed_response
      streamer = VideoMarkup::Streamer.find_or_initialize_by(slug: data['channel']['_id'], kind: 'twitch')
      streamer.update!(name: data['channel']['name'])
      video = streamer.twitch_videos.find_or_initialize_by(slug: slug)
      video.update!(title: data['title'], published_at: data['published_at'], snippet_template: data.dig('preview', 'template'))
      video
    end
  end

  def snippet_url

  end
end
