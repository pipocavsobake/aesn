# == Schema Information
#
# Table name: video_markup_moments
#
#  id          :bigint           not null, primary key
#  value       :bigint
#  video_id    :bigint           not null
#  user_id     :bigint
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  stop_at     :bigint
#  playlist_id :bigint
#
# Indexes
#
#  index_video_markup_moments_on_playlist_id  (playlist_id)
#  index_video_markup_moments_on_user_id      (user_id)
#  index_video_markup_moments_on_video_id     (video_id)
#
# Foreign Keys
#
#  fk_rails_...  (playlist_id => video_markup_playlists.id)
#  fk_rails_...  (user_id => users.id)
#  fk_rails_...  (video_id => video_markup_videos.id)
#

class VideoMarkup::Moment < ApplicationRecord
  belongs_to :video
  belongs_to :user
  belongs_to :playlist, required: false
  has_many :heroes_fights, dependent: :destroy, class_name: 'VideoMarkup::Heroes::Fight'
  accepts_nested_attributes_for :heroes_fights
  has_many :heroes_game_starts, dependent: :destroy, class_name: 'VideoMarkup::Heroes::GameStart'
  has_many :comments, class_name: 'VideoMarkup::Comment', dependent: :destroy
  accepts_nested_attributes_for :comments
  has_many :heroes_learn_skills, dependent: :destroy, class_name: 'VideoMarkup::Heroes::LearnSkill'
  accepts_nested_attributes_for :heroes_learn_skills, allow_destroy: true

  def remote_url
    video.remote_url(moment: self)
  end

  def name
    "#{colon_value} (#{value} сек) #{video&.title}"
  end

  def as_json(options = nil)
    attributes.merge({
      user_email: user.email,
      heroes_fights: heroes_fights.map(&:as_json),
      heroes_learn_skills: heroes_learn_skills.map(&:as_json),
      comments: comments.map(&:as_json),
      full_text: full_text,
      youtube_text: youtube_text,
      can: current_ability,
    })
  end

  def full_text
    "#{value} сек#{end_text} — #{related_text}"
  end

  def related_text
    [
      *heroes_fights,
      *comments,
      *heroes_learn_skills
    ].map(&:full_text).join(', ')
  end

  def youtube_text
    "#{colon_value} #{heroes_fights.map(&:full_text).join(", ")}"
  end

  def end_text
    return '' if stop_at.nil?

    " (по #{stop_at})"
  end

  def hh_mm_ss
    mm, ss = [(value / 60) % 60, value % 60].map { |v| v.to_s.rjust(2, '0') }
    [value > 3600 ? value / 3600 : nil, mm, ss]
  end

  def colon_value
    hh_mm_ss.compact.join(':')
  end
end
