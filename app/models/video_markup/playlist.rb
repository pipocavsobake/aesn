# == Schema Information
#
# Table name: video_markup_playlists
#
#  id         :bigint           not null, primary key
#  user_id    :bigint           not null
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_video_markup_playlists_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class VideoMarkup::Playlist < ApplicationRecord
  belongs_to :user
  has_many :moments, dependent: :nullify
end
