# == Schema Information
#
# Table name: video_markup_videos
#
#  id               :bigint           not null, primary key
#  type             :string
#  slug             :string
#  streamer_id      :bigint           not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  title            :string
#  published_at     :datetime
#  snippet_template :string
#
# Indexes
#
#  index_video_markup_videos_on_streamer_id  (streamer_id)
#
# Foreign Keys
#
#  fk_rails_...  (streamer_id => video_markup_streamers.id)
#

module VideoMarkup
  class Video::Twitch < Video
    def snippet_url(width: 400, height: 325)
      return nil unless snippet_template

      snippet_template.gsub('%{width}', width.to_s).gsub('%{height}', height.to_s)
    end

    def time_str(hh_mm_ss)
      return nil if hh_mm_ss.nil?

      hh, mm, ss = hh_mm_ss
      hh = '00' if hh.nil?
      hh = hh.to_s.rjust(2, '0')
      "#{hh}h#{mm}m#{ss}s"
    end

    # https://www.twitch.tv/videos/548392840?t=00h00m06s
    def remote_url(moment: nil)
      params = ({
        t: time_str(moment&.hh_mm_ss)
      }).compact
      "https://www.twitch.tv/videos/#{slug}?#{params.to_query}"
    end

    def as_json(options=nil)
      super(options).merge(js_type: 'twitch')
    end
  end
end
