# == Schema Information
#
# Table name: video_markup_videos
#
#  id               :bigint           not null, primary key
#  type             :string
#  slug             :string
#  streamer_id      :bigint           not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  title            :string
#  published_at     :datetime
#  snippet_template :string
#
# Indexes
#
#  index_video_markup_videos_on_streamer_id  (streamer_id)
#
# Foreign Keys
#
#  fk_rails_...  (streamer_id => video_markup_streamers.id)
#

module VideoMarkup
  class Video::Youtube < Video
    def snippet_url
      "https://i.ytimg.com/vi/#{slug}/hqdefault.jpg"
    end

    def remote_url(moment: nil)
      params = ({
        v: slug,
        t: moment&.value
      }).compact
      "https://www.youtube.com/watch?#{params.to_query}"
    end

    #validate :old_enough

    def old_enough
      return if old_enough?

      errors.add(:published_at, 'должен быть более суток назад')
    end

    def old_enough?
      published_at && (published_at + 1.day < Time.now)
    end

    def remote_data
      HTTParty.get(
        "https://www.googleapis.com/youtube/v3/videos",
        query: {
          part: 'snippet,status',
          id: slug,
          key: Rails.application.secrets.youtube_key
        }
      ).parsed_response
    end

    def as_json(options=nil)
      super(options=nil).merge(js_type: 'youtube')
    end
  end
end
