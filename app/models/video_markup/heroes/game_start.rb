# == Schema Information
#
# Table name: video_markup_heroes_game_starts
#
#  id                 :bigint           not null, primary key
#  moment_id          :bigint           not null
#  template_id        :bigint           not null
#  opponent_id        :bigint           not null
#  hero_id            :bigint           not null
#  opponent_hero_id   :bigint           not null
#  castle_id          :bigint           not null
#  opponent_castle_id :bigint           not null
#  difficulty         :bigint
#  red                :boolean
#  bonus              :bigint
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_video_markup_heroes_game_starts_on_castle_id           (castle_id)
#  index_video_markup_heroes_game_starts_on_hero_id             (hero_id)
#  index_video_markup_heroes_game_starts_on_moment_id           (moment_id)
#  index_video_markup_heroes_game_starts_on_opponent_castle_id  (opponent_castle_id)
#  index_video_markup_heroes_game_starts_on_opponent_hero_id    (opponent_hero_id)
#  index_video_markup_heroes_game_starts_on_opponent_id         (opponent_id)
#  index_video_markup_heroes_game_starts_on_template_id         (template_id)
#
# Foreign Keys
#
#  fk_rails_...  (castle_id => video_markup_heroes_castles.id)
#  fk_rails_...  (hero_id => video_markup_heroes_heroes.id)
#  fk_rails_...  (moment_id => video_markup_moments.id)
#  fk_rails_...  (opponent_castle_id => video_markup_heroes_castles.id)
#  fk_rails_...  (opponent_hero_id => video_markup_heroes_heroes.id)
#  fk_rails_...  (opponent_id => video_markup_streamers.id)
#  fk_rails_...  (template_id => video_markup_heroes_templates.id)
#

class VideoMarkup::Heroes::GameStart < ApplicationRecord
  belongs_to :moment
  belongs_to :template
  belongs_to :opponent, class_name: 'VideoMarkup::Streamer'
  belongs_to :hero
  belongs_to :opponent_hero, class_name: 'VideoMarkup::Heroes::Hero'
  belongs_to :castle
  belongs_to :opponent_castle, class_name: 'VideoMarkup::Heroes::Castle'
end
