# == Schema Information
#
# Table name: video_markup_heroes_heroes
#
#  id                   :bigint           not null, primary key
#  name                 :string
#  castle_id            :bigint
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  enabled_fights_count :bigint           default(0), not null
#  image_data           :jsonb
#  cases                :jsonb            not null
#  sex                  :string
#  content              :text
#  aliases              :string           is an Array
#
# Indexes
#
#  index_video_markup_heroes_heroes_on_castle_id             (castle_id)
#  index_video_markup_heroes_heroes_on_castle_id_and_name    (castle_id,name) UNIQUE
#  index_video_markup_heroes_heroes_on_enabled_fights_count  (enabled_fights_count)
#
# Foreign Keys
#
#  fk_rails_...  (castle_id => video_markup_heroes_castles.id)
#

class VideoMarkup::Heroes::Hero < ApplicationRecord
  belongs_to :castle
  has_many :game_starts, dependent: :destroy
  has_many :game_opponent_starts, dependent: :destroy, foreign_key: :opponent_hero_id, class_name: 'GameStart'
  include Select2Label
  include VideoMarkup::Heroes::WithFights

  searchkick

  include Shrine::Attachment(:image)

  def select_name
    "#{name} (#{castle.name})"
  end

  def full_name
    "#{name} (#{castle&.name || 'нейтрал'}; #{enabled_fights_count})"
  end

  def slug
    "hero-#{id}"
  end

  def to_liquid
    attributes
  end
end
