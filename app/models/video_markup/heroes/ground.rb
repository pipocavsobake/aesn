# == Schema Information
#
# Table name: video_markup_heroes_grounds
#
#  id         :bigint           not null, primary key
#  name       :string
#  penalty    :bigint
#  cases      :jsonb            not null
#  image_data :jsonb
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class VideoMarkup::Heroes::Ground < ApplicationRecord
  include Shrine::Attachment(:image)
  include Select2Label

  def select_name
    name
  end
end
