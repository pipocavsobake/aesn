# == Schema Information
#
# Table name: video_markup_heroes_templates
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  cases      :jsonb            not null
#

class VideoMarkup::Heroes::Template < ApplicationRecord
  has_many :game_starts, dependent: :destroy
end
