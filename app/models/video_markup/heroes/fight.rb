# == Schema Information
#
# Table name: video_markup_heroes_fights
#
#  id                    :bigint           not null, primary key
#  moment_id             :bigint           not null
#  hero_id               :bigint           not null
#  attack                :bigint
#  defence               :bigint
#  spell_power           :bigint
#  knowledge             :bigint
#  day                   :bigint
#  better_than_autofight :boolean
#  units_count           :bigint
#  upgraded_units_count  :bigint
#  bank_value            :bigint
#  bank_id               :bigint
#  unit_id               :bigint
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  template_id           :bigint
#  enabled_by_id         :bigint
#  ground_id             :bigint
#  bank_graded           :boolean
#  ai_value              :bigint
#
# Indexes
#
#  index_video_markup_heroes_fights_on_bank_id        (bank_id)
#  index_video_markup_heroes_fights_on_enabled_by_id  (enabled_by_id)
#  index_video_markup_heroes_fights_on_ground_id      (ground_id)
#  index_video_markup_heroes_fights_on_hero_id        (hero_id)
#  index_video_markup_heroes_fights_on_moment_id      (moment_id)
#  index_video_markup_heroes_fights_on_template_id    (template_id)
#  index_video_markup_heroes_fights_on_unit_id        (unit_id)
#
# Foreign Keys
#
#  fk_rails_...  (bank_id => video_markup_heroes_banks.id)
#  fk_rails_...  (enabled_by_id => users.id)
#  fk_rails_...  (ground_id => video_markup_heroes_grounds.id)
#  fk_rails_...  (hero_id => video_markup_heroes_heroes.id)
#  fk_rails_...  (moment_id => video_markup_moments.id)
#  fk_rails_...  (template_id => video_markup_heroes_templates.id)
#  fk_rails_...  (unit_id => video_markup_heroes_units.id)
#

class VideoMarkup::Heroes::Fight < ApplicationRecord
  belongs_to :moment
  belongs_to :hero
  belongs_to :unit, required: false
  belongs_to :bank, required: false
  belongs_to :template, required: false
  belongs_to :enabled_by, class_name: '::User', required: false
  belongs_to :ground, required: false
  has_many :spell_usages, class_name: 'VideoMarkup::Heroes::SpellUsage', dependent: :destroy
  has_many :spells, through: :spell_usages

  scope :enabled, -> { where.not(enabled_by: nil) }
  scope :disabled, -> { where(enabled_by: nil) }
  scope :user_id_eq, -> (user_id = 1) {
    joins(:moment).where(video_markup_moments: {user_id: user_id})
  }
  scope :user_id_not_eq, -> (user_id = 1) {
    joins(:moment).where.not(video_markup_moments: {user_id: user_id})
  }
  scope :bank, -> { where.not(bank_id: nil) }
  scope :unit, -> { where.not(unit_id: nil) }
  scope :tempest, -> { enabled.where.not(ai_value: nil).reorder(day: :asc, ai_value: :desc) }

  def self.ransackable_scopes(auth_object = nil)
    %i[user_id_eq user_id_not_eq]
  end

  def enabled?; enabled_by_id.present?; end

  delegate :user_id, to: :moment
  delegate :stop_at, to: :moment
  delegate :stop_at=, to: :moment
  after_save do
    if moment.stop_at_changed?
      moment.save
    end
  end

  counter_culture :hero, column_name: proc { |model| model.enabled? ? 'enabled_fights_count' : nil }
  counter_culture :unit, column_name: proc { |model| model.enabled? ? 'enabled_fights_count' : nil }
  counter_culture :bank, column_name: proc { |model| model.enabled? ? 'enabled_fights_count' : nil }

  include VideoMarkup::Heroes::FightText
  def as_json(options=nil)
    attributes.merge(can: current_ability, spell_ids: spell_ids)
  end

  before_save :set_ai_value

  def set_ai_value(options = {})
    bank.try do |bank|
      next if bank_value.nil?
      self.ai_value = bank.ai_values[4 * (bank_graded ? 1 : 0) + bank_value]
    end
    unit.try do |unit|
      self.ai_value = [
        upgraded_units_count.try do |c|
          next if c == 0
          ((options.dig(:units, unit.level, unit.grade_level + 1) || VideoMarkup::Heroes::Unit.where(level: unit.level, grade_level: unit.grade_level + 1).first)&.ai_value || 0) * c
        end,
        (units_count || 0) * unit.ai_value
      ].compact.sum
    end
  end

end
