# == Schema Information
#
# Table name: video_markup_heroes_units
#
#  id                   :bigint           not null, primary key
#  name                 :string
#  castle_id            :bigint
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  enabled_fights_count :bigint           default(0), not null
#  image_data           :jsonb
#  level                :bigint
#  grade_level          :bigint           default(0), not null
#  cases                :jsonb            not null
#  ground_id            :bigint
#  ai_value             :bigint
#  fight_value          :bigint
#  en_name              :string
#  attack               :bigint
#  defence              :bigint
#  damage_min           :bigint
#  damage_max           :bigint
#  speed                :bigint
#  pop_growth           :bigint
#  skills               :text
#  aliases              :string           is an Array
#  health_points        :bigint
#  gold                 :bigint
#
# Indexes
#
#  index_video_markup_heroes_units_on_castle_id             (castle_id)
#  index_video_markup_heroes_units_on_enabled_fights_count  (enabled_fights_count)
#  index_video_markup_heroes_units_on_ground_id             (ground_id)
#  index_video_markup_heroes_units_on_name                  (name) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (castle_id => video_markup_heroes_castles.id)
#  fk_rails_...  (ground_id => video_markup_heroes_grounds.id)
#

class VideoMarkup::Heroes::Unit < ApplicationRecord
  GOLEM_IDS = [33, 34, 145, 157, 159]
  belongs_to :castle, required: false
  include Select2Label
  include VideoMarkup::Heroes::WithFights

  include Shrine::Attachment(:image)
  validates :ai_value, presence: true

  searchkick

  def grade_level_txt
    '+' * grade_level
  end
  def select_name
    "#{name} (#{castle&.name || 'нейтрал'}; #{level}#{grade_level_txt})"
  end

  def full_name
    "#{name} (#{castle&.name || 'нейтрал'}; #{enabled_fights_count})"
  end

  def slug
    "unit-#{id}"
  end

  def to_liquid
    attributes.merge("castle" => castle.to_liquid)
  end

  def golem?
    GOLEM_IDS.include?(id)
  end

  scope :golems, -> { where(id: GOLEM_IDS) }

  def siblings
    if golem?
      return self.class.golems.where.not(id: id).to_a
    end

    return [] if castle_id.nil?

    self.class.where(level: level, castle_id: castle_id).where.not(id: id).to_a
  end
end
