# == Schema Information
#
# Table name: video_markup_heroes_learn_skills
#
#  id          :bigint           not null, primary key
#  skill_id    :bigint
#  hero_id     :bigint           not null
#  moment_id   :bigint           not null
#  level       :bigint           default("base")
#  attack      :bigint           default(0)
#  defence     :bigint           default(0)
#  spell_power :bigint           default(0)
#  knowledge   :bigint           default(0)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_video_markup_heroes_learn_skills_on_hero_id    (hero_id)
#  index_video_markup_heroes_learn_skills_on_moment_id  (moment_id)
#  index_video_markup_heroes_learn_skills_on_skill_id   (skill_id)
#
# Foreign Keys
#
#  fk_rails_...  (hero_id => video_markup_heroes_heroes.id)
#  fk_rails_...  (moment_id => video_markup_moments.id)
#  fk_rails_...  (skill_id => video_markup_heroes_skills.id)
#
class VideoMarkup::Heroes::LearnSkill < ApplicationRecord
  belongs_to :skill, required: false
  belongs_to :hero
  belongs_to :moment
  validate :at_least_one_skill
  extend Enumerize
  enumerize :level, in: {
    base: 1,
    advanced: 2,
    expert: 3,
  }

  def primaries
    [attack, defence, spell_power, knowledge]
  end

  def at_least_one_skill
    return if primaries.compact.find(&:positive?) || skill

    (VideoMarkup::Heroes::Skill::PRIMARIES + [:skill]).each do |field|
      errors.add(field, 'Необходимо указать хотя бы один навык')
    end
  end

  def full_text
    "#{hero.name} качнул#{skill_text}#{primary_text}"
  end

  def skill_text
    return if skill.nil?

    " #{level.text} #{skill.name}"
  end

  def primary_text
    return unless primaries.compact.find(&:positive?)

    " #{primaries.map { |pr| "+#{pr}" }.join(', ')}"
  end

  def as_json(options=nil)
    attributes.merge('level' => level.value)
  end
end
