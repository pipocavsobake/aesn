# == Schema Information
#
# Table name: video_markup_heroes_skills
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_video_markup_heroes_skills_on_name  (name) UNIQUE
#
class VideoMarkup::Heroes::Skill < ApplicationRecord
  has_many :learn_skills, class_name: 'VideoMarkup::Heroes::Skill', dependent: :destroy
  PRIMARIES = %i[attack defence spell_power knowledge].freeze
  include Select2Label
  def select_name
    name
  end
end
