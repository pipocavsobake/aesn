# == Schema Information
#
# Table name: video_markup_heroes_spells
#
#  id           :bigint           not null, primary key
#  name         :string
#  elements     :string           is an Array
#  file_data    :jsonb
#  level        :bigint
#  kind         :string
#  spell_points :bigint
#  duration     :string
#  base         :string
#  advanced     :string
#  expert       :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  aliases      :string           is an Array
#
class VideoMarkup::Heroes::Spell < ApplicationRecord
  has_many :usages, class_name: 'VideoMarkup::Heroes::SpellUsage', dependent: :destroy
  include Shrine::Attachment(:file)
  include Select2Label

  searchkick

  def cases
    {
      "gent_sing" => 'заклинания #{name}'
    }
  end
  def select_name
    name
  end
end
