# == Schema Information
#
# Table name: video_markup_heroes_castles
#
#  id             :bigint           not null, primary key
#  name           :string
#  ground_penalty :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  image_data     :jsonb
#
# Indexes
#
#  index_video_markup_heroes_castles_on_name  (name) UNIQUE
#

class VideoMarkup::Heroes::Castle < ApplicationRecord
  has_many :heroes, dependent: :destroy
  has_many :game_starts, dependent: :destroy
  has_many :game_opponent_starts, dependent: :destroy, foreign_key: :opponent_castle_id, class_name: 'GameStart'
  has_many :units, dependent: :destroy

  include Shrine::Attachment(:image)

  def to_liquid
    attributes
  end

end
