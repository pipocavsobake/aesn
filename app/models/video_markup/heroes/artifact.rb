# == Schema Information
#
# Table name: video_markup_heroes_artifacts
#
#  id                      :bigint           not null, primary key
#  name                    :string
#  en_name                 :string
#  aliases                 :string           is an Array
#  level                   :bigint
#  attack                  :bigint
#  defence                 :bigint
#  spell_power             :bigint
#  knowledge               :bigint
#  image_data              :jsonb
#  value                   :bigint
#  slot                    :bigint
#  combination_artifact_id :bigint
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#
# Indexes
#
#  index_video_markup_heroes_artifacts_on_combination_artifact_id  (combination_artifact_id)
#
# Foreign Keys
#
#  fk_rails_...  (combination_artifact_id => video_markup_heroes_artifacts.id)
#
class VideoMarkup::Heroes::Artifact < ApplicationRecord
  belongs_to :combination_artifact, class_name: 'VideoMarkup::Heroes::Artifact', required: false

  include Shrine::Attachment(:image)

  searchkick

  def cases
    {
      'gent_sing' => "артефакта #{name}",
    }
  end

  extend Enumerize
  enumerize :slot, in: {
    left_hand: 1,
    right_hand: 2,
    head: 3,
    finger: 4,
    neck: 5,
    shoulders: 6,
    body: 7,
    foot: 8,
    other: 100,
  }
  enumerize :level, in: {
    treasure: 1,
    minor: 2,
    major: 3,
    relic: 4,
    special: 5,
  }
end
