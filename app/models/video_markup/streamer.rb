# == Schema Information
#
# Table name: video_markup_streamers
#
#  id         :bigint           not null, primary key
#  slug       :string
#  name       :string
#  kind       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  tts        :string
#

class VideoMarkup::Streamer < ApplicationRecord
  has_many :heroes_game_starts, foreign_key: :opponent, dependent: :destroy, class_name: 'VideoMarkup::Heroes::GameStart'
  extend Enumerize
  enumerize :kind, in: %i[youtube twitch]
  has_many :videos, dependent: :destroy
  has_many :youtube_videos, -> { where(type: 'VideoMarkup::Video::Youtube')}, class_name: 'VideoMarkup::Video::Youtube'
  has_many :twitch_videos, -> { where(type: 'VideoMarkup::Video::Twitch')}, class_name: 'VideoMarkup::Video::Twitch'
  has_many :statuses, class_name: 'VideoMarkup::StreamerStatus', dependent: :destroy, foreign_key: :streamer_id
  has_and_belongs_to_many :tags,
    class_name: 'VideoMarkup::StreamerTag',
    foreign_key: :streamer_id,
    association_foreign_key: :tag_id,
    join_table: :video_markup_streamers_tags
  has_many :twitch_webhook_subscriptions, class_name: 'Twitch::WebhookSubscription', foreign_key: :streamer_id, dependent: :destroy
  def create_webhook_subscription!
    raise 'cannot create webhook subscription for non twitch streamer' if kind != 'twitch'
    twitch_webhook_subscriptions.create!(
      callback: "https://aesn.ru/webhooks/twitch/#{id}",
      mode: 'subscribe',
      topic: "https://api.twitch.tv/helix/streams?user_id=#{slug}",
      lease_seconds: 3_600 * 24 * 10,
    )
  end
end
