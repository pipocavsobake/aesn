# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    if user&.admin?
      can :read, :all
      can :manage, :all
      return
    end
    if user&.page_editor?
      can :manage, [
        Page,
        Page::Template,
      ]
    end
    if user&.saiu?
      can :manage, Saiu::Base.descendants
      can :manage, Page, subdomain: 'saiu'
    end
    if user
      can :read, :all
      cannot :read, User
      cannot :read, [
        Marusia::Phrase,
        Marusia::Alias,
        Marusia::Request,
        Twitch::WebhookEvent,
        Twitch::WebhookSubscription,
        Twitch::WebhookChallenge,
      ]
      can :manage, User, id: user.id
      can :create, [
        VideoMarkup::Streamer,
        VideoMarkup::Video,
        VideoMarkup::Video::Youtube,
        VideoMarkup::Video::Twitch,
        VideoMarkup::Moment,
      ]
      can :manage, VideoMarkup::Moment, user: user
      can :manage, VideoMarkup::Heroes::Fight, -> { user_id_eq(user.id) } do |fight|
        fight.moment.user_id == user.id
      end
      can :manage, Sport::Exercise, user_id: user.id
    else
      can :read, Sport::Activity, enabled: true
    end
  end

  def self.current
    RequestStore.store[:current_ability]
  end
end
