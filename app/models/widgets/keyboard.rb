# == Schema Information
#
# Table name: widgets_keyboards
#
#  id                  :bigint           not null, primary key
#  name                :string
#  slug                :string
#  keys                :bigint           default(88)
#  first_freq          :float            default(27.5)
#  block_multiplicator :float            default(2.0)
#  block_schema        :bigint           default(["3", "4"]), is an Array
#  start_block_at      :bigint           default(3)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  enabled             :boolean
#  has_samples         :boolean
#
# Indexes
#
#  index_widgets_keyboards_on_slug  (slug) UNIQUE
#

class Widgets::Keyboard < ApplicationRecord
  class Key
    include AssignReaders
    attr_reader :color, :left_black, :right_black, :frequency, :relative_number, :block_number, :id, :sample
  end
  scope :enabled, -> { where(enabled: true) }
  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged
  def slug_candidates
    [
      :name,
      %i[name block_schema_string],
      %i[name block_schema_string block_multiplicator],
      %i[name block_schema_string block_multiplicator keys start_block_at]
    ]
  end

  validates :name, :keys, :first_freq, :block_multiplicator, :block_schema, :start_block_at, presence: true
  validate :check_first_freq
  validate :check_last_freq
  validate :check_block_schema_size

  def data
    attributes
  end

  def block_schema_string
    block_schema&.join('-')
  end

  def block_schema_string=(value)
    self.block_schema = value.split('-').map(&:to_i)
  end

  def keys_in_block
    block_schema.map { |sub| 2 * sub - 1 }.sum
  end

  def block_profile
    @block_profile ||= block_schema.each_with_object([]) do |sub, obj|
      sub.times { obj << :white; obj << :black }
      obj.pop
    end
  end

  def full_blocks
    (keys - start_block_at) / keys_in_block
  end

  def last_keys
    (keys - start_block_at) % keys_in_block
  end

  def key_colors
    [
      start_block_at == 0 ? [] : block_profile[-start_block_at..-1],
      block_profile * full_blocks,
      block_profile[0...last_keys]
    ].flatten
  end

  def mult_for(idx)
    ([block_multiplicator] * (idx / keys_in_block)).reduce(1, &:*) *
      (block_multiplicator**((idx % keys_in_block).to_f / keys_in_block))
  end

  def key_data
    key_colors.map.with_index do |color, idx|
      Key.new(
        color: color,
        left_black: idx > 0 && key_colors[idx - 1] == :black,
        right_black: idx < keys - 1 && key_colors[idx + 1] == :black,
        frequency: first_freq * mult_for(idx),
        relative_number: (idx - start_block_at) % keys_in_block,
        block_number: (idx - start_block_at) / keys_in_block,
        id: idx,
        sample: has_samples ? "/samples/#{id}/#{idx}.wav" : nil
      )
    end
  end

  after_create_commit :create_samples!

  def create_samples!
    return if has_samples

    Widgets::KeyboardJob.perform_later(id)
  end

  def to_liquid
    attributes
  end

  private

  def check_block_schema_size
    if keys_in_block > keys
      errors.add(:keys, 'Клавиш слишком мало для данной схемы блока')
    end
  end

  def check_first_freq
    if first_freq < 20
      errors.add(:first_freq, 'Нижняя частота должна быть в слуховом диапазоне человека')
    end
  end
  def check_last_freq
    last_freq = mult_for(keys-1) * first_freq
    if last_freq > 20_000
      errors.add(:keys, "Слишком много клавиш для выбранных параметров. Последняя клавиша будет звучать на частоте #{las_freq} Гц")
    end
  end
end
