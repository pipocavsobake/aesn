# == Schema Information
#
# Table name: widgets_calendars
#
#  id             :bigint           not null, primary key
#  name           :string           not null
#  date_from      :date             not null
#  date_to        :date             not null
#  file_data      :jsonb
#  user_id        :bigint           not null
#  content        :text
#  enabled        :boolean          default(FALSE), not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  column_mapping :jsonb
#
# Indexes
#
#  index_widgets_calendars_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class Widgets::Calendar < ApplicationRecord
  RESERVED_HEADERS = %w[Дата Время дата время]
  include Shrine::Attachment(:file)
  belongs_to :user
  def days(*col_names)
    return (date_from..date_to).map { |date| {date: date} } if file.nil?

    data = data_from_file
    by_date = data.index_by { |row| Date.parse(row['Дата']) }

    (date_from..date_to).map do |date|
      day_data = by_date[date]

      next {date: date} if day_data.nil?
      values = mapping.each_with_object({}) do |(column_name, mapping), object|
        val = day_data[column_name]
        next if val.nil?

        case mapping[:type].to_sym
        when :boolean
          object[column_name] = {type: :boolean, value: mapping[val], color_hex: mapping[:color_hex][val]}
        when :integer
          object[column_name] = {type: :integer, value: val, color_hex: mapping[:color_hex].call(val)}
        end
      end
      {date: date, values: values}
    end
  end

  def data_from_file
    @data_from_file ||= CSV.read(file.download.path, headers: true)
  end

  def mapping
    @mapping ||= build_column_mapping
  end

  def build_column_mapping
    data = data_from_file
    columns_to_map = data.headers.compact - RESERVED_HEADERS
    columns_to_map.each_with_object({}) do |column_name, object|
      object[column_name] = try_build_mapping(data[column_name])
    end.compact
  end

  def try_build_mapping(values)
    distinct_values = values.sort_by(&:to_s).uniq
    return nil if distinct_values.compact.blank?

    if distinct_values.compact.size == 2 && boolean_values?(distinct_values.compact)
      return boolean_mapping(distinct_values.compact)
    end
    if distinct_values.all? { |val| val.nil? || val.is_a?(Integer) }
      return integer_mapping(*distinct_values.compact.minmax)
    end
    as_floats = distinct_values.map do |val|
      next if val.nil?
      break unless val.is_a?(String)

      Float(val) rescue break
    end
    if as_floats&.select(&:itself).present?
      as_abs = as_floats.select(&:itself).map(&:abs)
      return float_mapping(*(as_floats.compact.minmax + as_abs.minmax))
    end
    nil
  end

  BOOLEAN_SCHEMES = %w[
    да#нет
    no#yes
    n#y
    f#t
    +#-
  ]
  POS_VALUES = %w[да yes y t +]
  NEG_VALUES = %w[нет no n f -]
  def boolean_values?(values)
    BOOLEAN_SCHEMES.include?(values.map(&:to_s).map(&:downcase).sort.join('#'))
  end

  def boolean_mapping(values)
    pos = values.find { |val| POS_VALUES.include?(val.to_s.downcase) }
    neg = values.find { |val| NEG_VALUES.include?(val.to_s.downcase) }
    {type: :boolean, color_hex: {pos => 'ff0000', neg => '00ff00'} }
  end

  def integer_mapping(min, max)
    mid = (min+max).to_f / 2.0
    semiwidth = (max - min).to_f / 2.0
    {
      type: :integer,
      color_hex: proc { |val|
        val = val.to_f
        val > mid ?
          "ff#{(255.0 * (max - val).to_f / semiwidth).round.to_s(16)}00" :
          "#{(255.0 * val.to_f / semiwidth).round.to_s(16)}ff00"
        },
    }
  end

  def float_mapping(min, max, abs_min, abs_max)
    integer_mapping(min, max)
  end
end
