# == Schema Information
#
# Table name: yandex_messages
#
#  id         :bigint           not null, primary key
#  data       :jsonb
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Yandex::Message < ApplicationRecord
  def response
    return new_session if data['session']['new']
    return help if data['request']['command'].strip == 'помощь'
    repeate
  end

  def repeate
    msg = data['request']['command']
    {
      text: "Вы сказали: #{msg}",
      tts: "Вы сказали #{msg}",
      buttons: [],
      end_session: false
    }
  end

  def help
    new_session
  end

  def new_session
    {
      text: "Я браузер без глаз. Пока что я в режиме разработке, поэтому я просто повторяю за тобой, пока не скажешь «стоп»",
      tts: "Я браузер без глаз. Пока что я в режиме разработке, поэтому я просто повторяю за тобой, пока не скажешь стоп",
      buttons: [],
      end_session: false
    }
  end

end
