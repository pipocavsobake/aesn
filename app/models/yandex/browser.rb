# == Schema Information
#
# Table name: yandex_browsers
#
#  id         :bigint           not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Yandex::Browser < ApplicationRecord
  has_many :commands, dependent: :destroy
end
