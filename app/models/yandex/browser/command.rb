# == Schema Information
#
# Table name: yandex_browser_commands
#
#  id         :bigint           not null, primary key
#  browser_id :bigint           not null
#  data       :jsonb
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_yandex_browser_commands_on_browser_id  (browser_id)
#
# Foreign Keys
#
#  fk_rails_...  (browser_id => yandex_browsers.id)
#

class Yandex::Browser::Command < ApplicationRecord
  belongs_to :browser
end
