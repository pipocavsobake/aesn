# == Schema Information
#
# Table name: marusia_phrases
#
#  id         :bigint           not null, primary key
#  kind       :string           not null
#  content    :text
#  tts        :text
#  boost      :bigint           default(1), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_marusia_phrases_on_kind_boost_DESC  (kind,boost DESC)
#
class Marusia::Phrase < ApplicationRecord

  def self.get(kind)
    where(kind: kind).to_a.flat_map { |phrase| [phrase] * phrase.boost }.sample
  end
end
