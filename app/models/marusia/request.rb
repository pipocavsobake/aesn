# == Schema Information
#
# Table name: marusia_requests
#
#  id             :bigint           not null, primary key
#  request_params :jsonb
#  response       :jsonb
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
class Marusia::Request < ApplicationRecord
end
