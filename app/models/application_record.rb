class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  include ExtendActiveRecord

  def current_ability
    %i[read create update destroy].each_with_object({}) do |action, object|
      object[action] = Ability.current.can?(action, self)
    end
  end

  def siblings
    []
  end

  def should_refresh_shrine_derivatives?(attachment_name, sizes: [])
    attachment = send(attachment_name)
    return false if attachment.nil?
    return false if attachment.storage_key == :cache

    min_updated_at = send("#{attachment_name}_derivatives")&.values&.map { |der| der.metadata['updated_at'].try { |value| value.is_a?(Time) ? value : Time.parse(value) } }&.min
    return false if min_updated_at && min_updated_at > Time.parse(attachment.metadata['updated_at'])

    true
  end
end
