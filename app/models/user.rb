# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#  failed_attempts        :integer          default(0), not null
#  unlock_token           :string
#  locked_at              :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  telegram_code          :string
#  roles                  :string           is an Array
#
# Indexes
#
#  index_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_unlock_token          (unlock_token) UNIQUE
#

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :rememberable, :validatable,
         :omniauthable, omniauth_providers: (Rails.application.secrets.oauth2&.keys || [])
  extend Enumerize
  HARDCODED_ADMINS = %w[pipocavsobake@gmail.com]
  enumerize :roles, in: %i[admin page_editor saiu], multiple: true, predicates: true
  scope :with_role, -> (role) { where("? = ANY (roles)", role) }
  has_many :identities, dependent: :destroy
  has_one :strava_identity, -> { where(provider: :strava) }, class_name: 'User::Identity'
  include User::Omniauth
  include User::Telegrammable

  has_many :video_markup_moments, class_name: 'VideoMarkup::Moment', dependent: :destroy
  has_many :video_markup_comments, class_name: 'VideoMarkup::Comment', dependent: :destroy
  has_many :local_markups, class_name: 'LocalMarkup::Markup', dependent: :destroy
  has_many :share_links, dependent: :destroy

  has_many :strava_activities, through: :identities, class_name: 'Strava::Activity'
  has_many :sport_activities, class_name: 'Sport::Activity', dependent: :destroy
  has_many :sport_exercises, class_name: 'Sport::Exercise', dependent: :destroy

  has_many :widgets_calendars, class_name: 'Widgets::Calendar', dependent: :destroy

  def name
    ru_name, en_name = [nil, nil]
    identities.each do |identity|
      next if identity.name.blank?

      if identity.name != email && identity.name.match?(/[а-яА-Я]/)
        ru_name ||= identity.name
        next
      end
      en_name ||= identity.name if identity.name != email
    end
    [ru_name, en_name, email].compact.first
  end
end
