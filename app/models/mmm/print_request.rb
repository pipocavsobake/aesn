# == Schema Information
#
# Table name: mmm_print_requests
#
#  id         :bigint           not null, primary key
#  uid        :string
#  state      :string           default("pending"), not null
#  file_data  :jsonb
#  data       :jsonb
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  chat_id    :string
#
class Mmm::PrintRequest < ApplicationRecord
  include PrinterUploader::Attachment(:file)
  extend Enumerize
  enumerize :state, in: %i[pending fetched done failed]
  after_update_commit :notify_via_telegram

  def notify_via_telegram
    Telegram.bots[:mmm_printer].send_message(chat_id: chat_id, text: state_message)
  end

  def state_message
    "#{file.metadata.dig('filename')}: #{I18n.t("telegram.mmm.printer.state_message.#{state}")}"
  end

  def as_json(options=nil)
    attributes.except('file_data').merge(
      'file_url' => "https://aesn.ru#{file_url}"
    )
  end
end
