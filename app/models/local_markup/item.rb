# == Schema Information
#
# Table name: local_markup_items
#
#  id             :bigint           not null, primary key
#  file           :string
#  related_files  :string           default([]), is an Array
#  second         :integer
#  data           :jsonb
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  markup_id      :bigint
#  check_point_id :bigint
#
# Indexes
#
#  index_local_markup_items_on_check_point_id  (check_point_id)
#  index_local_markup_items_on_markup_id       (markup_id)
#
# Foreign Keys
#
#  fk_rails_...  (check_point_id => local_markup_check_points.id)
#  fk_rails_...  (markup_id => local_markup_markups.id)
#
class LocalMarkup::Item < ApplicationRecord
  attr_accessor :check_point_file
  belongs_to :markup, class_name: 'LocalMarkup::Markup'
  belongs_to :check_point, class_name: 'LocalMarkup::CheckPoint'
  accepts_nested_attributes_for :check_point
  validates :check_point, presence: true

  before_validation do
    chp = self.check_point || LocalMarkup::CheckPoint.find_or_initialize_by(
      path: check_point_file,
      markup_id: markup_id,
    )
    if chp.persisted?
      chp.update_columns(reached_at: Time.now) if chp.reached_at.nil?
      self.check_point_id = chp.id
    else
      assign_attributes(
        check_point_attributes: chp.attributes.merge(reached_at: Time.now),
      )
    end
  end
end
