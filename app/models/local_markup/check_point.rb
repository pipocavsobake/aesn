# == Schema Information
#
# Table name: local_markup_check_points
#
#  id           :bigint           not null, primary key
#  markup_id    :bigint           not null
#  path         :string
#  reached_at   :datetime
#  processed_at :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_local_markup_check_points_on_markup_id  (markup_id)
#
# Foreign Keys
#
#  fk_rails_...  (markup_id => local_markup_markups.id)
#
class LocalMarkup::CheckPoint < ApplicationRecord
  belongs_to :markup, class_name: 'LocalMarkup::Markup'
  has_many :files, class_name: 'LocalMarkup::File', foreign_key: :check_point_id, dependent: :destroy
  has_many :items, class_name: 'LocalMarkup::Item', foreign_key: :check_point_id, dependent: :destroy

  # LocalMarkup::CheckPoint.counter_culture_fix_counts
  counter_culture :markup
  def as_json(options=nil)
    attributes.merge(
      items: items.map(&:as_json)
    )
  end
end
