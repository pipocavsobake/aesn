# == Schema Information
#
# Table name: local_markup_markups
#
#  id                 :bigint           not null, primary key
#  user_id            :bigint           not null
#  name               :string
#  file_data          :jsonb
#  cues_file_data     :jsonb
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  default_form       :string
#  check_points_count :bigint           default(0), not null
#
# Indexes
#
#  index_local_markup_markups_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class LocalMarkup::Markup < ApplicationRecord
  belongs_to :user
  has_many :items, class_name: 'LocalMarkup::Item', dependent: :destroy, foreign_key: :markup_id
  has_many :check_points, class_name: 'LocalMarkup::CheckPoint', dependent: :destroy, foreign_key: :markup_id
  has_and_belongs_to_many :target_markups, class_name: self.to_s, join_table: :local_markup_merges, foreign_key: :source_markup_id, association_foreign_key: :target_markup_id
  has_and_belongs_to_many :source_markups, class_name: self.to_s, join_table: :local_markup_merges, foreign_key: :target_markup_id, association_foreign_key: :source_markup_id
  include LocalMarkupUploader::Attachment(:file)
  include Shrine::Attachment(:cues_file)
  extend Enumerize
  enumerize :default_form, in: %w[audio one_button]

  validates :name, :file, presence: true

  def as_json(options=nil)
    {
      id: id,
      name: name,
      file: file_url(:diff) || file_url,
      cues_file: cues_file&.url,
      default_form: default_form,
    }
  end

  def total
    file.metadata.fetch('check_points_count')
  end


  def finished?
    check_points_count == total
  end

  def name_with_counts
    return name if file.nil?

    "#{name} (#{check_points_count} / #{total})"
  end
  include LocalMarkup::Result
end
