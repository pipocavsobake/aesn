module User::Omniauth
  extend ActiveSupport::Concern
  class_methods do
    def from_omniauth(auth)
      identity = User::Identity.find_or_initialize_by(uid: auth.uid, provider: auth.provider)
      identity.token = auth.credentials.token
      identity.token_expires_at = Time.at(auth.credentials.expires_at)
      identity.refresh_token = auth.credentials.refresh_token
      identity.name = auth.info.name
      identity.save! if identity.persisted?

      return identity.user if identity.persisted?

      email = auth.info.email || build_omniauth_email(auth.provider, auth.uid)
      obj = where(email: email).first_or_create! do |user|
        user.password = Devise.friendly_token[0, 20]
        user.roles = ['admin'] if User::HARDCODED_ADMINS.include?(user.email)
        # user.avatar = auth.info.image if auth.provider.to_s != 'facebook'
      end
      identity.user = obj
      identity.save
      obj
    end

    def build_omniauth_email(provider, uid)
      "#{uid}@#{provider}.aesn.ru"
    end
  end

  def email_built_for_omniauth?
    Rails.application.secrets.oauth2.keys.any? do |provider|
      email.split('@').last == "#{provider}.aesn.ru"
    end
  end

  def omniauth_mergeable?
    return false if identities.size != 1

    provider = Rails.application.secrets.oauth2.keys.find do |provider|
      email.split('@').last == "#{provider}.aesn.ru"
    end

    identities.first.build_email_for_omniauth == email
  end
end
