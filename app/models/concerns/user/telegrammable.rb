module User::Telegrammable
  extend ActiveSupport::Concern
  included do
    has_many :telegram_chats, dependent: :destroy
  end

  class_methods do
    def by_tg(chat_id)
      User::Identity.find_by(provider: :telegram, uid: chat_id)&.user
    end

    def create_from_tg!(chat_id)
      iden = User::Identity.create!(provider: :telegram, uid: chat_id)
      iden.user = create!(email: "tg_#{chat_id}@aesn.ru", password: SecureRandom.base64(6))
    end
  end
end
