module ExtendActiveRecord
  extend ActiveSupport::Concern
  included do
    def self.quoted_column(name)
      cn = columns_hash[name.to_s].name
      qtn = quoted_table_name
      qcn = connection.quote_column_name cn
      "#{qtn}.#{qcn}"
    end
  end
end
