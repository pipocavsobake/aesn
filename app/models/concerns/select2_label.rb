module Select2Label
  extend ActiveSupport::Concern
  def as_json(options=nil)
    attributes.merge(
      value: id,
      label: select_name,
    )
  end
end
