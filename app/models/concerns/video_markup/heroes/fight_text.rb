module VideoMarkup::Heroes::FightText
  extend ActiveSupport::Concern

  def full_text
    "#{hero.name} на #{day || 'неизвестный'} день #{enemy_text}#{ground_text}; статы #{primaries_text}"
  end

  def full_text_with_streamer
    "#{moment.video.streamer.name} за #{full_text}#{template_text}"
  end

  def template_text
    return nil if template.nil?

    "; шаблон #{template&.name || 'неизвестный'}"
  end

  def primaries
    [attack, defence, spell_power, knowledge]
  end

  def primaries_text
    return "неизвестны" if primaries.compact.empty?

    primaries.map { |value| value || '?' }.join(' ')
  end

  def enemy_text
    return bank_text if bank
    return unit_text if unit

    "непонятно что делает"
  end

  def ground_text
    return if ground.nil?

    " на земле «#{ground.name}»"
  end

  def bank_text
    "захватывает #{bank.cases['accs_sing']} #{bank_value || 'неизвестного'} уровня#{bank_gs_text}"
  end

  def bank_gs_text
    return unless bank_graded
    " с улучшенным стеком"
  end

  def unit_text
    "бьёт #{"#{units_count}#{upgraded_units_text} #{unit.name}" || "неизвестное количество #{unit.name}"}"
  end

  def upgraded_units_text
    return unless upgraded_units_count

    " (#{upgraded_units_count} улучшенных, #{units_count - upgraded_units_count} обычных)"
  end
end
