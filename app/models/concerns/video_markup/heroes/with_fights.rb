module VideoMarkup::Heroes::WithFights
  extend ActiveSupport::Concern
  included do
    has_many :fights, dependent: :destroy
    scope :with_fights, -> {
      where(
        # 'enabled_fight_count > 0'
        self.model.arel_table[:enabled_fights_count].gt(0)
      )
    }
  end
end
