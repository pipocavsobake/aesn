module LocalMarkup::Result
  extend ActiveSupport::Concern
  included do
    before_validation do
      next if file || !@json_data

      tmp_file = "/tmp/aesn_local_markup_result_#{SecureRandom.hex}.json"
      File.open(tmp_file, "w") { |f| f.write(@json_data.to_json) }
      self.file = File.open(tmp_file)
    end
    validate do
      next if source_markups.empty?
      source_that_is_already_merge = source_markups.find(&:merge?)

      if source_that_is_already_merge
        self.errors.add(:source_markups, 'Не могут содержать производные сессии разметки')
      end
    end
    scope :derivative, -> {
      where("id IN (#{klass.derivative_ids_sql})")
    }
    def self.derivative_ids_sql
      association = reflect_on_association(:source_markups)
      q_jt = connection.quote_table_name(association.join_table)
      q_fk = connection.quote_column_name(association.foreign_key)
      "SELECT DISTINCT #{q_fk} FROM #{q_jt}"
    end
  end
  def file_path
    file.storage.path(file.id)
  end
  def entries
    @entries ||= File.open(file_path).read
  end

  def json_data
    @json_data ||= JSON.parse(entries)
  end

  def merge(with:)
    LocalMarkup::Result.merge(json_data, with.json_data)
  end

  def merge?
    source_markups.any?
  end

  def &(with)
    ret = self.class.new
    ret.instance_variable_set("@json_data", LocalMarkup::Result.merge(json_data, with.json_data))
    ret
  end

  def self.merge(base, diff)
    (base.keys + diff.keys).each_with_object({}) do |key, obj|
      key = "#{key}/" if key.last != '/'
      obj[key] = (base[key] || []) + (diff[key] || [])
    end
  end

  def result
    markup_ids = [id, *source_markup_ids]
    chs = LocalMarkup::CheckPoint.where(markup_id: markup_ids)
    its = LocalMarkup::Item.where(markup_id: markup_ids)
    ch = Hash[chs.pluck(:id, :path)]
    its.pluck(:check_point_id, :file).each_with_object({}) do |(ch_id, file), obj|
      obj[ch[ch_id]] ||= []
      obj[ch[ch_id]].push(file)
    end
  end

  def result_csv
    markup_ids = [id, *source_markup_ids]
    chs = LocalMarkup::CheckPoint.where(markup_id: markup_ids)
    item_files = Set.new(LocalMarkup::Item.where(markup_id: markup_ids).pluck(:file))
    data = json_data
    data = data['files'] if data.key?('files')
    rows = []
    data.each do |key, values|
      values.each do |url|
        rows << [url, item_files.include?(url)]
      end
    end
    rows
  end

  def plain_data
    data = json_data
    data = data['files'] if data.key?('files')
    data.values.reduce([]) do |s, a|
      s + a
    end.sort.uniq
  end

  def to_aria2c
    plain_data.reduce('') do |str, url|
      path = url
      uri = URI.parse(url) rescue nil
      if uri && uri.host
        path = uri.path
      end
      "#{str}#{url}\n  out=#{path}\n"
    end
  end

end
