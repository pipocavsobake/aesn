# == Schema Information
#
# Table name: share_links
#
#  id         :bigint           not null, primary key
#  user_id    :bigint           not null
#  url        :string
#  session    :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_share_links_on_session  (session)
#  index_share_links_on_slug     (slug)
#  index_share_links_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class ShareLink < ApplicationRecord
  belongs_to :user
  validates_presence_of :url, :slug
  validates :url, url: true
  before_validation :set_slug
  def to_param(args = {})
    slug
  end

  private
  def set_slug
    self.slug ||= SecureRandom.urlsafe_base64(32, false)
  end
end
