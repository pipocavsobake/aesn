class H3hota::FizmigSkill < ApplicationSkill
  MODELS_IN_SKILL = {
    VideoMarkup::Heroes::Unit => 'h3_hota_units',
    VideoMarkup::Heroes::Bank => 'h3_hota_banks',
    VideoMarkup::Heroes::Hero => 'h3_hota_heroes',
    VideoMarkup::Heroes::Artifact => 'h3_hota_artifacts',
    VideoMarkup::Heroes::Spell => 'h3_hota_spells',
  }
  def initialize(request, session, meta)
    @request = request
    @session = session
    @meta = meta
    @version = request['version']
  end

  def respond
    return greetings if @session['new']

    response = try_find_response
    return response if response

    default_response
  end

  def self.with_joins(tokens)
    (1..tokens.size).each_with_object([]) do |size, array|
      (0..(tokens.size - size)).each do |start|
        array.push tokens.slice(start, size).join(' ')
      end
    end
  end

  def self.find_by_tokens(tokens)
    stop = false
    with_joins(tokens).map do |token|
      next nil if stop
      s_response = Searchkick.search(token,
        index_name: MODELS_IN_SKILL.keys,
        fields: ['name^3', 'aliases'],
        limit: 1,
      )
      if s_response.first
        score = s_response.response.dig('hits', 'max_score')
        [token, s_response.first, score]
      end
    end.compact.sort_by(&:third).reverse
  end

  def self.find_attributes_by_tokens(object, tokens)
    tokens.map do |token|
      s_response = Marusia::Alias.search(
        token,
        where: {
          kind: MODELS_IN_SKILL[object.class]
        },
        limit: 1
      )
      [token, s_response.first, s_response.response.dig('hits', 'max_score')] if s_response.first
    end.compact.sort_by(&:third).reverse
  end

  def respond_with_alias(object, m_alias)
    values = [object, *object.siblings].map do |resource|
      value = resource.send(m_alias.target)
      "у #{resource.cases['gent_sing']} #{value}"
    end.join(', ')
    text = "#{m_alias.name} #{values}"
    {
      text: text,
      tts: text,
      end_session: false,
    }
  rescue => e
    Rails.logger.error(e)
    nil
  end

  private

  def try_find_response
    common_response = try_find_common_response
    return common_response if common_response

    tokens = @request.to_h.dig('nlu', 'tokens').dup
    tokens.reject! do |token|
      token.size < 3 || %w[какая какое какой сколько как много].include?(token)
    end
    self.class.find_by_tokens(tokens).first.try do |token, object, score|
      tokens -= token.split(' ')

      response = self.class.find_attributes_by_tokens(object, tokens).first.try do |token, m_alias, score|
        respond_with_alias(object, m_alias)
      end
    end
  end

  def greetings
    use_phrase(
      Marusia::Phrase.get(:h3_hota_greetings),
      buttons: [{title: 'атака чародея'}, {title: 'стоимость никса'}, {title: 'Скорость циклопа'}],
      end_session: false,
    )
  end
end
