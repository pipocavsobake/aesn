class ApplicationSkill
  def default_response
    {
      text: "Вашу фразу я восприняла как: «#{@request.to_h.dig('command')}» и не знаю, что ответить. Попробуйте спросить иначе",
      end_session: false,
    }
  end
  def use_phrase(phrase, end_session: true, buttons: nil)
    {
      text: phrase.content,
      tts: phrase.tts,
      end_session: end_session,
      buttons: buttons,
    }.compact
  end

  def try_use_phrase_kind(kind, end_session: true, buttons: nil)
    Marusia::Phrase.get(kind).try { |phrase| use_phrase(phrase, end_session: end_session, buttons: buttons) }
  end

  def thanks
    try_use_phrase_kind(:thanks)
  end

  def yes_other
    try_use_phrase_kind(:yes_other, end_session: false)
  end

  def try_find_common_response
    return thanks if (@request.to_h.dig('nlu', 'tokens').try { |tokens| (tokens & ['спасибо', 'благодарю']).any? } )

    return yes_other if @request.to_h.dig('command') == 'иначе'
  end
end
