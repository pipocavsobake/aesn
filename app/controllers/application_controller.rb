class ApplicationController < ActionController::Base
  include DeviseStoreLocation
  rescue_from CanCan::AccessDenied, with: :access_denied
  before_action :set_root_breadcrumb
  before_action :set_page
  before_action :set_template
  before_action :save_current_ability
  before_action :restrict_subdomain
  def page_title
    "AESN"
  end
  helper_method :page_title

  def set_root_breadcrumb
    breadcrumb 'Главная', root_path
  end

  def set_template
    @template = find_seo_model(Page::Template)
  end

  def save_current_ability
    RequestStore.store[:current_ability] = current_ability
  end

  def render_template!
    @page = @template.render(@resource) if @template && @resource && !@page
  end

  def set_page
    # Такой порядок нужен для пасхалок, На некоторых страницах будет уникальный текст
    @page = find_seo_model(Page) || @page
  end

  def find_seo_model(model)
    model.pluck(:fullpath, :subdomain).find do |fullpath, subdomain|
      page_params = AppUrl.recognize_path(fullpath, subdomain: subdomain)
      page_params[:controller] == params[:controller] && page_params[:action] == params[:action]
    end.try do |fullpath, subdomain|
      model.find_by(fullpath: fullpath, subdomain: subdomain)
    end
  rescue => e
    #raise e if Rails.env.development?
    logger.error(e)
    nil
  end

  def access_denied(exception)
    raise exception if current_user
    session[:denied_from_path] = request.path

    redirect_to new_user_session_path
  end

  def after_sign_in_path_for(resource)
    session[:denied_from_path].presence || super
  end

  def restrict_subdomain
    return if request.subdomain.blank? || request.subdomain == 'www'

    raise ActiveRecord::RecordNotFound
  end

  helper_method :strava_connected?
  def strava_connected?
    current_user&.strava_identity
  end


end
