class Users::RunsController < UsersController
  before_action :set_user
  def index
    @date_from = params[:date_from].try { |d| Time.parse(d).beginning_of_day } || 2.weeks.ago.beginning_of_day
    @date_to = params[:date_to].try { |d| Time.parse(d).end_of_day } || Date.today.end_of_day
    @q = @user.strava_identity.strava_activities.where(strava_type: 'Run').ransack(ransack_params)
    @activities = @q.result.to_a.sort_by(&:start_date)
    @total_distance = @activities.map(&:distance).sum
    @total_time = @activities.map(&:moving_time).sum
  end

  private
  def set_user
    @user = User.find(params[:user_id])
    raise ActiveRecord::RecordNotFound if @user.strava_identity.nil?
    breadcrumb @user.name, user_path(@user)
  end

  def ransack_params
    return default_ransack_params if params[:q].nil?

    params.require(:q).permit(:start_date_gteq, :start_date_lt)
  end

  def default_ransack_params
    {
      start_date_gteq: 2.weeks.ago.beginning_of_day,
      start_date_lt: Date.today.end_of_day,
    }
  end
end
