class ShareLinksController < ApplicationController
  breadcrumb 'Одноразовые ссылки', :share_links
  def new
    authorize! :create, ShareLink
    breadcrumb 'Новая ссылка', new_share_link_path
    @share_link = current_user.share_links.new
  end

  def create
    authorize! :create, ShareLink
    @share_link = current_user.share_links.new(params.require(:share_link).permit(:url))
    if @share_link.save
      redirect_to @share_link, flash: {success: 'Ссылка создана успешно. Отправьте её другу'}
      return
    else
      breadcrumb 'Новая ссылка', new_share_link_path
      render 'new', flash: {error: 'Ошибки валидации'}
    end
  end

  def index
    return unless current_user
    @share_links = current_user.share_links.page(params[:page])
  end

  def show
    @share_link = ShareLink.find_by(slug: params[:id])
    raise ActiveRecord::RecordNotFound if @share_link.nil?
    raise CanCan::AccessDenied if params[:h] && @share_link.user_id != current_user&.id && session[:share_link] != @share_link.session && @share_link.session.present?
    if @share_link.session.blank? && @share_link.user_id != current_user&.id && params[:h]
      session_share_link = SecureRandom.urlsafe_base64(32, false)
      session[:share_link] = session_share_link
      @share_link.update_columns(session: session_share_link)
    end
    breadcrumb "Ссылка от #{@share_link.user.name} #{l(@share_link.created_at, format: :short)}", share_link_path(@share_link)
  end
end
