class Widgets::KeyboardsController < WidgetsController
  before_action :set_keyboard_breadcrumbs
  def new
    breadcrumb 'Новая клавиатура', new_widgets_keyboard_path
    @keyboard = Widgets::Keyboard.new
  end

  def create
    breadcrumb 'Новая клавиатура', new_widgets_keyboard_path
    @keyboard = Widgets::Keyboard.new(keyboard_params)
    if @keyboard.save
      redirect_to @keyboard, flash: { success: 'Клавиатура создана' }
    else
      render 'new'
    end
  end

  def index
    scope = Widgets::Keyboard.order(id: :desc)
    @total_count = scope.count
    @enabled_count = scope.enabled.count
    scope = scope.enabled if params[:enabled]
    @keyboards = scope.page(params[:page])
  end

  def show
    @resource = @keyboard = Widgets::Keyboard.friendly.find(params[:id])
    breadcrumb @keyboard.name, widgets_keyboard_path(@keyboard)
    render_template!
  end

  helper_method :css_for

  def css_for(datum)
    (["color-#{datum[:color]}"] +
     datum.slice(:left_black, :right_black).compact.keys).map do |css_class|
      "widgets__keyboard-key-#{css_class}"
    end
  end

  helper_method :style_for

  def style_for(datum, total_left)
    left = total_left
    left -= 15 if datum[:color] == :black
    "left: #{left}px"
  end

  private

  def set_keyboard_breadcrumbs
    breadcrumb 'Клавиатуры', widgets_keyboards_path
  end

  def keyboard_params
    params.require(:widgets_keyboard).permit(
      :name, :keys, :first_freq, :block_multiplicator, :start_block_at,
      :block_schema_string
    )
  end
end
