class Widgets::CalendarsController < WidgetsController
  before_action :authenticate_user!, except: :show
  breadcrumb "Календарь", :widgets_calendars
  before_action :set_calendar, only: %i[edit update]
  def index
    @calendars = current_user.widgets_calendars.page(params[:page]).per(12)
  end

  def new
    @calendar = init_new_calendar
    breadcrumb "Новый", request.path
  end

  def create
    @calendar = current_user.widgets_calendars.new(calendar_params)
    @calendar.save!
    redirect_to @calendar, flash: {success: 'Успех'}
  rescue => e
    flash[:error] = "#{e.class.to_s}\n#{e.message}\n#{e.backtrace.first(4).join("\n")}"
    @calendar ||= init_new_calendar
    breadcrumb 'Новый', new_widgets_calendar_path
    render 'new'
  end

  def show
    @calendar = Widgets::Calendar.find_by!('user_id = ? OR enabled', current_user&.id)
    @days = @calendar.days
    @columns = @calendar.mapping.keys
    @selected_columns = params[:columns] || [@columns.first].compact
    breadcrumb @calendar.name, widgets_calendar_path(@calendar)
  end

  def edit
    breadcrumb 'Редактирование', edit_widgets_calendar_path(@calendar)
  end

  def update
    @calendar.update!(calendar_params)
    redirect_to @calendar, flash: {success: 'Успех'}
  rescue => e
    flash[:error] = "#{e.class.to_s}\n#{e.message}\n#{e.backtrace.first(4).join("\n")}"
    breadcrumb 'Редактирование', edit_widgets_calendar_path(@calendar)
    render 'edit'
  end

  private
  def calendar_params
    params.require(:widgets_calendar).permit(:name, :date_from, :date_to, :file, :content, :enabled)
  end

  def set_calendar
    @calendar = current_user.widgets_calendars.find(params[:id] || params[:calendar_id])
    breadcrumb @calendar.name, widgets_calendar_path(@calendar)
  end

  def init_new_calendar
    date_from = params[:date_from].try { |t| Time.parse(t).to_date } || Time.now.beginning_of_year.to_date
    Widgets::Calendar.new(
      date_from: date_from,
      date_to:   params[:date_to].try   { |t| Time.parse(t).to_date } || (date_from.next_year - 1.day),
    )
  end
end
