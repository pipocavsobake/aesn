class WebhooksController < ActionController::Base
  skip_before_action :verify_authenticity_token
  def twitch
    Twitch::WebhookEvent.create!(data: params.permit!)
  rescue => e
    puts e.class.to_s
    puts e.message
    puts e.backtrace
  ensure
    head 204
  end

  def twitch_check
    Twitch::WebhookChallenge.create(data: params.permit!)
    render plain: params['hub.challenge']
  end
end
