class User::MergesController < ApplicationController
  before_action :ensure_mergeable
  before_action :set_user_merge, only: :update

  def new
    @user_merge = current_user.identities.first.merges.first_or_initialize
  end

  def create
    @user_merge = current_user.identities.first.merges.new(user_merge_params)
    if @user_merge.save
      redirect_to root_path, flash: {success: "Проверьте почту. Письмо со ссылкой на подтверждение действия, уже там"}
    else
      flash[:error] = @user_merge.errors.full_messages.join("\n")
      render 'new'
    end
  end

  def update
    if @user_merge.update(user_merge_params)
      redirect_to root_path, flash: {success: "Проверьте почту. Письмо со ссылкой на подтверждение действия, уже там"}
    else
      flash[:error] = @user_merge.errors.full_messages.join("\n")
      render 'new'
    end
  end

  def confirm
    @user_merge = User::Merge.find_by!(token: params[:token], token_expires_at: (Time.now..))
    @user_merge.confirm_in_transaction!
    sign_in(@user_merge.target_user!)
    redirect_to root_path, flash: {success: 'Аккаунт успешно привязан'}
  end

  private

  def set_user_merge
    @user_merge = current_user.identities.first.merges.find(params[:id] || params[:user_merges_id])
  end

  def ensure_mergeable
    return if current_user.omniauth_mergeable?

    raise ActiveRecord::RecordNotFound
  end

  def user_merge_params
    params.require(:user_merge).permit(:email)
  end
end
