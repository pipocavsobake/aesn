class VideoMarkup::MomentsController < VideoMarkupController
  def index
    q = params[:q]
    scope = VideoMarkup::Moment.preload(:comments, :user, {video: :streamer, heroes_fights: %i[hero unit bank template spells ground], heroes_learn_skills: [:skill]})
    @moments = scope.ransack(q).result.page(params[:page])
  end

  helper_method :path_to_other_page
  def path_to_other_page
    video_markup_heroes_bank_path(VideoMarkup::Heroes::Bank.order(enabled_fights_count: :desc).first.slug)
  end
end
