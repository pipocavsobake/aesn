class VideoMarkup::Heroes::UnitsController < VideoMarkupController
  breadcrumb 'Юниты', :video_markup_heroes_units
  def index
    @units_by_castle = VideoMarkup::Heroes::Unit.preload(:castle).group_by(&:castle)
  end

  def show
    @resource = @unit = VideoMarkup::Heroes::Unit.find(params[:slug].delete_prefix('unit-'))
    @fights = @unit.fights.enabled.preload(:hero, :unit, :template, moment: [:comments, :user, {video: :streamer, heroes_fights: %i[hero unit template spells], heroes_learn_skills: [:skill]}]).page(params[:page])
    breadcrumb @unit.full_name, video_markup_heroes_unit_path(@unit.slug)
    render_template!
  end

  helper_method :path_to_other_unit
  def path_to_other_unit
    return nil if @resource.nil?

    next_resource = @resource.class.with_fights.where('id > ?', @resource.id).first
    next_resource ||= @resource.class.with_fights.reorder(:id).first
    video_markup_heroes_unit_path(next_resource.slug)
  end
end
