class VideoMarkup::Heroes::HeroesController < VideoMarkupController
  breadcrumb 'Герои', :video_markup_heroes_heroes
  def index
    @heroes_by_castle = VideoMarkup::Heroes::Hero.preload(:castle).group_by(&:castle)
  end

  def show
    @resource = @hero = VideoMarkup::Heroes::Hero.find(params[:slug].delete_prefix('hero-'))
    @fights = @hero.fights.enabled.preload(:hero, :bank, :unit, :template, moment: [:comments, :user, {video: :streamer, heroes_fights: %i[hero unit bank template spells], heroes_learn_skills: [:skill]}]).page(params[:page])
    breadcrumb @hero.full_name, video_markup_heroes_hero_path(@hero.slug)
    render_template!
  end

  helper_method :path_to_other_hero
  def path_to_other_hero
    return nil if @resource.nil?

    next_resource = @resource.class.with_fights.where('id > ?', @resource.id).first
    next_resource ||= @resource.class.with_fights.reorder(:id).first
    video_markup_heroes_hero_path(next_resource.slug)
  end
end
