class VideoMarkup::Heroes::FightsController < VideoMarkupController
  breadcrumb 'Боёвки', :video_markup_heroes_fights
  def index
    q = params[:q]
    scope = VideoMarkup::Heroes::Fight.enabled.preload(:hero, :bank, :unit, :ground, :template, moment: [:comments, :user, {video: :streamer, heroes_fights: %i[hero unit template spells bank ground], heroes_learn_skills: [:skill]}])
    if ['tempest'].include?(params[:scope])
      scope = scope.send(params[:scope])
    end
    @fights = scope.ransack(q).result.page(params[:page])
  end

  helper_method :path_to_other_page
  def path_to_other_page
    video_markup_heroes_bank_path(VideoMarkup::Heroes::Bank.order(enabled_fights_count: :desc).first.slug)
  end
end
