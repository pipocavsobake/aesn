class VideoMarkup::Heroes::BanksController < VideoMarkupController
  breadcrumb 'Банки существ', :video_markup_heroes_banks
  def index
    @banks = VideoMarkup::Heroes::Bank.with_fights.order(enabled_fights_count: :desc)
  end

  def show
    @resource = @bank = VideoMarkup::Heroes::Bank.find(params[:slug].delete_prefix('bank-'))
    session_q_key = "video_makrup_heroes_bank_#{@bank.id}_q"
    session[session_q_key] = params[:q] if params[:q]

    q = params[:q] || session[session_q_key]
    @q = @bank.fights.enabled.preload(:hero, :bank, :template, moment: [:comments, :user, {video: :streamer, heroes_fights: %i[hero bank template spells], heroes_learn_skills: [:skill]}]).ransack(q)

    @fights = @q.result.page(params[:page])
    breadcrumb @bank.full_name, video_markup_heroes_bank_path(@bank.slug)
    render_template!
  end

  helper_method :path_to_other_bank
  def path_to_other_bank
    return nil if @resource.nil?

    next_resource = @resource.class.with_fights.where('id > ?', @resource.id).first
    next_resource ||= @resource.class.with_fights.reorder(:id).first
    video_markup_heroes_bank_path(next_resource.slug)
  end
end
