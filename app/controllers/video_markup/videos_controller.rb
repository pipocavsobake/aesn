class VideoMarkup::VideosController < VideoMarkupController
  breadcrumb 'Видео', :video_markup_videos
  def show
    #@video = VideoMarkup::Video.accessible_by(current_ability).find(params[:id])
    @video = VideoMarkup::Video.find(params[:id])
    authorize! :read, @video
    @title = "Видео #{@video.streamer.kind} #{@video.streamer.name} #{@video.title} #{l(@video.published_at, format: :long)}"
    breadcrumb @title, url_for(params.permit!)
  end

  def index
    @videos = VideoMarkup::Video.preload(:streamer).page(params[:page])
  end

  def create
    @video = VideoMarkup::Video.create_from_url(params[:url])
    if @video.nil?
      render json: {error: 'Не удалось распознать ссылку'}, status: 422
      return
    end
    render json: {url: video_markup_video_path(@video)}
  rescue VideoMarkup::Video::KnownError => e
    render json: {error: e.message}, status: 422
  rescue ActiveRecord::RecordInvalid => e
    render json: {error: e.message}, status: 422
  rescue => e
    puts e.class.name
    puts e.message
    puts e.backtrace
    render json: {error: e.message}, status: 500
  end

  def new
    breadcrumb 'Новое', new_video_markup_video_path
    @video = VideoMarkup::Video.new
  end

  private

  def page_title
    @title
  end

end
