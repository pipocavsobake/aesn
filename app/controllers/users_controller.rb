class UsersController < ApplicationController
  breadcrumb 'Пользователи', :users
  before_action :set_user, only: %i[show]
  def index
    @users = User.reorder(id: :desc).page(params[:page]).per(24)
  end

  def show
    breadcrumb @user.name, user_path(@user)
    @resource = @user
    render_template!
  end

  private
  def set_user
    @user = User.find(params[:id])
  end
end
