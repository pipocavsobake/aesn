class Mmm::PrintRequestsController < ApplicationController
  skip_before_action :verify_authenticity_token
  def index
    @requests = Mmm::PrintRequest.ransack(params[:q]).result
    render json: @requests
  end

  def update
    checksum = Digest::MD5.hexdigest("#{request.raw_post}#{Rails.application.secrets.mmm_printer_secret}")
    if request.headers['X-Checksum'] != checksum
      logger.debug("Checksum missmatch for body #{request.raw_post.inspect} #{request.headers['X-Checksum'].inspect} #{checksum.inspect}")
      head 401
      return
    end
    @request = Mmm::PrintRequest.find(params[:id])
    @request.update!(print_request_update_params)
    head :ok
  rescue ActiveRecord::RecordNotFound
    head 404
  rescue => e
    render json: {class: e.class.to_s, message: e.message, backtrace: e.backtrace}, status: 422
  end

  private
  def ransack_params
    params[:q]&.permit(:state_eq)
  end

  def print_request_update_params
    params.require(:mmm_print_request).permit(:state, data: %i[error printed_at])
  end
end
