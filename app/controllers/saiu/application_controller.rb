class Saiu::ApplicationController < ApplicationController
  skip_before_action :restrict_subdomain
  layout 'saiu'
  before_action :set_og_site_name

  def set_og_site_name
    @og_site_name = "Saiu"
  end
end
