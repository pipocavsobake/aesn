class Saiu::ClaimsController < Saiu::ApplicationController
  def new
    @claim = Saiu::Claim.new
  end

  def create
    @claim = Saiu::Claim.new(claim_params)
    if @claim.save
      redirect_to thanks_saiu_claims_path, flash: {success: 'Заявка успешно отправлена'}
    else
      render 'new'
    end
  end

  def thanks
  end

  private
  def claim_params
    params.require(:saiu_claim).permit(:phone, :best_time, :comment)
  end
end
