class Telegram::BaseController < Telegram::Bot::UpdatesController
  class UnauthorizedError < StandardError; end
  def respond_with(type, **args, &block)
    key = [type.to_sym, type.to_s].find { |arg| args[arg] }
    if !block_given? || !%i[audio video document photo].include?(type.to_sym) || !key
      return super(type, **args)
    end

    source = yield
    file = Telegram::File.find_by(source: source)
    if file
      args[key] = file.telegram_id
      return super(type, **args)
    end
    reply = super(type, **args)
    Telegram::File.save_reply!(type, source, reply)
    reply
  end

  def current_user
    User.by_tg((from || chat)['id'])
  end

  def authenticate_user!
    raise UnauthorizedError if current_user.nil?
  end

  rescue_from UnauthorizedError, with: :send_message_to_unauthorized

  def send_message_to_unauthorized
    #respond_with :message, text: 'Вы не авторизованы'
    logger.info("Не авторизованный тип")
  end
end
