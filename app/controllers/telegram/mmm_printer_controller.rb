class Telegram::MmmPrinterController < Telegram::BaseController
  ADMINS = %w[235750579 214642709].freeze
  def start!(data = nil, *)
    text = "Отправь сообщение с файлом и мы попробуем его распечатать"
    respond_with :message, text: text
  end

  def message(message)
    Telegram::Message.create!(controller: self.class.to_s, payload: payload)
    payload.dig('document', 'file_id').try do |file_id|
      unless ADMINS.include?(chat&.dig(:id)&.to_s)
        respond_with :message, text: 'Спасибо! Но тут пока могут печать только местные. Отправили запрос админам'
        ADMINS.each do |admin|
          bot.send_document(document: file_id, chat_id: admin.to_i, caption: "Пользователь #{payload.dig('from', 'username')} хочет распечатать файл. Просто перешлите его, если хотите, чтобы файл был распечатан")
        end
        return
      end
      file_path = bot.get_file(file_id: file_id).dig('result', 'file_path')
      resp = HTTParty.get("https://api.telegram.org/file/bot#{bot.token}/#{file_path}")
      raise "api telegram code: #{resp.code} body: #{resp.body}" if resp.code > 299
      tmp_fn = "/tmp/mmm_printer_#{Time.now.to_i}#{File.extname(file_path)}"
      File.open(tmp_fn, 'wb') { |f| f.write(resp.body) }
      Mmm::PrintRequest.create!(uid: from&.dig(:id), chat_id: chat&.dig(:id), file: File.open(tmp_fn) )
      respond_with :message, text: 'Файл получен и добавлен в очередь на печать'
      return
    end
    respond_with :message, text: 'Сохранил сообщение; буду думать, что с ним делать теперь. Файла-то нет в прикрепе'
  rescue => e
    logger.error("#{e.class.to_s}\n#{e.message}\n#{e.backtrace}")
    respond_with :message, text: 'Что-то не получилось у меня сообщение сохранить'
  end
end
