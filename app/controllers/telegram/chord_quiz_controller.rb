class Telegram::ChordQuizController < Telegram::BaseController
  # use callbacks like in any other controllers
  around_action :with_locale

  use_session!

  # Every update can have one of: message, inline_query, chosen_inline_result,
  # callback_query, etc.
  # Define method with same name to respond to this updates.
  def message(message)
    # message can be also accessed via instance method
    message == self.payload # true
    # store_message(message['text'])
  end

  # This basic methods receives commonly used params:
  #
  #   message(payload)
  #   inline_query(query, offset)
  #   chosen_inline_result(result_id, query)
  #   callback_query(data)

  # Define public methods ending with `!` to handle commands.
  # Command arguments will be parsed and passed to the method.
  # Be sure to use splat args and default values to not get errors when
  # someone passed more or less arguments in the message.
  def start!(data = nil, *)
    # do_smth_with(data)

    # There are `chat` & `from` shortcut methods.
    # For callback queries `chat` if taken from `message` when it's available.
    text = from ? "Hello #{from['username']}! Chord" : 'Hi there! Chord'
    # There is `respond_with` helper to set `chat_id` from received message:
    respond_with :message, text: text
    path = Rails.root.join('public', 'samples', '2', '0.mp3').to_s
    audio = File.open(path)
    respond_with(:audio, audio: audio) { path }
  end

  private

  def with_locale(&block)
    I18n.with_locale(locale_for_update, &block)
  end

  def locale_for_update
    if from
      # locale for user
    elsif chat
      # locale for chat
    end
  end
end
