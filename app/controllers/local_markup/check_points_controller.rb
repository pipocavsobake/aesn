class LocalMarkup::CheckPointsController < ApplicationController
  before_action :authenticate_user!

  def show
    @check_point = LocalMarkup::CheckPoint.find_by(check_point_params)
    authorize! :read, @check_point
    render json: @check_point.as_json
  rescue => e
    render json: {
      error: {
        class: e.class.to_s,
        message: e.message,
        line: e.backtrace.first,
      }
    }
  end

  def index
    @markup = LocalMarkup::Markup.find(params[:local_markup_id])
    authorize! :read, @markup
    paths = @markup.check_points.where.not(processed_at: nil).pluck(:path)
    render json: Hash[paths.zip([true] * paths.size)]
  end

  def update
    @check_point = LocalMarkup::CheckPoint.find_or_initialize_by(
      check_point_params
    )
    if @check_point.persisted?
      authorize! :edit, @check_point
    else
      authorize! :create, @check_point
    end
    if @check_point.update(processed_at: Time.now)
      render json: {id: @check_point.id}
    else
      render json: {errors: @check_point.errors.messages}, status: 422
    end
  end

  private
  def check_point_params
    params.permit(:path).merge(markup_id: params[:local_markup_id])
  end

end
