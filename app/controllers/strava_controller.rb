class StravaController < ApplicationController
  breadcrumb 'Strava', :strava_index
  def index
    initialize_sync_form
  end

  def initialize_sync_form
    return unless strava_connected?

    identity = current_user.strava_identity
    max_activity_created_at = identity.strava_activities.maximum(:created_at)
    @sync = identity.strava_syncs.new(after: max_activity_created_at.beginning_of_day)
  end
end
