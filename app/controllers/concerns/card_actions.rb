module CardActions
  extend ActiveSupport::Concern
  # Не работает. Рендерит 2 раза

  included do
    helper_method :add_card_action
    helper_method :render_card_actions
  end

  def add_card_action(key, link_target, title: , label:, method: nil, style: nil, target: nil, &block)
    @_card_actions ||= {}
    @_card_actions[key] ||= []
    @_card_actions[key] << [link_target, OpenStruct.new({
      title: title,
      label: label,
      method: method,
      style: style,
      target: target,
      content: helpers.capture(&block),
    })]
  end

  def render_card_actions(key, dropdown_id: nil)
    dropdown_id ||= SecureRandom.hex(10)
    render_to_string(partial: 'shared/card_actions', locals: {actions: @_card_actions[key], dropdown_id: dropdown_id})
  end
end
