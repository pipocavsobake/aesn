module Telegram::Notes
  extend ActiveSupport::Concern
  def note!
    return if authenticate_user!
    current_user.update!(telegram_state: :note)
    respond_with :message, text: 'Введите название или выберите из списка'
  end
end
