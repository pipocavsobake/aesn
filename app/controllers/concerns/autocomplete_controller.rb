module AutocompleteController
  extend ActiveSupport::Concern

  def ac_options(key: :q, match: :word_start)
    [
      params[key].presence || "*",
      {
        match: match,
        misspellings: { below: 5 },
        limit: ac_limit
      }
    ]
  end

  def ac_limit
    return 10 if (limit = params[:limit].to_i) == 0
    return 10 if limit > 50
    limit
  end
end
