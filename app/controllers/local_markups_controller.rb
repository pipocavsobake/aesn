class LocalMarkupsController < ApplicationController
  breadcrumb 'Разметка локальных файлов', :local_markups
  before_action :check_access
  before_action :load_markup, only: %i[show result]

  def index
    @local_markups = LocalMarkup::Markup.accessible_by(current_ability).order(id: :desc).page(params[:page])
  end

  def new
    breadcrumb "Новая сессия разметки", url_for
    @local_markup = current_user.local_markups.new
  end

  def create
    @local_markup = current_user.local_markups.new(local_markup_params)
    if @local_markup.save
      redirect_to local_markup_path(@local_markup)
    else
      breadcrumb "Новая сессия разметки", url_for
      render 'new', flash: {error: @local_markup.errors.full_messages.join("\n")}
    end
  end

  def show
    breadcrumb @local_markup.name, local_markup_path(@local_markup)
  end

  def result
    if params[:format] != 'json'
      redirect_to url_for(params.permit!.merge(format: :json))
      return
    end
    send_data(
      @local_markup.result.to_json,
      type: 'application/json',
      disposition: "attachment; filename=local_markup_result_#{@local_markup.id}.json",
    )
  end

  private
  def local_markup_params
    params.require(:local_markup_markup).permit(:name, :file, :cues_file, :default_form)
  end

  def check_access
    authorize! :read, LocalMarkup::Markup
  end

  def load_markup
    @local_markup = LocalMarkup::Markup.accessible_by(current_ability).find(params[:id].presence || params[:local_markup_id])
  end

end
