class WidgetsController < ApplicationController
  before_action :set_widgets_breadcrumb
  def index; end

  private

  def set_widgets_breadcrumb
    breadcrumb 'Виджеты', widgets_path
  end
end
