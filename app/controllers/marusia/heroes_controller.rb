class Marusia::HeroesController < ActionController::Base
  skip_before_action :verify_authenticity_token
  def create
    m_session = params[:session].permit!
    version = params[:version]
    full_request = params.permit!
    resource = Marusia::Request.create!(request_params: full_request, response: {
      session: m_session,
      version: version,
      response: H3hota::FizmigSkill.new(full_request[:request], full_request[:session], full_request[:meta]).respond,
    })
    render json: resource.response
  end
end
