class HomeController < ApplicationController
  def index
    if current_user
      @video = VideoMarkup::Video.order(:id).last
    end
  end
end
