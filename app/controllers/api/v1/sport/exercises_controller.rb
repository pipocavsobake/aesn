class Api::V1::Sport::ExercisesController < Api::V1Controller
  include AutocompleteController
  def autocomplete
    term, options = ac_options
    records = Sport::Exercise.search(term, **options.merge(load: false))
    render json: records.map { |rec| rec.slice("id", "name", "aliases", "content", "user_id") }
  end
end
