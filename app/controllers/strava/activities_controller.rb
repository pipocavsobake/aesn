class Strava::ActivitiesController < StravaController
  breadcrumb 'Активности', :strava_activities
  def index
    authenticate_user!
    scope = current_user.strava_activities
    @activities = scope.where(index_where_params).order(strava_id: :desc).page(params[:page]).per(12)
    @strava_types = scope.group(:strava_type).count.to_a.sort_by(&:last).reverse
  end

  def show
    @use_vanilla_bootstrap = true
    @resource = @activity = Strava::Activity.find(params[:id])
    breadcrumb @resource.full_name, strava_activity_path(@resource)
    render_template!
  end

  def sync_details
    sync do |service|
      service.fetch_activity(@activity)
    end
  end

  def sync_streams
    sync do |service|
      service.fetch_streams(@activity)
    end
  end

  private
  def sync
    @resource = @activity = Strava::Activity.find(params[:activity_id])
    identity = current_user.strava_identity
    service = Strava::ActivityService.new(identity)
    yield service
    flash[:success] = 'Данные обновлены'
  rescue ActiveRecord::RecordNotFound => e
    raise e
  rescue => e
    Rails.logger.error(e)
    flash[:error] = "Ошибка #{e.class.to_s}\n#{e.message}\n#{e.backtrace.first(3).join("\n")}"
  ensure
    redirect_back fallback_location: strava_activity_path(@activity)
  end

  def index_where_params
    params.permit(:strava_type)
  end


end
