class Strava::SyncsController < StravaController
  def create
    identity = current_user.strava_identity
    strava_sync = identity.strava_syncs.create!(sync_params)
    Strava::ActivityService.new(
      identity,
      stream_job: Strava::StreamJob,
      activity_job: Strava::ActivityJob,
    ).fetch_activities(sync_id: strava_sync.id, after: sync_params[:after])
    flash[:success] = "Базовая синхронизация выполнена. Подробности (подробный маршрут, данные с датчиков и прочее) скачаются чуть позже"
  rescue => e
    Rails.logger.error(e)
    flash[:error] = "Ошибка синхронизации #{e.class.to_s}\n#{e.message}\n#{e.backtrace.first(3).join("\n")}"
  ensure
    redirect_back fallback_location: strava_index_path
  end

  private
  def sync_params
    params.require(:strava_sync).permit(:after)
  end
end
