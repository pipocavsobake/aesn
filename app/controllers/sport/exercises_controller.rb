class Sport::ExercisesController < SportController
  breadcrumb 'Упражнения', :sport_exercises
  before_action :authenticate_user!, only: %i[new create edit update]
  before_action :set_exercise, only: %i[edit update show refresh_video_derivatives]
  before_action :load_dictionaries, only: %i[new edit]

  def index
    @video_js = true
    @exercises = Sport::Exercise.all.page(params[:page]).per(12)
  end

  def new
    breadcrumb 'Новое', request.fullpath
    @exercise = Sport::Exercise.new
  end

  def create
    Sport::Exercise.transaction do
      @exercise = current_user.sport_exercises.create!(sport_exercise_params)
    end
    redirect_to @exercise, flash: {success: 'Успех'}
  rescue => e
    flash[:error] = "#{e.class.to_s}\n#{e.message}\n#{e.backtrace.first(4).join("\n")}"
    breadcrumb 'Новое', request.fullpath
    load_dictionaries
    render 'new'
  end

  def show
    @use_vanilla_bootstrap = @video_js = true
    breadcrumb @exercise.name, request.fullpath
  end

  def edit
    breadcrumb @exercise.name, sport_exercise_path(@exercise)
    breadcrumb "Редактирование", request.fullpath
  end

  def update
    authorize! :update, @exercise
    Sport::Exercise.transaction do
      @exercise.update!(sport_exercise_params)
    end
    redirect_to @exercise, flash: {success: 'Успех'}
  rescue => e
    flash[:error] = "#{e.class.to_s}\n#{e.message}\n#{e.backtrace.first(4).join("\n")}"
    load_dictionaries
    breadcrumb @exercise.name, request.fullpath
    render 'edit'
  end

  def refresh_video_derivatives
    authorize! :update, @exercise
    @exercise.video_derivatives!
    @exercise.save!
    flash[:success] = 'Успех'
  rescue => e
    flash[:error] = "#{e.class.to_s}\n#{e.message}\n#{e.backtrace.first(4).join("\n")}"
  ensure
    redirect_back fallback_location: sport_exercise_path(@exercise)
  end

  private
  def sport_exercise_params
    params.require(:sport_exercise).permit(:name, :content, :video, :simple_group_name)
  end

  def set_exercise
    @resource = @exercise = Sport::Exercise.find(params[:id] || params[:exercise_id])
  end

  def load_dictionaries
    @simple_groups = Sport::Exercise::SimpleGroup.pluck(:name)
  end
end
