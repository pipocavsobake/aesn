class Sport::ActivitiesController < SportController
  breadcrumb 'Тренировки', :sport_activities
  before_action :authenticate_user!, except: %i[show]
  before_action :set_kinds, only: %i[new create edit update]
  before_action :set_activity, only: %i[edit update show]

  def index
    @sport_activities = current_user.sport_activities.reorder(id: :desc).page(params[:page]).per(12)
  end

  def new
    breadcrumb 'Новая', request.fullpath
    @sport_activity = provider_activity&.to_sport_activity || Sport::Activity.new
  end

  def create
    @sport_activity = current_user.sport_activities.new(sport_activity_params)
    @sport_activity.save!
    redirect_to @sport_activity, flash: {success: 'Успех'}
  rescue => e
    flash[:error] = "#{e.class.to_s}\n#{e.message}\n#{e.backtrace.first(4).join("\n")}"
    breadcrumb 'Новая', new_sport_activity_path
    render 'new'
  end

  def show
    @video_js = true
    @page = @sport_activity
    breadcrumb @sport_activity.name, request.fullpath
  end

  def edit
    breadcrumb @sport_activity.name, sport_activity_path(@sport_activity)
    breadcrumb "Редактирование", edit_sport_activity_path(@sport_activity)
  end

  def update
    @sport_activity.update!(sport_activity_params)
    redirect_to @sport_activity, flash: {success: 'Успех'}
  rescue => e
    flash[:error] = "#{e.class.to_s}\n#{e.message}\n#{e.backtrace.first(4).join("\n")}"
    breadcrumb @sport_activity.name, request.fullpath
    render 'edit'
  end

  private
  def provider_activity
    if params[:strava_activity_id]
      return Strava::Activity.find(params[:strava_activity_id])
    end
    nil
  end

  def sport_activity_params
    params.require(:sport_activity).permit(
      :name,
      :strava_activity_id,
      :kind,
      :duration,
      :started_at,
      :enabled,
      activity_exercises_attributes: %i[
        id
        exercise_id
        comment
        _destroy
      ]
    )
  end

  def set_kinds
    @kinds = (current_user.sport_activities.distinct.pluck(:kind) + Sport::Activity.default_kinds).sort.uniq
  end

  def set_activity
    @resource = @sport_activity = Sport::Activity.accessible_by(current_ability).find(params[:id])
  end
end
