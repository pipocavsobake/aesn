class Sport::Exercises::SimpleGroupsController < SportController
  breadcrumb 'Группы упражнений', :sport_exercises_simple_groups
  def index
    @simple_groups = Sport::Exercise::SimpleGroup.all.page(params[:page]).per(12)
  end

  def show
    @simple_group = Sport::Exercise::SimpleGroup.find(params[:id])
    @exercises = @simple_group.exercises.page(params[:page]).per(12)

    @video_js = true
    breadcrumb @simple_group.name, sport_exercises_simple_group_path(@simple_group)
  end
end
