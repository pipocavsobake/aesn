class Yandex::BrowserController < YandexController
  def message
    resp = Yandex::Message.create!(data: params.permit!).response
    render json: {
      session: ya_session,
      version: params.require(:version),
      response: resp
    }
  end

  # Очередь задачь для poll'инга
  def show
    @browser = Yandex::Browser.find(params[:id])
    render json: {
      commands: @browser.commands.destroy_all,
    }
  end

  # Ответ от браузера
  def answer
    @browser = Yandex::Browser.find(params[:id])
    p params[:data]
    render json: {ok: true}
  end

  def create
    @browser = Yandex::Browser.create!
    render json: @browser
  end

  private
  def yandex_params
    params.require(:request).permit(:command, :original_utterance)
  end

  def ya_session
    params.require(:session).permit(:session_id, :message_id, :user_id)
  end
end
