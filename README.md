# Aesn
Rails, React
## Настройка
В системе должны быть установлены `postgresql`, `redis`, `ruby`, `bundler`, `nodejs`, `yarn` (это необходимо). Для некоторой функциональности нужны `imagemagick`, `ffmpeg`.

```
cp config/database.yml.example config/database.yml # указать пароли
cp config/secrets.yml.example config/sectets.yml # указать ключи от сервисов
bundle install
bundle exec rake db:migrate # или thor dl для скачивания боевой базы
yarn install
```

## Запуск
```
./bin/webpack-dev-server
bundle exec puma
```

## Деплой
```
bundle exec cap production deploy
```

## Тесты
```
bundle exec rspec
```
