namespace :experiment do
  def red_restart?(value)
    value < 1
  end

  def blue_restart?(value)
    value < 1
  end
  task :restart do
    results = [-1, 0, 1]
    iterations = 100_000
    restarts = 2

    res = []

    iterations.times do
      red_restarts, blue_restarts = [restarts, restarts]
      result_red, result_blue = [nil, nil]
      while red_restarts > 0 || blue_restarts > 0
        result_red = results.sample
        result_blue = results.sample

        if red_restart?(result_red)
          red_restarts -= 1
          next
        end

        if blue_restart?(result_blue)
          blue_restarts -= 1
          next
        end

        break
      end
      res << [result_red, result_blue]
    end
    puts res.map(&:first).reduce(&:+)
    puts res.map(&:second).reduce(&:+)

  end
end
