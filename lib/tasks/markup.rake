namespace :markup do
  task tf: :environment do
    markup = LocalMarkup::Markup.find(27)
    CSV.open('tmp/tf27.csv', 'w') do |csv|
      csv << ['url', 'ok', 'nsfw', 'sfw']
      markup.result_csv.each do |url, ok|
        resp = HTTParty.post('http://localhost:5000', body: "url=#{url}")
        tf = resp.parsed_response
        csv << [url, ok, tf['nsfw'], tf['sfw']]
      end
    end
  end
end
