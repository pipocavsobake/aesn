# frozen_string_literal: true

require 'cloner'

# thor dl command setup
class Dl < Cloner::Base
  no_commands do
    def rails_path
      File.expand_path('../../config/environment', __dir__)
    end

    def ssh_host
      'aesn.ru'
    end

    def ssh_user
      'aesn'
    end

    def remote_dump_path
      '/home/aesn/dump'
    end

    def pg_remote_bin_path(util)
      "/usr/lib/postgresql/14/bin/#{util}"
    end

    def remote_app_path
      '/home/aesn/app/current'
    end

    def env_from
      @from
    end

    def verbose?
      true
    end
  end

  desc 'download', 'clone files and DB from production'
  option :from
  def download
    load_env
    @from = @options[:from].presence || 'production'
    clone_db
    #rsync_public('uploads')
  end
end
