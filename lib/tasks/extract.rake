namespace :extract do
  task :merge_csv do
    dest = ARGV[-1]
    inputs = ARGV[1..-2]
    if inputs.size == 1 && inputs.first.include?('*')
      inputs = Dir.glob(inputs.first)
    end
    l_inputs = inputs[0..-2]
    l_widths = l_inputs.map do |l_input|
      lwidth = 0
      CSV.foreach(l_input) do |row|
        lwidth = [lwidth, row.size].max
      end
      lwidth
    end
    CSV.open(dest, 'w') do |csv|
      csvs = inputs.map { |input| CSV.open(input) }
      loop do
        lines = csvs.map(&:readline)
        break if lines.all?(&:nil?)
        lines = lines.map.with_index do |line, idx|
          line = line.to_a
          next line if idx == lines.size - 1
          line += [nil] * (l_widths[idx] - line.size) if l_widths[idx] > line.size
          line
        end
        csv << lines.reduce(&:+)
      end
      csvs.each { |i_csv| i_csv.close }
    end

    # Иначе argv-аргументы интерпретируются как rake-задачи
    exit(0)
  end
  desc 'stress'
  task :stress do
    1000.times do |i|
      puts i
      f = CSV.open('tmp/cg.2.1.csv')
      items = Hash[f.read].transform_values { |v| JSON.parse(v) }
      f.close
      puts items.size
      GC.start
    end
  end
  desc 'hd2'
  task hd2: :environment do
    puts "source ~/.zshrc"
    CSV.open('tmp/extract_errors.csv', 'w') do |csv|
      CSV.open('tmp/2extract.csv').read.each do |row|
        fullpath = row[0]
        new_path = File.join('/hd1', fullpath, File.basename(fullpath))
        unless File.file?(new_path)
          next
          FileUtils.mkdir_p(File.dirname(new_path))
          FileUtils.copy_file(fullpath, new_path, true)
        end
        #puts Terrapin::CommandLine.new('cd', ':path').command(path: File.dirname(new_path))
        #puts "#{Terrapin::CommandLine.new('extract', ':path').command(path: File.basename(new_path))} && #{Terrapin::CommandLine.new('rm', ':path').command(path: File.basename(new_path))}"
        csv << [new_path]
      end
    end
  end

  desc 'mark'
  task :mark do
    items = CSV.open('tmp/combined.csv', headers: true).read
    CSV.open("tmp/cg.csv", "w") do |csv|
      grouped = items.group_by { |item| item['htmlurl'] }.each do |root, objects|
        files = objects.pluck('fullpath')
        ret = files.select { |file| File.file?(file) && !File.directory?(file) }
        out = ret + ret.select do |file|
          %w[.rar .zip .7z].include?(File.extname(file).downcase)
        end.map do |file|
          dir = "/hd1#{file}"
          if File.directory?(dir)
            #puts dir
            Dir.glob(File.join(dir, "**", "*"))
          end
        end.compact.flatten.map do |path|
          begin
            {a: path}.to_json
            path
          rescue JSON::GeneratorError
            begin
              encoded = path.force_encoding("windows-1251").encode("UTF-8")
              {a: encoded}.to_json
              encoded
            rescue JSON::GeneratorError, Encoding::UndefinedConversionError
              p path
              nil
            end
          end
        end.compact
        next if out.empty?
        csv << [root, out.to_json]
      end
    end
    #File.open("tmp/cg.csv", 'w') { |f| f.write(grouped.reject { |_, v| v.empty? }.to_json)  }
  end

  desc 'combine'
  task :combine do
    data = CSV.open('tmp/go/types/result.csv').read
    files = data.select { |*_, e| !e&.ends_with?('is a directory') }.map(&:first).select { |f| !f.starts_with?('/hd2/000') }
    hash = Hash[files.zip([true] * files.size)]
    meta = Dir.glob('tmp/*.map.csv').map { |path| CSV.open(path, headers: true).read.map(&:to_h) }.reduce(&:+)
    meta.select { |row| hash[row['fullpath']] }
    CSV.open('tmp/wp.combined.csv', 'w') do |wp_csv|
      CSV.open('tmp/combined.csv', 'w') do |csv|
        csv << meta.first.keys
        meta.each { |row| csv << row.values; wp_csv << [row['fullpath'], row['htmlurl']] if row['haspass'] }
      end
    end
  end

  desc 'yandex'
  task :yandex do
    root = '/hd2/yadi.sk'
    sav_file = '/data/ic/yadi.sk/inputs.txt'
    map_file = '/data/ic/yadi.sk/map'
    urls_file = '/home/anatoly/workspaces/go/src/gitlab.com/pipocavsobake/goclon/tmp/urls.csv.bak'
    with_pass_file = '/home/anatoly/workspaces/go/src/gitlab.com/pipocavsobake/goclon/tmp/ic_with_pass.csv'
    ic_dir = '/home/anatoly/workspaces/go/src/gitlab.com/pipocavsobake/goclon/tmp/ic'
    out_file = 'tmp/yadi.sk.map.csv'

    lines = File.open(sav_file).read.split("\n")
    groups = lines.in_groups_of(4)
    raise if groups.find { |gr| gr[0].starts_with?('  ') }
    raise if groups.find { |gr| !gr[1].starts_with?('  out=') }
    raise if groups.find { |gr| !gr[2].starts_with?('  referer=') }
    raise if groups.find { |gr| !gr[3].starts_with?('  dir=') }
    ya_hash = Hash[groups.map { |a,b,c,d| [b.delete_prefix('  out=').strip, c.delete_prefix('  referer=').strip] }]

    ic_hash = Hash[CSV.open(urls_file).read].transform_keys(&:strip)

    with_pass = CSV.open(with_pass_file).read.map(&:first)
    with_pass = Hash[with_pass.zip([true] * with_pass.size)]
    with_pass.default = false

    map_hash = Hash[CSV.open(map_file).read.map(&:reverse)].transform_keys(&:strip)
    CSV.open(out_file, 'w') do |csv|
      csv << ['fullpath', 'htmlpath', 'htmlurl', 'haspass']
      map_hash.each do |desk_url, dirname|
        Dir.glob(File.join(root, dirname, "**", "*")).each do |fullpath|
          next if %w[. ..].include?(fullpath)

          ic_slug = File.basename(ic_hash[desk_url])
          html_path = File.join(ic_dir, ic_slug)
          ic_url = URI.join('http://www.intoclassics.net/news/', ic_slug)
          has_pass = with_pass[ic_slug]

          csv << [fullpath, html_path, ic_url, has_pass]
        end
      end
    end
  end

  desc 'mail_files'
  task :mail_files do
    root = '/hd2/cloud.mail.ru'
    sav_file = '/data/ic/files.mail.ru/input.txt'
    urls_file = '/home/anatoly/workspaces/go/src/gitlab.com/pipocavsobake/goclon/tmp/urls.csv.bak'
    with_pass_file = '/home/anatoly/workspaces/go/src/gitlab.com/pipocavsobake/goclon/tmp/ic_with_pass.csv'
    ic_dir = '/home/anatoly/workspaces/go/src/gitlab.com/pipocavsobake/goclon/tmp/ic'
    out_file = 'tmp/files.mail.map.csv'

    lines = File.open(sav_file).read.split("\n")
    groups = lines.in_groups_of(4)
    mail_hash = Hash[groups.map { |a,b,c,d| [b.delete_prefix('  out=').strip, c.delete_prefix('  referer=').strip] }]

    ic_hash = Hash[CSV.open(urls_file).read].transform_keys(&:strip)

    with_pass = CSV.open(with_pass_file).read.map(&:first)
    with_pass = Hash[with_pass.zip([true] * with_pass.size)]
    with_pass.default = false

    CSV.open(out_file, "w") do |csv|
      csv << ['fullpath', 'htmlpath', 'htmlurl', 'haspass']
      mail_hash.each do |filename, desk_url|
        fullpath = File.join(root, filename)
        ic_slug = File.basename(ic_hash[desk_url])
        html_path = File.join(ic_dir, ic_slug)
        ic_url = URI.join('http://www.intoclassics.net/news/', ic_slug)
        has_pass = with_pass[ic_slug]
        csv << [fullpath, html_path, ic_url, has_pass]
      end
    end
  end


  desc 'mail_cloud'
  task :mail_cloud do
    root = '/hd2/cloud.mail.ru'
    sav_file = '/data/ic/cloud.mail.ru/input.txt.sav'
    urls_file = '/home/anatoly/workspaces/go/src/gitlab.com/pipocavsobake/goclon/tmp/urls.csv.bak'
    with_pass_file = '/home/anatoly/workspaces/go/src/gitlab.com/pipocavsobake/goclon/tmp/ic_with_pass.csv'
    ic_dir = '/home/anatoly/workspaces/go/src/gitlab.com/pipocavsobake/goclon/tmp/ic'
    out_file = 'tmp/cloud.mail.map.csv'

    lines = File.open(sav_file).read.split("\n")
    groups = lines.in_groups_of(4)
    mail_hash = Hash[groups.map { |a,b,c,d| [b.delete_prefix('  out=').strip, c.delete_prefix('  referer=').strip] }]

    ic_hash = Hash[CSV.open(urls_file).read].transform_keys(&:strip)

    with_pass = CSV.open(with_pass_file).read.map(&:first)
    with_pass = Hash[with_pass.zip([true] * with_pass.size)]
    with_pass.default = false

    CSV.open(out_file, "w") do |csv|
      csv << ['fullpath', 'htmlpath', 'htmlurl', 'haspass']
      mail_hash.each do |filename, desk_url|
        fullpath = File.join(root, filename)
        ic_slug = File.basename(ic_hash[desk_url])
        html_path = File.join(ic_dir, ic_slug)
        ic_url = URI.join('http://www.intoclassics.net/news/', ic_slug)
        has_pass = with_pass[ic_slug]
        csv << [fullpath, html_path, ic_url, has_pass]
      end
    end
  end
end
