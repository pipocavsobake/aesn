namespace :heroes do
  namespace :fizmig do
    def create_unit(row)
      VideoMarkup::Heroes::Unit.create!(unit_params(row))
    end
    def update_unit(unit, row)
      unit.update!(unit_params(row).try { |attrs| attrs[:aliases] = (attrs[:aliases] + (unit.aliases || [])).uniq; attrs })
    end

    def unit_params(row)
      {
        name: row[2].squish,
        level: row[3],
        damage_min: row[5],
        damage_max: row[6],
        attack: row[7],
        defence: row[8],
        health_points: row[9],
        speed: row[10],
        pop_growth: row[11],
        gold: row[12],
        skills: row[14],
        aliases: ((row[1].blank? || (row[1].squish == row[2].squish)) ? [] : [row[1]]),
        ai_value: row[15],
      }
    end
    task units: :environment do
      CSV.foreach('tmp/units_dump.csv') do |row|
        Hash[VideoMarkup::Heroes::Unit.all.map { |u| [u.id, u] }].try do |units|
          unit = units[row[0].to_i]
          if unit
            update_unit(unit, row)
          else
            create_unit(row)
          end
        end
      end
    end

    task aliases: :environment do
      [
        ['минимальный урон', :damage_min],
        ['максимальный урон', :damage_max],
        ['атака', :attack],
        ['защита', :defence],
        ['здоровье', :health_points],
        ['стоимость', :gold],
        ['стоит', :gold],
        ['особенность', :skills],
        ['ценность', :ai_value],
      ].each do |name, target|
        Marusia::Alias.find_or_initialize_by(name: name, kind: 'h3_hota_units').update!(target: target)
      end

      if !Marusia::Phrase.where(kind: :h3_hota_greetings).any?
        Marusia::Phrase.create!(
          kind: :h3_hota_greetings,
          content: 'Спросите меня про юнитов игры Герои 3 HOTA, например: «сколько здоровья у копейщика» или «какая скорость у виверны»',
          tts: 'Спросите меня про юнитов игры Герои 3 х`ота, например: сколько здоровья у копейщика или какая скорость у вив`ерны',
        )
      end

    end
  end
end
