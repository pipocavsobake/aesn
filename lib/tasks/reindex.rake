namespace :reindex do
  desc 'missing'
  task missing: :environment do
    Rails.eager_load!
    ::Searchkick.models.each do |model|
      begin
        model.search('*', limit: 1, load: false).first
      rescue Searchkick::MissingIndexError
        puts "Running `#{model.to_s}.reindex`"
        model.reindex
      end
    end
  end
end
