namespace :strava do
  task fetch_activities: :environment do
    User::Identity.where(provider: :strava).each do |identity|
      identity.strava_activities.preload(:stream_set).each do |activity|
        if activity.stream_set.nil?
          Strava::StreamJob.perform_later(activity.id)
        end
      end
      after = 1.day.ago
      strava_sync = identity.strava_syncs.create!(after: after)
      Strava::ActivityService.new(
        identity,
        stream_job: Strava::StreamJob,
        activity_job: Strava::ActivityJob,
      ).fetch_activities(sync_id: strava_sync.id, after: after)
    end
  end
end
