namespace :mmm_printer do
  task run_loop: :environment do
    Terrapin::CommandLine.logger = Logger.new(STDOUT)
    print_cmd = Terrapin::CommandLine.new("lpr", '-P HP_LaserJet_P1005 :file_path')
    pdf_cmd = Terrapin::CommandLine.new('libreoffice', '--headless --invisible --convert-to pdf --outdir /tmp :file_path')
    loop do
      t1 = Time.now
      resp = HTTParty.get("https://aesn.ru/mmm/print_requests?q[state_eq]=pending")
      if resp.code != 200
        raise "Bad response #{resp.code} #{resp.body.inspect}"
      end
      data = resp.body
      data = JSON.parse(data) if data.is_a?(String)
      data.each do |request|
        id = request['id']
        file_url = request['file_url']
        uri = URI.parse(file_url)
        tmp_path = "/tmp/mmm_printer_#{id}_#{File.basename(uri.path)}"
        dl_resp = HTTParty.get(file_url)
        if dl_resp.code != 200
          raise "Не получилось скачать файл #{file_url} #{dl_resp.code} #{dl_resp.body.inspect}"
        end
        File.open(tmp_path, 'wb') { |f| f.write(dl_resp.body) }
        error = nil
        begin
          print_cmd.run(file_path: tmp_path)
        rescue => e
          error = "#{e.class.to_s}\n#{e.message}\n#{e.backtrace.first}"
        end
        extname = File.extname(tmp_path)
        if error.present? && extname != '.pdf'
          begin
            pdf_cmd.run(file_path: tmp_path)
            print_cmd.run(file_path: "#{tmp_path.delete_suffix(extname)}.pdf")
            error = nil
          rescue => e
            error += "\n#{e.class.to_s}\n#{e.message}\n#{e.backtrace.first}"
          end
        end
        payload = {mmm_print_request: {state: (error.nil? ? :done : :failed), data: {error: error}}, random_string: SecureRandom.hex(40)}.to_json
        resp = HTTParty.put("https://aesn.ru/mmm/print_requests/#{id}", body: payload, headers: {
          'Content-type' => 'application/json',
          'X-Checksum' => Digest::MD5.hexdigest("#{payload}#{Rails.application.secrets.mmm_printer_secret}")
        })
        if resp.code > 299
          raise "Bad response #{resp.code} #{resp.body.inspect}"
        end
      end
    rescue => e
      puts e.class.to_s
      puts e.message
      puts e.backtrace.first(3)
    ensure
      t2 = Time.now
      dt = 10.0 - (t2 - t1)
      sleep dt if dt > 0
    end
  end
end
