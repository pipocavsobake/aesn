const path = require('path');
const { environment } = require('@rails/webpacker')
const jquery = require('./plugins/jquery')

environment.config.set('resolve.alias.~', path.resolve(__dirname, '..', '..', 'app', 'javascript'));

environment.plugins.prepend('jquery', jquery)
module.exports = environment
