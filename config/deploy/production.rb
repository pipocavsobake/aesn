# frozen_string_literal: true

set :stage, :production
set :rails_env, :production

server 'aesn.ru', user: 'aesn', roles: %w[web app db]
