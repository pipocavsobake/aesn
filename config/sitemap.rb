# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "https://aesn.ru"
SitemapGenerator::Sitemap.sitemaps_path = 'sitemap/'

SitemapGenerator::Sitemap.create do

  add video_markup_path


  add widgets_path
  add widgets_keyboards_path
  add new_widgets_keyboard_path
  Widgets::Keyboard.enabled.select(:slug).each do |keyboard|
    add widgets_keyboard_path(keyboard.slug)
  end

  add video_markup_heroes_banks_path
  add video_markup_heroes_heroes_path
  add video_markup_heroes_units_path
  add video_markup_videos_path

  VideoMarkup::Heroes::Bank.with_fights.select(:id).each do |bank|
    add video_markup_heroes_bank_path(bank.slug)
  end
  VideoMarkup::Heroes::Hero.with_fights.select(:id).each do |bank|
    add video_markup_heroes_hero_path(bank.slug)
  end
  VideoMarkup::Heroes::Unit.with_fights.select(:id).each do |bank|
    add video_markup_heroes_unit_path(bank.slug)
  end

  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end
end
