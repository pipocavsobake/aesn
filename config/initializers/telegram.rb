class Telegram::Bot::Client
  def initialize(token = nil, username = nil, server: SERVER, **options)
    proxy = Rails.application.secrets.telegram_proxy_proxy
    proxy_user = Rails.application.secrets.telegram_proxy_user
    proxy_pass = Rails.application.secrets.telegram_proxy_pass
    @client = HTTPClient.new(proxy)
    @client.set_proxy_auth(proxy_user, proxy_pass)
    @token = token || options[:token]
    @username = username || options[:username]
    @base_uri = format(URL_TEMPLATE, token: self.token, server: server)
  end
end
