# frozen_string_literal: true

set :user, 'aesn'
set :application, 'aesn'

set :repo_url, 'git@gitlab.com:pipocavsobake/aesn.git'
set :branch, ENV['REVISION'] || 'master'
# set :default_env, {
#  'REDIS_URL' => 'redis://localhost:6379/1'
# }

set :deploy_to, -> { "/home/#{fetch(:user)}/app" }

set :puma_state, -> { File.join(shared_path, 'tmp', 'puma', 'state') }
set :puma_pid, -> { File.join(shared_path, 'tmp', 'puma', 'pid') }
set :puma_conf, -> { File.join(current_path, 'config', 'puma.rb') }
set :puma_service_unit_name, "puma"
set :puma_systemctl_user, :user

# set :sidekiq_concurrency, 5 # кол-во тредов на процесс, синхронизировано с pool в active_record

Rake::Task['puma:check'].clear
Rake::Task['puma:config'].clear
namespace :puma do
  task :check do
    puts 'override'
  end
  task :config do
  end
end

set :keep_releases, 10

set :use_sudo, :false

# set :linked_files, %w(config/database.yml config/secrets.yml public/robots.txt public/sitemap.xml config/puma.rb)
set :linked_files, %w[config/database.yml config/secrets.yml]
set :linked_dirs, %w[log tmp vendor/bundle public/assets public/samples node_modules public/packs public/uploads public/sitemap]

set :sidekiq_service_unit_name, "sidekiq.service"


after 'sidekiq:start', 'reindex:missing'
namespace :reindex do
  task :missing do
    on roles(:db) do
      within release_path do
        execute :rake, 'reindex:missing'
      end
    end
  end
end

set :rsync_cmd, "rsync -avz --progress"
