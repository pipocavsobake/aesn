require 'sidekiq/web'
Rails.application.routes.draw do
  constraints subdomain: "saiu" do
    get '', to: 'saiu/claims#new'
    namespace :saiu do
      resources :claims do
        collection do
          get :thanks
        end
      end
    end
  end
  ActiveAdmin.routes(self)
  devise_for :users, controllers: {
    omniauth_callbacks: 'users/omniauth_callbacks',
  }
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'home#index'
  namespace :widgets do
    resources :keyboards, only: %i[index show new create]
    resources :calendars
  end
  resources :widgets, only: :index

  get '/homma_fights', to: 'video_markup/heroes/fights#index', as: 'video_markup_heroes_fights'
  scope :homma_fights do
    get ':slug', to: 'video_markup/heroes/units#show', constraints: { slug: /unit-.*/ }, as: 'video_markup_heroes_unit'
    get ':slug', to: 'video_markup/heroes/heroes#show', constraints: { slug: /hero-.*/ }, as: 'video_markup_heroes_hero'
    get ':slug', to: 'video_markup/heroes/banks#show', constraints: { slug: /bank-.*/ }, as: 'video_markup_heroes_bank'
  end
  namespace :video_markup do
    resources :videos, only: %i[show new create index]
    resources :moments
    namespace :heroes do
      resources :units, only: %i[index]
      resources :banks, only: %i[index]
      resources :heroes, only: %i[index]
    end
  end
  get '/video_markup', to: 'video_markup#index'
  namespace :strava do
    resources :activities, only: %i[index show] do
      put :sync_details
      put :sync_streams
    end
    resources :syncs, only: %i[create]
  end
  resources :strava, only: :index do
  end
  namespace :sport do
    resources :activities
    namespace :exercises do
      resources :simple_groups, only: %i[index show]
    end
    resources :exercises do
      put :refresh_video_derivatives
    end
  end
  resources :sport, only: :index
  resources :local_markups, only: %i[create index new show] do
    resources :check_points, only: %i[update show index], controller: 'local_markup/check_points'
    get 'result'
  end
  resources :share_links, only: %i[new create show index]
  authenticate :user, lambda { |u| Ability.new(u).can?(:read, :sidekiq) } do
    mount Sidekiq::Web => "/sidekiq"
  end
  telegram_webhook Telegram::ChordQuizController, :chord_quiz
  telegram_webhook Telegram::WebhookController
  telegram_webhook Telegram::MmmPrinterController, :mmm_printer
  post '/yandex/browser/message', to: 'yandex/browser#message'
  post '/yandex/browser', to: 'yandex/browser#create'
  post '/yandex/browser/answer', to: 'yandex/browser#answer'
  get '/yandex/browser/:id', to: 'yandex/browser#show'
  post '/webhooks/twitch', to: 'webhooks#twitch'
  get '/webhooks/twitch', to: 'webhooks#twitch_check'
  post '/webhooks/twitch/:streamer_id', to: 'webhooks#twitch'
  get '/webhooks/twitch/:streamer_id', to: 'webhooks#twitch_check'
  namespace :marusia do
    resources :heroes, only: :create
  end
  namespace :mmm do
    resources :print_requests, only: %i[index update]
  end

  namespace :user do
    resources :merges, only: %i[new create update] do
      collection do
        get "confirm"
      end
    end
  end

  resources :users, only: %i[index show] do
    resources :runs, only: :index, controller: 'users/runs'
  end

  namespace :api do
    namespace :v1 do
      namespace :sport do
        resources :exercises, only: [] do
          collection do
            get :autocomplete
          end
        end
      end
    end
  end
end
