module AdminHelpers
  extend ActiveSupport::Concern
  def with_redirect_back
    yield
  rescue => e
    puts e.class.to_s
    puts e.message
    puts e.backtrace
    redirect_back fallback_location: collection_path, flash: { error: I18n.t("admin.actions.#{action_type}")}
  end

  def action_type
    return :member if params[:id].present?
    return :batch if params[:ids].present?

    :collection
  end

end
