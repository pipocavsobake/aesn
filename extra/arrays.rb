module Arrays
  module_function

  def moving_average(a, ndays, precision)
    a.each_cons(ndays).map { |e| e.reduce(&:+).fdiv(ndays).round(precision) }
  end

  def filled_moving_average(a, ndays, precision)
    half = ((ndays - 1) / 2).to_i
    other_half = ndays - half - 1
    a[0...half] + moving_average(a, ndays, precision) + a[-other_half..-1]
  end
end
