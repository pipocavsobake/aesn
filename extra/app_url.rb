module AppUrl
  module_function
  def http_host
    opts = Rails.application.routes.default_url_options
    return opts[:host] if opts[:port].blank?

    "#{opts[:host]}:#{opts[:port]}"
  end

  def recognize_path(path, subdomain: '', method: 'GET', **extras)
    path = ActionDispatch::Journey::Router::Utils.normalize_path(path) unless %r{://}.match?(path)
    env = Rack::MockRequest.env_for(path, method: method)
    host = http_host
    host = "#{subdomain}.#{host}" if subdomain.present?
    env.merge!({"X-FORWARDED-HOST" => host, "HTTP_HOST" => host})
    Rails.application.routes.try do |route_set|
      req = route_set.send(:make_request, env)
      route_set.send(:recognize_path_with_request, req, path, extras)
    end
  end

  def recognize_url(url, method: 'GET', **extras)
    uri = URI.parse(url)
    if !uri.host.ends_with?('.travelask.ru') && uri.host != 'travelask.ru'
      raise ActionController::RoutingError.new("wrong host #{uri.host}")
    end

    subdomain = uri.host.delete_suffix('travelask.ru').delete_suffix('.')
    recognize_path(uri.path, subdomain: subdomain)
  end
end
