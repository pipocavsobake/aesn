module AdminHelpers::Images
  extend ActiveSupport::Concern
  def self.preview(context, attachment, image_attrs: {})
    return nil if attachment.nil?

    context.link_to(
      context.image_tag(attachment.url, **image_attrs),
      attachment.url, target: '_blank',
    )
  end
end
