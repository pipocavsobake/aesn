module AdminHelpers::Cases
  extend ActiveSupport::Concern
  LIST = %i[nomn_sing gent_sing datv_sing accs_sing ablt_sing loct_sing nomn_plur gent_plur datv_plur accs_plur ablt_plur loct_plur]

  def self.table(context, resource)
    context.table do
      LIST.in_groups(2).to_a.transpose.each do |keys|
        context.tr do
          keys.each do |ru_case|
            context.td { resource.cases[ru_case.to_s] }
          end
        end
      end
    end
  end

  def self.inputs(context, form)
    form.inputs name: 'Падежи', for: :cases do |c_f|
      LIST.each do |ru_case|
        c_f.input ru_case, input_html: { value: form.object.cases[ru_case.to_s] }
      end
    end
  end

  def self.row(context, resource)
    context.row :cases do
      table(context, resource)
    end
  end
end
