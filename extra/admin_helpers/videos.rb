module AdminHelpers::Videos
  DEFAULT_VIDEO_ATTRS = {controls: true}
  extend ActiveSupport::Concern
  def self.preview(context, attachment, video_attrs: {})
    return nil if attachment.nil?

    context.video('', **DEFAULT_VIDEO_ATTRS.merge(video_attrs).merge(src: attachment.url))
  end
end
