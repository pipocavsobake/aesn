module TwitchHelpers
  def self.get(path, **query)
    resp = HTTParty.get("https://api.twitch.tv#{path}", headers: {
      'Client-Id' => Rails.application.secrets.twitch_id,
      'Accept' => 'application/vnd.twitchtv.v5+json',
      'Authorization' => "Bearer #{User::Identity.twitch_authorization}"
    }, query: query)
    if resp.code < 200 || resp.code > 299
      raise "Bad twitch api #{path} code #{resp.code} #{resp.body}"
    end
    resp.to_h
  end
  def self.streams(game_id: )
    get('/helix/streams', game_id: game_id, first: 100, language: 'ru')
  end

  def self.games(id: nil, name: nil)
    raise "Provide id or name" if id.nil? && name.nil?

    get('/helix/games', id: id, name: name)
  end
end
