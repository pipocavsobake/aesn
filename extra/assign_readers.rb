# Для работы с фортепианной клавиатурой, где очень много ключей в hash-е
module AssignReaders
  extend ActiveSupport::Concern
  def initialize(args = {})
    args = args.to_h
    args.each { |k, v| instance_variable_set("@#{k}", v) }
    @_assign_reader_source = args
  end
  def [](key)
    instance_variable_get("@#{key}")
  end
  def slice(*args, &block)
    @_assign_reader_source.slice(*args, &block)
  end
  def to_h
    @_assign_reader_source.to_h
  end
end
