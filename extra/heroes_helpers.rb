module HeroesHelpers
  TWITCH_GAME_ID = "2798".freeze
  def self.active_streamers
    all = VideoMarkup::StreamerStatus.preload(:streamer).where(game_id: TWITCH_GAME_ID).where('created_at > ?', 90.seconds.ago).group_by(&:streamer).keys
    with_tts = all.select(&:tts)
    names = with_tts.map(&:name).join(', ')
    tts = with_tts.map(&:tts).join('. ')
    and_more = " и ещё" if with_tts.size > 0
    suffix = "#{and_more} #{(all.size - with_tts.size).humanize(locale: :ru)} игроков, имена которых я читать не умею" if all.size > with_tts.size
    suffix = "никого нет. По крайней мере я не вижу" if all.empty?
    {
      text: "Сейчас онлайн #{names}#{suffix}",
      tts: "Сейчас онлайн #{tts}#{suffix}",
      end_session: true,
    }
  end

  def self.youtube_streamers(ids)
    []
  end

  def self.fetch_twitch_streamers
    TwitchHelpers.streams(game_id: TWITCH_GAME_ID)['data'].each do |data|
      streamer = VideoMarkup::Streamer.find_or_initialize_by(kind: :twitch, slug: data['user_id'])
      streamer.update!(name: data['user_name'])
      streamer.statuses.create!(data.slice('game_id', 'started_at'))
    end
  end

end
