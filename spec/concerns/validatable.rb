require 'rails_helper'

RSpec.shared_examples 'validatable' do
  let(:model) { described_class }
  let(:model_sym) { model.to_s.underscore.to_sym }
  let(:object) { build(model_sym) }
  it("should have a valid factory") { expect(object).to be_valid }
  it("should have a creatable factory") do
    expect(create(described_class.to_s.underscore)).to be_persisted
  end
  # Код ниже использует private API из FactoryBot. При обновлении factory_bot может сломаться
  FactoryBot.factories[described_class.to_s.underscore].definition.defined_traits.each do |trait|
    it("should have a valid factory with trait #{trait.name}") do
      expect(build(described_class.to_s.underscore, trait.name)).to be_valid
    end
    it("should have a creatable factory with trait #{trait.name}") do
      expect(create(described_class.to_s.underscore, trait.name)).to be_persisted
    end
  end
end
