require 'vcr'

VCR.configure do |c|
  c.ignore_localhost = true
  c.cassette_library_dir = 'spec/cassettes'
  c.hook_into :webmock
  if ENV["CI"]
    c.default_cassette_options = {record: :none}
  else
    c.default_cassette_options = {record: :new_episodes}
  end
  c.configure_rspec_metadata!
  c.ignore_request do |request|
    port = request.parsed_uri.port
    host = request.parsed_uri.host
    next true if VCR::RequestIgnorer::LOCALHOST_ALIASES.include?(host)
    next true if host == 'chromedriver.storage.googleapis.com'
    next ![80, 443].include?(port)
  end
end
