Capybara.register_driver :chrome_root do |app|
  options = ::Selenium::WebDriver::Chrome::Options.new
  options.args << '--headless'
  options.args << '--no-sandbox'
  options.args << '--window-size=1920,1080'

  Capybara::Selenium::Driver.new(app, browser: :chrome, options: options)
end
Capybara.register_driver :chrome_visible do |app|
  options = ::Selenium::WebDriver::Chrome::Options.new

  Capybara::Selenium::Driver.new(app, browser: :chrome, options: options)
end

if ENV['CHROME_VISIBLE']
  Capybara.javascript_driver = :chrome_visible
else
  #Capybara.javascript_driver = :selenium_chrome_headless
  Capybara.javascript_driver = :chrome_root
end
Capybara.default_driver = :rack_test
Capybara.default_max_wait_time = 5

#Capybara.register_server :puma do |app, port, host|
  #require 'rack/handler/puma'
  #Rack::Handler::Puma.run(app, Host: host, Port: port, Threads: "1:1")
#end

Capybara.configure do |config|
  config.server = :puma
  config.server_port = 9334
  config.run_server = true
  config.always_include_port = true
end
