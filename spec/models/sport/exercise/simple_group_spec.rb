# == Schema Information
#
# Table name: sport_exercise_simple_groups
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'rails_helper'

RSpec.describe Sport::Exercise::SimpleGroup, type: :model do
  it_behaves_like "validatable"
end
