# == Schema Information
#
# Table name: strava_syncs
#
#  id               :bigint           not null, primary key
#  after            :datetime
#  user_identity_id :bigint           not null
#  summary_state    :string           default("new"), not null
#  streams_state    :string           default("new"), not null
#  details_state    :string           default("new"), not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_strava_syncs_on_user_identity_id  (user_identity_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_identity_id => user_identities.id)
#
require 'rails_helper'

RSpec.describe Strava::Sync, type: :model do
  it_behaves_like "validatable"
end
