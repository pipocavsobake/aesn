# == Schema Information
#
# Table name: strava_stream_sets
#
#  id          :bigint           not null, primary key
#  activity_id :bigint           not null
#  source_data :jsonb
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_strava_stream_sets_on_activity_id  (activity_id)
#
# Foreign Keys
#
#  fk_rails_...  (activity_id => strava_activities.id)
#
require 'rails_helper'

RSpec.describe Strava::StreamSet, type: :model do
  it_behaves_like "validatable"
end
