# == Schema Information
#
# Table name: local_markup_files
#
#  id             :bigint           not null, primary key
#  check_point_id :bigint           not null
#  path           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_local_markup_files_on_check_point_id  (check_point_id)
#
# Foreign Keys
#
#  fk_rails_...  (check_point_id => local_markup_check_points.id)
#
require 'rails_helper'

RSpec.describe LocalMarkup::File, type: :model do
  it_behaves_like 'validatable'
end
