# == Schema Information
#
# Table name: marusia_requests
#
#  id             :bigint           not null, primary key
#  request_params :jsonb
#  response       :jsonb
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
require 'rails_helper'

RSpec.describe Marusia::Request, type: :model do
  it_behaves_like "validatable"
end
