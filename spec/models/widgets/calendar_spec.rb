# == Schema Information
#
# Table name: widgets_calendars
#
#  id             :bigint           not null, primary key
#  name           :string           not null
#  date_from      :date             not null
#  date_to        :date             not null
#  file_data      :jsonb
#  user_id        :bigint           not null
#  content        :text
#  enabled        :boolean          default(FALSE), not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  column_mapping :jsonb
#
# Indexes
#
#  index_widgets_calendars_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
require 'rails_helper'

RSpec.describe Widgets::Calendar, type: :model do
  it_behaves_like "validatable"
end
