# == Schema Information
#
# Table name: saiu_claims
#
#  id         :bigint           not null, primary key
#  phone      :string
#  email      :string
#  best_time  :text
#  comment    :text
#  status     :string           default("new"), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'rails_helper'

RSpec.describe Saiu::Claim, type: :model do
  it_behaves_like "validatable"
end
