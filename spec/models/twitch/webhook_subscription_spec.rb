# == Schema Information
#
# Table name: twitch_webhook_subscriptions
#
#  id            :bigint           not null, primary key
#  callback      :string
#  mode          :string
#  topic         :string
#  lease_seconds :string
#  secret        :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  response_body :text
#  response_code :bigint
#  streamer_id   :bigint
#
# Indexes
#
#  index_twitch_webhook_subscriptions_on_streamer_id  (streamer_id)
#  index_twitch_webhook_subscriptions_on_topic        (topic) WHERE ((mode)::text = 'subscribe'::text)
#
# Foreign Keys
#
#  fk_rails_...  (streamer_id => video_markup_streamers.id)
#
require 'rails_helper'

RSpec.describe Twitch::WebhookSubscription, type: :model do
  it_behaves_like "validatable"
end
