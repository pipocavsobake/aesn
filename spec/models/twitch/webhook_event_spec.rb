# == Schema Information
#
# Table name: twitch_webhook_events
#
#  id         :bigint           not null, primary key
#  data       :jsonb
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'rails_helper'

RSpec.describe Twitch::WebhookEvent, type: :model do
  it_behaves_like "validatable"
end
