# == Schema Information
#
# Table name: video_markup_comments
#
#  id         :bigint           not null, primary key
#  moment_id  :bigint           not null
#  content    :text
#  user_id    :bigint           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_video_markup_comments_on_moment_id  (moment_id)
#  index_video_markup_comments_on_user_id    (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (moment_id => video_markup_moments.id)
#  fk_rails_...  (user_id => users.id)
#
require 'rails_helper'

RSpec.describe VideoMarkup::Comment, type: :model do
  it_behaves_like 'validatable'
end
