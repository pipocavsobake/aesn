# == Schema Information
#
# Table name: video_markup_streamers
#
#  id         :bigint           not null, primary key
#  slug       :string
#  name       :string
#  kind       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  tts        :string
#
require 'rails_helper'

RSpec.describe VideoMarkup::Streamer, type: :model do
  it_behaves_like 'validatable'
end
