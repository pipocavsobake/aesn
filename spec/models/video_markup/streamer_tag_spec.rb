# == Schema Information
#
# Table name: video_markup_streamer_tags
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_video_markup_streamer_tags_on_name  (name)
#
require 'rails_helper'

RSpec.describe VideoMarkup::StreamerTag, type: :model do
  it_behaves_like "validatable"
end
