# == Schema Information
#
# Table name: video_markup_heroes_templates
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  cases      :jsonb            not null
#
require 'rails_helper'

RSpec.describe VideoMarkup::Heroes::Template, type: :model do
  it_behaves_like 'validatable'
end
