# == Schema Information
#
# Table name: video_markup_heroes_spells
#
#  id           :bigint           not null, primary key
#  name         :string
#  elements     :string           is an Array
#  file_data    :jsonb
#  level        :bigint
#  kind         :string
#  spell_points :bigint
#  duration     :string
#  base         :string
#  advanced     :string
#  expert       :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  aliases      :string           is an Array
#
require 'rails_helper'

RSpec.describe VideoMarkup::Heroes::Spell, type: :model do
  it_behaves_like 'validatable'
end
