# == Schema Information
#
# Table name: video_markup_heroes_skills
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_video_markup_heroes_skills_on_name  (name) UNIQUE
#
require 'rails_helper'

RSpec.describe VideoMarkup::Heroes::Skill, type: :model do
  it_behaves_like "validatable"
end
