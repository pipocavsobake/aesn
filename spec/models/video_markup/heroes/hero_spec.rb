# == Schema Information
#
# Table name: video_markup_heroes_heroes
#
#  id                   :bigint           not null, primary key
#  name                 :string
#  castle_id            :bigint
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  enabled_fights_count :bigint           default(0), not null
#  image_data           :jsonb
#  cases                :jsonb            not null
#  sex                  :string
#  content              :text
#  aliases              :string           is an Array
#
# Indexes
#
#  index_video_markup_heroes_heroes_on_castle_id             (castle_id)
#  index_video_markup_heroes_heroes_on_castle_id_and_name    (castle_id,name) UNIQUE
#  index_video_markup_heroes_heroes_on_enabled_fights_count  (enabled_fights_count)
#
# Foreign Keys
#
#  fk_rails_...  (castle_id => video_markup_heroes_castles.id)
#
require 'rails_helper'

RSpec.describe VideoMarkup::Heroes::Hero, type: :model do
  it_behaves_like 'validatable'
end
