# == Schema Information
#
# Table name: video_markup_heroes_learn_skills
#
#  id          :bigint           not null, primary key
#  skill_id    :bigint
#  hero_id     :bigint           not null
#  moment_id   :bigint           not null
#  level       :bigint           default("base")
#  attack      :bigint           default(0)
#  defence     :bigint           default(0)
#  spell_power :bigint           default(0)
#  knowledge   :bigint           default(0)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_video_markup_heroes_learn_skills_on_hero_id    (hero_id)
#  index_video_markup_heroes_learn_skills_on_moment_id  (moment_id)
#  index_video_markup_heroes_learn_skills_on_skill_id   (skill_id)
#
# Foreign Keys
#
#  fk_rails_...  (hero_id => video_markup_heroes_heroes.id)
#  fk_rails_...  (moment_id => video_markup_moments.id)
#  fk_rails_...  (skill_id => video_markup_heroes_skills.id)
#
require 'rails_helper'

RSpec.describe VideoMarkup::Heroes::LearnSkill, type: :model do
  it_behaves_like "validatable"
end
