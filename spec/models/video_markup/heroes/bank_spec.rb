# == Schema Information
#
# Table name: video_markup_heroes_banks
#
#  id                   :bigint           not null, primary key
#  name                 :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  enabled_fights_count :bigint           default(0), not null
#  image_data           :jsonb
#  comment              :string
#  cases                :jsonb            not null
#  ai_values            :bigint           is an Array
#  en_name              :string
#
# Indexes
#
#  index_video_markup_heroes_banks_on_enabled_fights_count  (enabled_fights_count)
#  index_video_markup_heroes_banks_on_name                  (name) UNIQUE
#
require 'rails_helper'

RSpec.describe VideoMarkup::Heroes::Bank, type: :model do
  it_behaves_like 'validatable'
end
