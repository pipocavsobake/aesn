# == Schema Information
#
# Table name: video_markup_streamer_statuses
#
#  id          :bigint           not null, primary key
#  streamer_id :bigint           not null
#  game_id     :bigint
#  started_at  :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_video_markup_streamer_statuses_on_game_id_and_created_at  (game_id,created_at)
#  index_video_markup_streamer_statuses_on_streamer_id             (streamer_id)
#
# Foreign Keys
#
#  fk_rails_...  (streamer_id => video_markup_streamers.id)
#
require 'rails_helper'

RSpec.describe VideoMarkup::StreamerStatus, type: :model do
  it_behaves_like "validatable"
end
