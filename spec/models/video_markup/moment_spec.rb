# == Schema Information
#
# Table name: video_markup_moments
#
#  id          :bigint           not null, primary key
#  value       :bigint
#  video_id    :bigint           not null
#  user_id     :bigint
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  stop_at     :bigint
#  playlist_id :bigint
#
# Indexes
#
#  index_video_markup_moments_on_playlist_id  (playlist_id)
#  index_video_markup_moments_on_user_id      (user_id)
#  index_video_markup_moments_on_video_id     (video_id)
#
# Foreign Keys
#
#  fk_rails_...  (playlist_id => video_markup_playlists.id)
#  fk_rails_...  (user_id => users.id)
#  fk_rails_...  (video_id => video_markup_videos.id)
#
require 'rails_helper'

RSpec.describe VideoMarkup::Moment, type: :model do
  it_behaves_like 'validatable'
end
