# == Schema Information
#
# Table name: user_merges
#
#  id               :bigint           not null, primary key
#  identity_id      :bigint           not null
#  email            :string
#  token_expires_at :datetime         not null
#  token            :string           not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_user_merges_on_identity_id  (identity_id)
#  index_user_merges_on_token        (token) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (identity_id => user_identities.id)
#
require 'rails_helper'

RSpec.describe User::Merge, type: :model do
  it_behaves_like "validatable"
end
