# == Schema Information
#
# Table name: mmm_print_requests
#
#  id         :bigint           not null, primary key
#  uid        :string
#  state      :string           default("pending"), not null
#  file_data  :jsonb
#  data       :jsonb
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  chat_id    :string
#
require 'rails_helper'

RSpec.describe Mmm::PrintRequest, type: :model do
  it_behaves_like "validatable"
end
