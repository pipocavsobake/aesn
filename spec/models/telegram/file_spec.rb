# == Schema Information
#
# Table name: telegram_files
#
#  id          :bigint           not null, primary key
#  source      :string
#  telegram_id :string
#  data        :jsonb
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_telegram_files_on_source       (source)
#  index_telegram_files_on_telegram_id  (telegram_id)
#
require 'rails_helper'

RSpec.describe Telegram::File, type: :model do
  it_behaves_like 'validatable'
end
