# == Schema Information
#
# Table name: telegram_messages
#
#  id         :bigint           not null, primary key
#  controller :string
#  payload    :jsonb
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'rails_helper'

RSpec.describe Telegram::Message, type: :model do
  it_behaves_like "validatable"
end
