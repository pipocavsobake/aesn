require 'rails_helper'

RSpec.feature 'home_page' do
  let(:page_obj) { create(:page, fullpath: '/', h1: 'Заг') }
  scenario 'visit', :js do
    page_obj
    visit '/'
    expect(page).to have_css('h1', text: page_obj.h1)
  end
end
