require 'rails_helper'

RSpec.feature 'video_markup/video' do
  include WardenHelper
  let(:user) { create(:user, roles: ['admin']) }
  let(:url) { "https://www.youtube.com/watch?v=AvylvC4XDQs" }
  let(:invalid_url) { 'hasdadsd' }
  let(:invalid_only_on_backend_url) { "https://www.youtube.com/watch?vst=000000" }
  around(:each) do |example|
    VCR.use_cassette('youtube', &example)
  end
  scenario 'create youtube', :js do
    warden_sign_in(user)
    visit '/video_markup/videos/new'
    element = find("input[placeholder='Вставьте ссылку']")
    element.native.send_keys(invalid_url)
    expect(page).to have_css('button[disabled]', text: 'Отправить')

    element.native.clear
    element.native.send_keys(invalid_only_on_backend_url)
    find('button', text: 'Отправить').click
    wait = Selenium::WebDriver::Wait.new ignore: Selenium::WebDriver::Error::NoSuchAlertError
    alert = wait.until { page.driver.browser.switch_to.alert }
    expect(alert.text).to eq("Не удалось распознать ссылку")
    alert.accept

    element.native.clear
    element.native.send_keys(url)
    find('button', text: 'Отправить').click
    expect(page).to have_css('button', text: 'Пауза и заметка')

    find('iframe').native.click
    expect(page).to have_css('.cb-player-indicator[data-playing]')
    find('button', text: 'Пауза и заметка').click
    expect(page).to have_no_css('.cb-player-indicator[data-playing]')
    ts_elem = find('.input-group', text: 'Текущий таймкод')
    expect(ts_elem.text.match(/Текущий таймкод (\d+(?:\.\d+)?) сек/)[1].to_f).to be > 0

  end

end
