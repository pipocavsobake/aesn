# == Schema Information
#
# Table name: user_identities
#
#  id               :bigint           not null, primary key
#  uid              :string
#  provider         :string
#  name             :string
#  user_id          :bigint           not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  token            :string
#  token_expires_at :datetime
#  refresh_token    :string
#
# Indexes
#
#  index_user_identities_on_provider_and_uid  (provider,uid) UNIQUE
#  index_user_identities_on_user_id           (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
FactoryBot.define do
  factory :'user/identity', class: 'User::Identity' do
    user
  end
end
