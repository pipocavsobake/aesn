# == Schema Information
#
# Table name: user_merges
#
#  id               :bigint           not null, primary key
#  identity_id      :bigint           not null
#  email            :string
#  token_expires_at :datetime         not null
#  token            :string           not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_user_merges_on_identity_id  (identity_id)
#  index_user_merges_on_token        (token) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (identity_id => user_identities.id)
#
FactoryBot.define do
  factory :'user/merge', class: 'User::Merge' do
        identity { nil }
    email { "MyString" }
    token { "MyString" }
  end
end
