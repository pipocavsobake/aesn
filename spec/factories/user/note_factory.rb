# == Schema Information
#
# Table name: user_notes
#
#  id         :bigint           not null, primary key
#  user_id    :bigint           not null
#  content    :text
#  theme_id   :bigint           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_user_notes_on_theme_id  (theme_id)
#  index_user_notes_on_user_id   (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (theme_id => user_themes.id)
#  fk_rails_...  (user_id => users.id)
#
FactoryBot.define do
  factory :'user/note', class: 'User::Note' do
    user
    association(:theme, factory: 'user/theme')
  end
end
