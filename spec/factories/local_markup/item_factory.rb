# == Schema Information
#
# Table name: local_markup_items
#
#  id             :bigint           not null, primary key
#  file           :string
#  related_files  :string           default([]), is an Array
#  second         :integer
#  data           :jsonb
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  markup_id      :bigint
#  check_point_id :bigint
#
# Indexes
#
#  index_local_markup_items_on_check_point_id  (check_point_id)
#  index_local_markup_items_on_markup_id       (markup_id)
#
# Foreign Keys
#
#  fk_rails_...  (check_point_id => local_markup_check_points.id)
#  fk_rails_...  (markup_id => local_markup_markups.id)
#
FactoryBot.define do
  factory :'local_markup/item', class: 'LocalMarkup::Item' do
    association(:check_point, factory: 'local_markup/check_point')
    association(:markup, factory: 'local_markup/markup')
  end
end
