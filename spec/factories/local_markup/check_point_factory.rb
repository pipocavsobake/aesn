# == Schema Information
#
# Table name: local_markup_check_points
#
#  id           :bigint           not null, primary key
#  markup_id    :bigint           not null
#  path         :string
#  reached_at   :datetime
#  processed_at :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_local_markup_check_points_on_markup_id  (markup_id)
#
# Foreign Keys
#
#  fk_rails_...  (markup_id => local_markup_markups.id)
#
FactoryBot.define do
  factory :'local_markup/check_point', class: 'LocalMarkup::CheckPoint' do
    association(:markup, factory: 'local_markup/markup')
  end
end
