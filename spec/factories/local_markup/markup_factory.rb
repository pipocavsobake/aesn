# == Schema Information
#
# Table name: local_markup_markups
#
#  id                 :bigint           not null, primary key
#  user_id            :bigint           not null
#  name               :string
#  file_data          :jsonb
#  cues_file_data     :jsonb
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  default_form       :string
#  check_points_count :bigint           default(0), not null
#
# Indexes
#
#  index_local_markup_markups_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
FactoryBot.define do
  factory :'local_markup/markup', class: 'LocalMarkup::Markup' do
    user
    name { 'Название сессии разметки' }
    file { Rack::Test::UploadedFile.new('spec/factories/local_markup/markup_file.json', 'plain/text') }
  end
end
