# == Schema Information
#
# Table name: local_markup_files
#
#  id             :bigint           not null, primary key
#  check_point_id :bigint           not null
#  path           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_local_markup_files_on_check_point_id  (check_point_id)
#
# Foreign Keys
#
#  fk_rails_...  (check_point_id => local_markup_check_points.id)
#
FactoryBot.define do
  factory :'local_markup/file', class: 'LocalMarkup::File' do
    association(:check_point, factory: 'local_markup/check_point')
  end
end
