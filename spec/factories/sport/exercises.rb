# == Schema Information
#
# Table name: sport_exercises
#
#  id              :bigint           not null, primary key
#  name            :string           not null
#  slug            :string           not null
#  content         :text
#  video_data      :jsonb
#  enabled         :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  user_id         :bigint           not null
#  aliases         :string           default([]), not null, is an Array
#  simple_group_id :bigint
#
# Indexes
#
#  index_sport_exercises_on_simple_group_id  (simple_group_id)
#  index_sport_exercises_on_slug             (slug) UNIQUE
#  index_sport_exercises_on_user_id          (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (simple_group_id => sport_exercise_simple_groups.id)
#  fk_rails_...  (user_id => users.id)
#
FactoryBot.define do
  factory :'sport/exercise', class: 'Sport::Exercise' do
        name { "MyString" }
    content { "MyText" }
    image { "" }
  end
end
