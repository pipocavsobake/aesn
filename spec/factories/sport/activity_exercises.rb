# == Schema Information
#
# Table name: sport_activity_exercises
#
#  id          :bigint           not null, primary key
#  activity_id :bigint           not null
#  exercise_id :bigint           not null
#  comment     :text
#  position    :bigint           default(0), not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_sport_activity_exercises_on_activity_id  (activity_id)
#  index_sport_activity_exercises_on_exercise_id  (exercise_id)
#
# Foreign Keys
#
#  fk_rails_...  (activity_id => sport_activities.id)
#  fk_rails_...  (exercise_id => sport_exercises.id)
#
FactoryBot.define do
  factory :'sport/activity_exercise', class: 'Sport::ActivityExercise' do
    activity
    exersize
    comment { "MyText" }
    position { 1 }
  end
end
