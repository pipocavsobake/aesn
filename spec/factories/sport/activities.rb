# == Schema Information
#
# Table name: sport_activities
#
#  id               :bigint           not null, primary key
#  name             :string           not null
#  user_id          :bigint
#  started_at       :datetime
#  duration_seconds :bigint
#  kind             :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  enabled          :boolean          default(FALSE), not null
#  og_image_data    :jsonb
#
# Indexes
#
#  index_sport_activities_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
FactoryBot.define do
  factory :'sport/activity', class: 'Sport::Activity' do
        user { "" }
    started_at { "2022-01-01 14:50:54" }
    duration_seconds { "" }
    kind { "MyString" }
  end
end
