# == Schema Information
#
# Table name: saiu_claims
#
#  id         :bigint           not null, primary key
#  phone      :string
#  email      :string
#  best_time  :text
#  comment    :text
#  status     :string           default("new"), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :'saiu/claim', class: 'Saiu::Claim' do
    phone { "MyString" }
    email { "MyString" }
    best_time { "MyText" }
    comment { "MyText" }
  end
end
