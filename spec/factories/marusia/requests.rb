# == Schema Information
#
# Table name: marusia_requests
#
#  id             :bigint           not null, primary key
#  request_params :jsonb
#  response       :jsonb
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
FactoryBot.define do
  factory :'marusia/request', class: 'Marusia::Request' do
        request_params { "" }
    response { "" }
  end
end
