# == Schema Information
#
# Table name: marusia_aliases
#
#  id         :bigint           not null, primary key
#  name       :string
#  kind       :string           not null
#  target     :string
#  boost      :bigint           default(0), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_marusia_aliases_on_kind_and_name            (kind,name) UNIQUE
#  index_marusia_aliases_on_kind_and_name_and_boost  (kind,name,boost)
#
FactoryBot.define do
  factory :'marusia/alias', class: 'Marusia::Alias' do
        name { "MyString" }
    kind { "MyString" }
    target { "MyString" }
    boost { "" }
  end
end
