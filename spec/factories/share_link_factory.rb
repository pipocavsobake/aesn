# == Schema Information
#
# Table name: share_links
#
#  id         :bigint           not null, primary key
#  user_id    :bigint           not null
#  url        :string
#  session    :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_share_links_on_session  (session)
#  index_share_links_on_slug     (slug)
#  index_share_links_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
FactoryBot.define do
  factory :share_link do
    user
    url { 'https://aesn.ru/sl_example' }
  end
end
