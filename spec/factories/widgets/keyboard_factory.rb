# == Schema Information
#
# Table name: widgets_keyboards
#
#  id                  :bigint           not null, primary key
#  name                :string
#  slug                :string
#  keys                :bigint           default(88)
#  first_freq          :float            default(27.5)
#  block_multiplicator :float            default(2.0)
#  block_schema        :bigint           default(["3", "4"]), is an Array
#  start_block_at      :bigint           default(3)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  enabled             :boolean
#  has_samples         :boolean
#
# Indexes
#
#  index_widgets_keyboards_on_slug  (slug) UNIQUE
#
FactoryBot.define do
  factory :'widgets/keyboard', class: 'Widgets::Keyboard' do
    name { 'Тестовая клавиатура' }
  end
end
