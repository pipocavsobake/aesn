# == Schema Information
#
# Table name: pages
#
#  id            :bigint           not null, primary key
#  title         :string
#  fullpath      :string           not null
#  content       :text
#  h1            :string
#  keywords      :text
#  description   :text
#  og_title      :string
#  robots        :string
#  og_image_data :jsonb
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  subdomain     :string
#
# Indexes
#
#  index_pages_on_fullpath                (fullpath) UNIQUE WHERE (subdomain IS NULL)
#  index_pages_on_subdomain_and_fullpath  (subdomain,fullpath)
#
FactoryBot.define do
  factory :page do
    title { 'Заголовок страницы' }
    sequence(:fullpath) { |n| "/testpage/#{n}" }
    content { '<p>Контент страницы</p>' }
    h1 { 'h1 страницы' }
    keywords { 'ключевые слова страницы' }
    description { 'описание страницы' }
    og_title { 'og заголовок страницы' }
  end
end
