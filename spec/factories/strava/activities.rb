# == Schema Information
#
# Table name: strava_activities
#
#  id                :bigint           not null, primary key
#  user_identity_id  :bigint
#  strava_id         :bigint
#  external_id       :string
#  upload_id         :bigint
#  name              :string
#  distance          :bigint
#  moving_time       :bigint
#  strava_type       :string
#  start_date        :datetime
#  start_point       :point
#  end_point         :point
#  average_speed     :decimal(8, 3)
#  max_speed         :decimal(8, 3)
#  description       :text
#  map_polyline      :text
#  source_data       :jsonb
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  sport_activity_id :bigint
#
# Indexes
#
#  index_strava_activities_on_sport_activity_id  (sport_activity_id)
#  index_strava_activities_on_user_identity_id   (user_identity_id)
#
# Foreign Keys
#
#  fk_rails_...  (sport_activity_id => sport_activities.id)
#  fk_rails_...  (user_identity_id => user_identities.id)
#
FactoryBot.define do
  factory :'strava/activity', class: 'Strava::Activity' do
        strava_id { "" }
    strave_external_id { "MyString" }
    strava_uploads_id { "" }
    name { "MyString" }
    distance { "9.99" }
    moving_time { "" }
    strava_type { "MyString" }
    start_date { "2021-12-21 20:20:08" }
    start_lat { "9.99" }
    start_lng { "9.99" }
    average_speed { "9.99" }
    max_speed { "9.99" }
    description { "MyText" }
    source_data { "" }
  end
end
