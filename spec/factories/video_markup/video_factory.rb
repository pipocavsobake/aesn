# == Schema Information
#
# Table name: video_markup_videos
#
#  id               :bigint           not null, primary key
#  type             :string
#  slug             :string
#  streamer_id      :bigint           not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  title            :string
#  published_at     :datetime
#  snippet_template :string
#
# Indexes
#
#  index_video_markup_videos_on_streamer_id  (streamer_id)
#
# Foreign Keys
#
#  fk_rails_...  (streamer_id => video_markup_streamers.id)
#
FactoryBot.define do
  factory :'video_markup/video' do
    association(:streamer, factory: 'video_markup/streamer')
  end
end
