# == Schema Information
#
# Table name: video_markup_heroes_spell_usages
#
#  id         :bigint           not null, primary key
#  spell_id   :bigint           not null
#  fight_id   :bigint           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_video_markup_heroes_spell_usages_on_fight_id  (fight_id)
#  index_video_markup_heroes_spell_usages_on_spell_id  (spell_id)
#
# Foreign Keys
#
#  fk_rails_...  (fight_id => video_markup_heroes_fights.id)
#  fk_rails_...  (spell_id => video_markup_heroes_spells.id)
#
FactoryBot.define do
  factory :'video_markup/heroes/spell_usage' do
    association(:spell, factory: 'video_markup/heroes/spell')
    association(:fight, factory: 'video_markup/heroes/fight')
  end
end
