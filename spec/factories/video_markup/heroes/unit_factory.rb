# == Schema Information
#
# Table name: video_markup_heroes_units
#
#  id                   :bigint           not null, primary key
#  name                 :string
#  castle_id            :bigint
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  enabled_fights_count :bigint           default(0), not null
#  image_data           :jsonb
#  level                :bigint
#  grade_level          :bigint           default(0), not null
#  cases                :jsonb            not null
#  ground_id            :bigint
#  ai_value             :bigint
#  fight_value          :bigint
#  en_name              :string
#  attack               :bigint
#  defence              :bigint
#  damage_min           :bigint
#  damage_max           :bigint
#  speed                :bigint
#  pop_growth           :bigint
#  skills               :text
#  aliases              :string           is an Array
#  health_points        :bigint
#  gold                 :bigint
#
# Indexes
#
#  index_video_markup_heroes_units_on_castle_id             (castle_id)
#  index_video_markup_heroes_units_on_enabled_fights_count  (enabled_fights_count)
#  index_video_markup_heroes_units_on_ground_id             (ground_id)
#  index_video_markup_heroes_units_on_name                  (name) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (castle_id => video_markup_heroes_castles.id)
#  fk_rails_...  (ground_id => video_markup_heroes_grounds.id)
#
FactoryBot.define do
  factory :'video_markup/heroes/unit' do
    ai_value { 10 }
  end
end
