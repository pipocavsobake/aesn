# == Schema Information
#
# Table name: video_markup_heroes_learn_skills
#
#  id          :bigint           not null, primary key
#  skill_id    :bigint
#  hero_id     :bigint           not null
#  moment_id   :bigint           not null
#  level       :bigint           default("base")
#  attack      :bigint           default(0)
#  defence     :bigint           default(0)
#  spell_power :bigint           default(0)
#  knowledge   :bigint           default(0)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_video_markup_heroes_learn_skills_on_hero_id    (hero_id)
#  index_video_markup_heroes_learn_skills_on_moment_id  (moment_id)
#  index_video_markup_heroes_learn_skills_on_skill_id   (skill_id)
#
# Foreign Keys
#
#  fk_rails_...  (hero_id => video_markup_heroes_heroes.id)
#  fk_rails_...  (moment_id => video_markup_moments.id)
#  fk_rails_...  (skill_id => video_markup_heroes_skills.id)
#
FactoryBot.define do
  factory :'video_markup/heroes/learn_skill', class: 'VideoMarkup::Heroes::LearnSkill' do
    association(:skill, factory: 'video_markup/heroes/skill')
    association(:hero, factory: 'video_markup/heroes/hero')
    association(:moment, factory: 'video_markup/moment')
    level { 1 }
    attack { 0 }
    defence { 0 }
    spell_power { 0 }
    knowledge { 0 }
  end
end
