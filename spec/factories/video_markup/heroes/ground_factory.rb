# == Schema Information
#
# Table name: video_markup_heroes_grounds
#
#  id         :bigint           not null, primary key
#  name       :string
#  penalty    :bigint
#  cases      :jsonb            not null
#  image_data :jsonb
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :'video_markup/heroes/ground' do

  end
end
