# == Schema Information
#
# Table name: video_markup_heroes_fights
#
#  id                    :bigint           not null, primary key
#  moment_id             :bigint           not null
#  hero_id               :bigint           not null
#  attack                :bigint
#  defence               :bigint
#  spell_power           :bigint
#  knowledge             :bigint
#  day                   :bigint
#  better_than_autofight :boolean
#  units_count           :bigint
#  upgraded_units_count  :bigint
#  bank_value            :bigint
#  bank_id               :bigint
#  unit_id               :bigint
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  template_id           :bigint
#  enabled_by_id         :bigint
#  ground_id             :bigint
#  bank_graded           :boolean
#  ai_value              :bigint
#
# Indexes
#
#  index_video_markup_heroes_fights_on_bank_id        (bank_id)
#  index_video_markup_heroes_fights_on_enabled_by_id  (enabled_by_id)
#  index_video_markup_heroes_fights_on_ground_id      (ground_id)
#  index_video_markup_heroes_fights_on_hero_id        (hero_id)
#  index_video_markup_heroes_fights_on_moment_id      (moment_id)
#  index_video_markup_heroes_fights_on_template_id    (template_id)
#  index_video_markup_heroes_fights_on_unit_id        (unit_id)
#
# Foreign Keys
#
#  fk_rails_...  (bank_id => video_markup_heroes_banks.id)
#  fk_rails_...  (enabled_by_id => users.id)
#  fk_rails_...  (ground_id => video_markup_heroes_grounds.id)
#  fk_rails_...  (hero_id => video_markup_heroes_heroes.id)
#  fk_rails_...  (moment_id => video_markup_moments.id)
#  fk_rails_...  (template_id => video_markup_heroes_templates.id)
#  fk_rails_...  (unit_id => video_markup_heroes_units.id)
#
FactoryBot.define do
  factory :'video_markup/heroes/fight' do
    association(:moment, factory: 'video_markup/moment')
    association(:hero, factory: 'video_markup/heroes/hero')
    association(:unit, factory: 'video_markup/heroes/unit')
    units_count { 10 }
  end
end
