# == Schema Information
#
# Table name: video_markup_heroes_artifacts
#
#  id                      :bigint           not null, primary key
#  name                    :string
#  en_name                 :string
#  aliases                 :string           is an Array
#  level                   :bigint
#  attack                  :bigint
#  defence                 :bigint
#  spell_power             :bigint
#  knowledge               :bigint
#  image_data              :jsonb
#  value                   :bigint
#  slot                    :bigint
#  combination_artifact_id :bigint
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#
# Indexes
#
#  index_video_markup_heroes_artifacts_on_combination_artifact_id  (combination_artifact_id)
#
# Foreign Keys
#
#  fk_rails_...  (combination_artifact_id => video_markup_heroes_artifacts.id)
#
FactoryBot.define do
  factory :'video_markup/heroes/artifact', class: 'VideoMarkup::Heroes::Artifact' do
        name { "MyString" }
    en_name { "MyString" }
    aliases { "MyString" }
    level { "" }
    attack { "" }
    defence { "" }
    spell_power { "" }
    knowledge { "" }
  end
end
