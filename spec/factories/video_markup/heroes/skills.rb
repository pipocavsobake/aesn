# == Schema Information
#
# Table name: video_markup_heroes_skills
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_video_markup_heroes_skills_on_name  (name) UNIQUE
#
FactoryBot.define do
  factory :'video_markup/heroes/skill', class: 'VideoMarkup::Heroes::Skill' do
    name { "MyString" }
  end
end
