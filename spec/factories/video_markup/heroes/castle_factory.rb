# == Schema Information
#
# Table name: video_markup_heroes_castles
#
#  id             :bigint           not null, primary key
#  name           :string
#  ground_penalty :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  image_data     :jsonb
#
# Indexes
#
#  index_video_markup_heroes_castles_on_name  (name) UNIQUE
#
FactoryBot.define do
  factory :'video_markup/heroes/castle' do

  end
end
