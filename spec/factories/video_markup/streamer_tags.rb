# == Schema Information
#
# Table name: video_markup_streamer_tags
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_video_markup_streamer_tags_on_name  (name)
#
FactoryBot.define do
  factory :'video_markup/streamer_tag', class: 'VideoMarkup::StreamerTag' do
        
  end
end
