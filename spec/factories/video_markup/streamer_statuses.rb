# == Schema Information
#
# Table name: video_markup_streamer_statuses
#
#  id          :bigint           not null, primary key
#  streamer_id :bigint           not null
#  game_id     :bigint
#  started_at  :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_video_markup_streamer_statuses_on_game_id_and_created_at  (game_id,created_at)
#  index_video_markup_streamer_statuses_on_streamer_id             (streamer_id)
#
# Foreign Keys
#
#  fk_rails_...  (streamer_id => video_markup_streamers.id)
#
FactoryBot.define do
  factory :'video_markup/streamer_status', class: 'VideoMarkup::StreamerStatus' do
    association(:streamer, factory: 'video_markup/streamer')
    game_id { "123" }
    started_at { "2020-07-20 20:43:50" }
  end
end
