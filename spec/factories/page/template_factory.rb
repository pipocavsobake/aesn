# == Schema Information
#
# Table name: page_templates
#
#  id          :bigint           not null, primary key
#  title       :text
#  fullpath    :string
#  content     :text
#  h1          :text
#  keywords    :text
#  description :text
#  og_title    :text
#  robots      :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  subdomain   :string
#
FactoryBot.define do
  factory :'page/template', class: 'Page::Template' do

  end
end
