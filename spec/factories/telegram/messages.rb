# == Schema Information
#
# Table name: telegram_messages
#
#  id         :bigint           not null, primary key
#  controller :string
#  payload    :jsonb
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :'telegram/message', class: 'Telegram::Message' do
        controller { "MyString" }
    payload { "" }
  end
end
