# == Schema Information
#
# Table name: twitch_webhook_challenges
#
#  id         :bigint           not null, primary key
#  data       :jsonb
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :'twitch/webhook_challenge', class: 'Twitch::WebhookChallenge' do
  end
end
